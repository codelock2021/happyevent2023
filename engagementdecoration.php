
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="decoration" content="event management surat">
    <meta name="decoration" content="Baby shower decoration surat">
    <meta name="decoration" content="theme decoration surat">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Engagement Decoration</title>
    <link href="css/bootstrap.min.css" rel=stylesheet>
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/categorypage.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script src="js/header-footer.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="admin/assets/js/img_ajax1.js"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144098545-1');
    </script>
</head>
<body>
	<div w3-include-html="happyheader.php"></div> 
        <script>
            includeHTML();
        </script>
	<section class="baby_shower">
			<div class="baby_contain">
				<div class="baby_title">
					<h2 class="title">Engagement Decoration</h2>
				</div>
			</div>
			<div class="baby_all">
				<div class="baby_flex">
					<div class="baby25">
						
					</div>
				</div>
			</div>
	</section>
	<div w3-include-html="footer.html"></div> 
        <script>
            footerHTML();
        </script>

         <script src="js/plugins/jquery-3.5.1.min.js"></script>
    <script src="js/plugins/bootstrap.min.js"></script>
        <script src="js/script.js"></script>

</body>
</html>
<script>
    $(document).ready(function () {
            set_babyshower_image(9);
        });
</script>
