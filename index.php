<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Event planner and Balloon decoration">
    <meta name="description" content="Balloon decoration, Event planner, Birthday Organizer">
    <meta name="keywords" content="Balloon decoration, Event planner, Birthday Organizer, DJ, Party plot, Baby shower decoration, Room Decoration, Kanku pagla, Fog Matka, Fire Entry, Entry">
    

    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Wedding & Event Planner & Balloon Decoration</title>
    <link href="css/bootstrap.min.css" rel=stylesheet>
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/themify-icons.css" rel="stylesheet">
    <link href="modules/slick/slick.css" rel="stylesheet">
    <link href="modules/slick/slick-theme.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
    <link href="modules/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="modules/youtubepopup/YouTubePopUp.css" rel="stylesheet">
    <link href="modules/swiper/swiper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script src="js/plugins/jquery-3.5.1.min.js"></script>
    <script src="js/header-footer.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144098545-1');
    </script>
    <link rel="stylesheet" href="css/style_file_video.css">
    <script>
        
    </script>
    <style>
    .ResponsiveYTPlayer { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .ResponsiveYTPlayer iframe, .ResponsiveYTPlayer object, .ResponsiveYTPlayer embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
    .visible_in_mobile{
    display: none;
    }
        .whatsapp_a{
      background: #fff;
    text-decoration: none;
    color: #a2783a;
    position: fixed;
    display: flex;
    font-weight: 400;
    justify-content: space-between;
    z-index: 9999;
    bottom: 11px;
    right: 9px;
    font-size: 15px;
    padding: 10px 20px;
    border-radius: 30px;
    box-shadow: 0 1px 15px rgba(32, 33, 36, 0.28);
    align-items: center;
    }
    .whatsapp_a > img{
      width: 50px;
    }
    .mobile_whatsapp{
        display:none;
    }
    .image_w_txt{
    padding-top: 100px;
    height: 750px;
    background-image: url(images/image_over/bac1.jpg);
    background-repeat: no-repeat;
    position: relative;
    background-size: cover;
    }
    .image_w_txt::before{
      content: '';
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 1;
  background: rgb(0 0 0 / 52%);
    }
    .pos_ab_txt{
    /* position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%); */
    position: relative;
    z-index: 1;
    }
    .pos_ab_txt p{
        font-weight: bold;
        font-size: 19px;
        font-family: 'Nunito Sans', sans-serif;
        color: white;
    }
    .image_with_txt{
	/* font-size: 80px;
	font-weight: 700; */
	/* background-clip: text;
	-webkit-background-clip: text; */
	/* color: transparent; */
    height: 95px;
    width: 230px;
    margin: auto;
	background-image: url('admin/assets/images/header_bac/heading_bac_img.png');
    height: 110px;
    background-repeat: no-repeat;
    background-size: contain;
    }
    .paragraph_m{
        padding: 0 15px;
    }
    .background_fix{
        position: absolute;
        top: 0;
        left: 0;
        bottom: 120px;
        width: 100%;
        height: 100%;
        object-fit: cover;
        z-index: -1;
    }
    .font_color_b p{
        color: #fff;
    }
    .dis_none_desk{
        display: none;
    }
    @media only screen and (max-width: 1023px) {
     .image_w_txt{
    height: auto;
    padding-bottom: 50px;
    }
    }
    /*@media only screen and (max-width: 1370px) {*/
    /*.pos_ab_txt p {*/
    /*font-size: 15px;*/
    /*}*/
    /*}*/
    @media only screen and (max-width: 1200px) {
    .pos_ab_txt p {
    font-size: 19px;
    }
    .image_w_txt{
    height: auto;
    padding-bottom: 50px;
    }
    }
    .visible_in_desktop{
    display: block;
    }
    .video_width{
    width: 100% !important;
    height: 100% !important;
    }
    .padd_y_30{
        padding: 30px 0;
    }
    .embed-container {
    position: relative;
    padding-bottom: 70.75%;
    height: 0;
    overflow: hidden;
    }
    .embed-container iframe {
    position: absolute;
    top:0;
    left: 0;
    width: 100%;
    height: 100%;
    }
    .one_video_set >.upload_video{
        width: 100%;
    }
    
    /* 014 comment */
    @media only screen and (min-width: 320px) and (max-width: 767px) {
    .visible_in_desktop{
        display: none;
    }
    .visible_in_mobile{
        display: block;
     }
     .pos_ab_txt p{
         font-size: 15px;
     }
     .desktop_whatsapp{
         display:none;
     }
      .mobile_whatsapp{
         display:block;
     }
     .whatsapp_a{
         padding: 0 0;
     }
     .desktop_whatsapp{
      bottom: 7px;
     }
     }
    </style>
</head>
    <!-- Preloader -->
    <!-- <h1>Event Planner </h1> -->
    <div id="bethany-page-loading" class="bethany-pageloading">
        <div class="bethany-pageloading-inner"> 
        <img src="images/logos.png" class="logo" alt="logo"> 
        </div>
    </div>
    <!-- Navigation -->
    <div w3-include-html="happyheader.php"></div> 
        <script>
            includeHTML();
        </script>
   
       <!-- slider sections -->
   <div class="custom_slider visible_in_desktop">
    <div class="one-time sliderImages">
        <!-- <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide5.1.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div>
        <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide5.2.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div> -->
        <!--  <div class="slider_banner ">-->
        <!--    <div style="background-image:url(images/slider/slide1.jpg);" class="eventbg" baby_titletitle="event">-->
        <!--        <h2 class="text decor_title"></h2>-->
        <!--    </div>-->
        <!--</div>-->
         <!-- <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide5.3.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div> -->
    </div>
    </div>

           <!-- slider sections -->
   <div class="visible_in_mobile">
   <div class="one-time2 sliderImages_mobile">
        <!-- <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide5.1.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div>
        <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide5.2.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div> -->

        <!--  <div class="slider_banner ">-->
        <!--    <div style="background-image:url(images/slider/slide1.jpg);" class="eventbg" baby_titletitle="event">-->
        <!--        <h2 class="text decor_title"></h2>-->
        <!--    </div>-->
        <!--</div>-->
         <!-- <div class="slider_banner ">
            <div style="background-image:url(images/slider/slide5.3.jpg);" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div> -->
        </div>
    </div>

       <!-- custom Photo Gallery -->
    <div class="section-padding sec_main">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                <div class="titlephotoGalleryImg">
        </div>  
              
                </div>
            </div>
            <div class="row photoGalleryImg">
                <!--<div class="col-md-4 gallery-item">-->
                <!--    <a href="images/ballon/about/Screenshot_20210913-191143_Instagram.jpg" title="decoration" class="img-zoom">-->
                <!--        <div class="gallery-box">-->
                <!--            <div class="gallery-img"> <img src="images/ballon/about/Screenshot_20210913-191143_Instagram.jpg" class="img-fluid mx-auto d-block" alt="gallry1" title="decoration"> </div>-->
                <!--            <div class="gallery-detail text-center"> <i class="ti-heart"></i> </div>-->
                <!--        </div>-->
                <!--    </a>-->
                <!--</div>-->
              
            </div>
        </div>
    </div>

       <!-- custom Video Gallery -->
     <section class="video_gallary container sec_main">
               <div class="baby_contain title_videogallery">
              
            </div>
            <div class="custon_videos ind_video">
                    <div class="video_flex videoGallery">      
                    </div>
            </div>
    </section>

    <!-- single video section -->
    <section class="one_video_set">
      
    </section>
        <!-- custom Services -->
    <div class="section-padding hp-services sec_main">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center titleourServices">
                </div>
            </div>
            <div class="row ourServices">
            </div>
        </div>
    </div>



    <!-- Testimonials -->
    <div class="title_image_testtim">
    </div>


        <!-- custom Contact -->
        <a class="up" href="#order_now">
    <div class="section-padding sec_main">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Contact Us</h2>
                     <div class="image_with_txt"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 mb-4">
                       
                            <p>Thank you so much for reaching out to us - we are excited to connect with you! Tell us about you. If you do not receive response within 24 hours, please contact us directly email or phone.</p>
                            <span><b>Phone :</b>   <a href="tel:7600464414">7600464414</a>  /  <a href="tel:9714779996">9714779996</a></span>
                            <div><b>eMail :</b><a href = "mailto: happyeventsurat@gmail.com">happyeventsurat@gmail.com</a></div>
                            <p><b>Address :</b> Silver Business Point, 21, Green Rd, Uttran, Surat, Gujarat 394105A</p>
                        </div>
                        <div class="col-md-5 offset-md-1">
                            <h5>We look forward to being in touch!</h5>
                            <form method="POST" class="row"  id="addcontact">
                                <div class="col-md-6">
                                    <input type="text" name="name" id="name" placeholder="Full Name" />
                                    <span class="error name"></span>   
                                </div>
                                <div class="col-md-6">
                                    <input type="email" name="email" id="email" placeholder="Email"  />
                                    <span class="error email"></span> 
                                </div>
                                <div class="col-md-6">
                                    <input type="phone" name="phone" id="phone" placeholder="Phone"  />
                                    <span class="error phone"></span> 
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="text" id="text" placeholder="Service Type"  />
                                    <span class="error text"></span> 
                                </div>
                                <div class="col-md-12">
                                    <textarea name="message" id="message" cols="40" rows="4" placeholder="Message"></textarea>
                                    <span class="error message"></span> 
                                </div>
                                <div class="col-md-12">
                                    <button class="bethany-btn2" name="submit"  type="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </a>
    
          <!-- content and image -->
      <div class="main_div_text_image">
      <div class="">
      <div class="image_w_txt">
        <!--<img src="images/flo2.png" alt="">-->
      <div class="pos_ab_txt container">
        <p>Welcome to our event website! We provide a wide range of event planning services to make your special occasions unforgettable. Our services include baby shower decoration, balloon decoration, flower decoration, haldi setup, mehndi setup, Punjabi dhol, room decoration, wedding planning, couple entry, fog matka entry, fire gun, cold payro entry, varmala concept, birthday planning, theme decoration, chatthi decoration, baby welcome decoration, naming ceremony, surprise planning, car decoration, and magic show.</p>
        <p>Our team of experienced event planners is dedicated to creating customized experiences that are tailored to your unique needs and preferences. We specialize in creating stunning decorations and setups that transform your event space into a magical wonderland. Whether you're planning a baby shower, wedding, or birthday party, we have the expertise and creativity to bring your vision to life.</p>
        <p>We also offer unique elements like fog matka entry, fire gun, and cold payro entry to add an extra touch of excitement and uniqueness to your event. Our team can also provide entertainment services such as Punjabi dhol and magic shows to keep your guests engaged and entertained throughout the event.</p>
        <p>At our event website, we believe that every event should be special and unforgettable. That's why we provide comprehensive planning services that take care of every detail, from decorations to entertainment to surprise planning. So, if you're looking to create a memorable and personalized event, look no further than our team of experts.</p>

      </div>
      </div>
    </div>
    </div>

     <!-- custom Slider -->
   <div class="container dis_none_desk">
            <section class="pozo-section-slider pt-130">
                <div class="next-container-center">
                    <div class="swiper-container">
                        <div class="swiper-wrapper bottomSlider">
                           
                        </div>
                        <div class="swiper-button-next animate-box" data-animate-effect="fadeInRight"> <i class="ti-arrow-right" aria-hidden="true"></i> </div>
                        <div class="swiper-button-prev animate-box" data-animate-effect="fadeInLeft"> <i class="ti-arrow-left" aria-hidden="true"></i> </div>
                    </div>
                </div>
            </section>
    </div>
    <!-- start autoplay video slider -->
        <div class="video-slider">
        <div class="container016">
            <div class="video-carasol videoslid">
                <div id="mainDivClass" class="main_div_slider_video">
                </div>
            </div>
        </div>
    </div>
    <!-- end autoplay video slider -->
    <!-- Footer -->
    <div w3-include-html="footer.html"></div> 
        <script>
            footerHTML();
        </script>
    <!-- toTop -->
    <a href="index.html#" class="totop">TOP</a>
    <!-- jQuery -->
    <a class="whatsapp_a desktop_whatsapp" href="https://api.whatsapp.com/send?phone=917600464414">Chat with Us  <img src="images/WhatsApp.png" alt=""></a>
    <a class="whatsapp_a mobile_whatsapp" href="https://api.whatsapp.com/send?phone=917600464414"><img src="images/WhatsApp.png" alt=""></a>
   
</body>
 <script src="js/plugins/jquery-3.5.1.min.js"></script>
 <script src="admin/assets/js/img_ajax1.js"></script>
    <script src="js/plugins/bootstrap.min.js"></script>
    <script src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="js/plugins/jquery.isotope.v3.0.2.js"></script>
    <script src="js/plugins/modernizr-2.6.2.min.js"></script>
    <script src="js/plugins/jquery.waypoints.min.js"></script>
    <script src="modules/owl-carousel/owl.carousel.min.js"></script>
    <script src="modules/slick/slick.js"></script>
    <script src="modules/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="modules/masonry/masonry.pkgd.min.js"></script>
    <script src="modules/youtubepopup/YouTubePopUp.js"></script>
    <script src="modules/swiper/swiper.min.js"></script>
    <script src="js/script.js"></script>
</html>

<script>
    $(document).ready(function () {
             var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/embed/99_kgndge4c";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      var player;
      function onYouTubePlayerAPIReady() {
        player = new YT.Player('player', {
          playerVars: { 'autoplay': 1, 'controls': 1,'autohide':1,'wmode':'opaque' },
          videoId: 'JW5meKfy3fY',
          events: {
            'onReady': onPlayerReady}
        });
      }
      function onPlayerReady(event) {
        event.target.mute();
      }
    window.addEventListener('load', async () => {
    let video = document.querySelector('video[muted][autoplay]');
    try {
    await video.play();
    } catch (err) {
    video.controls = true;
     }
     
    });
        set_slider_images();
        set_mobile_slider_images();
        set_photo_gallery();
        set_video_gallery();
        set_our_services();
        client_review();
        set_bottom_slider_images();
        set_one_video_section(); 
         set_autoplay_video_slider_page();
        });
</script>
<script>
    function setVideoSlick(){
    $('#mainDivClass').slick({
        arrows: true,
         dots: false,
      infinite: true ,
      slidesToShow: 5,
        speed: 1000,
      slidesToScroll: 1,
         autoplay: false,
        prevArrow:"<button type='button' class='drag dogarrow1 slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
                nextArrow:"<button type='button' class='drag dogarrow2  slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
             responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 1250,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          arrows: false,
          arrows: true

        }
      },
      {
        breakpoint: 1531,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          dots: true,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
     
    ]
    });

//     var var1,var2,var3;
//     for (var input = 1; input <= 10; input++) {
//  console.log(input);
//  var abc= input;

// //  var var2= 'progress' + input;
// //  console.log(var2);
//     var1= "progressLoop" + input;
//     var2= "progress" + input;
//     var3= "video" + input;
//     console.log(var2);
//     var2 = document.getElementById(var2);
// function var1(){
//     console.log(var3);
//     setInterval(function () {
//         // var2.value = Math.round((var3.currentTime / var3.duration) * 100);
//         console.log(var3);
//       });
// }

// }
// var1();
  
    //  for(var i = 1; i <= 8; i++) {
    //     const progress  = document.getElementById("progress1" + i);
    //  }

// let text = "";
// let i = 1;
// while (i <= 8) {
//     var var2= progress + i;
//     const var2 = document.getElementById("var2");
//     var var1= progressLoop + i;
//     function var1() {
//   setInterval(function () {
   
//     var var3= video + i;
//     var2.value = Math.round((var3.currentTime / var3.duration) * 100);
//   });
// }
// var1();
//   i++;
// }
//    const progress1 = document.getElementById("progress1");
//    const progress2 = document.getElementById("progress2");
//    const progress3 = document.getElementById("progress3");
//    const progress4 = document.getElementById("progress4");
//    const progress5 = document.getElementById("progress5");
//    const progress6 = document.getElementById("progress6");
//    const progress7 = document.getElementById("progress7");
//    const progress8 = document.getElementById("progress8");

// function progressLoop1() {
//       setInterval(function () {
//         progress1   .value = Math.round((video1.currentTime / video1.duration) * 100);
//       });
//     }
//     progressLoop1();

// function progressLoop2() {
//   setInterval(function () {
//     progress2.value = Math.round((video2.currentTime / video2.duration) * 100);
//   });
// }
// progressLoop2();

// function progressLoop3() {
//   setInterval(function () {
//     progress3.value = Math.round((video3.currentTime / video3.duration) * 100);
//   });
// }
// progressLoop3();

// function progressLoop4() {
//   setInterval(function () {
//     progress4.value = Math.round((video4.currentTime / video4.duration) * 100);
//   });
// }
// progressLoop4();

// function progressLoop5() {
//   setInterval(function () {
//     progress5.value = Math.round((video5.currentTime / video5.duration) * 100);
//   });
// }
// progressLoop5();

// function progressLoop6() {
//   setInterval(function () {
//     progress6.value = Math.round((video6.currentTime / video6.duration) * 100);
//   });
// }
// progressLoop6();

// function progressLoop7() {
//   setInterval(function () {
//     progress7.value = Math.round((video7.currentTime / video7.duration) * 100);
//   });
// }
// progressLoop7();

// function progressLoop8() {
//   setInterval(function () {
//     progress8.value = Math.round((video8.currentTime / video8.duration) * 100);
//   });
// }
// progressLoop8();
}
</script>