<?php
include 'connection.php';
$con = new DB_Class();
$con = $GLOBALS['conn'];
?>

<head>
</head>
<header>
    <div class="announcbar">

        <p class="message">
            <span>Happy Event Surat</span>
            <span> Call Now -  <a href="tel:7600464414">7600464414</a>  |  
                        <a href="tel:9714779996">9714779996</a>  |  
                        <a href="tel:9974727717">9974727717</a>
                        </span>

        </p>

    </div>
    <div class="bethany-header-navbar">
        <div class="clstargetcontainer container bethany-navbar-container">
            <div class="bethany-navigation-wrap bg-light bethany-start-header start-style">
                <div class=" mainmenubar">
                    <div class="row">
                        <div class="col-md-12">
                            <nav class="navbar navbar-expand-md navbar-light">
                                <!-- Logo -->
                                <a class="navbar-brand" href="index.php">
                                    <img src="images/logos.png" alt="">
                                    <!-- <h2>Bethany<span>Wedding & Event</span></h2> -->
                                </a>
                                <!-- Menu -->
                                <p class="boggasbar"><button class="navbar-toggler" type="button" data-toggle="collapse"
                                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                        aria-expanded="false" aria-label="Toggle navigation"> <span
                                            class="navbar-toggler-icon"></span> </button></p>
                                <div class="colContactlapse navbar-collapse collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto py-4 py-md-0">
                                        <li class="nav-item "> <a class="nav-link" href="index.php">Home</a></li>
                                        <li class="nav-item dropdown"> <a class="nav-link dropbtn" href="#">Categories
                                                <i class="fa fa-caret-down"></i> </a>
                                            <div class="dropdown-content">
                                                <?php $query = "SELECT * from category_data order by id asc";
                                                $result = mysqli_query($con, $query);
                                                $ingore_array = array('Slider images','Photo Gallery','Our Services','Bottom Slider','Slider images Mobile');
                                                while($row = mysqli_fetch_array($result)){
                                                    $clsctgr = strtolower($row['category']);
                                                    if (in_array($row['category'], $ingore_array)) {
                                                        continue;
                                                    }
                                                ?>
                                                <a href="<?php echo str_replace(' ','',$clsctgr);?>.php"
                                                    value="<?php echo $row['id'];?>"
                                                    class="baby_shop"><?php echo $row['category'];?></a>
                                                <?php } ?>
                                                <!--                                                <a href="BabyShower.html " class="baby_shop">Baby Shower</a>
                                                <a href="BabyWelcom.html" class="baby_shop">Baby Welcome </a>
                                                <a href="BirthdayDecoration.html" class="baby_shop">Birthday Decoration  </a>
                                                <a href="NameCeremony.html" class="baby_shop">Naming Ceremony </a>
                                                <a href="RoomDecoration.html" class="baby_shop">Room Decoration</a>
                                                <a href="ThemeDecoration.html" class="baby_shop">Theme Decoration </a>
                                                <a href="HaldiCeremony.html " class="baby_shop">Haldi Ceremony </a>
                                                <a href="BrideDecoration.html" class="baby_shop">Bride to be Decoration </a>
                                                <a href="EngagementDecoration.html" class="baby_shop">Engagement Decoration </a>
                                                <a href="ShopInauguration.html" class="baby_shop">Shop Inauguration </a>
                                                <a href="CorporateEvent.html" class="baby_shop">Corporate Event</a>
                                                <a href="SurprisePlanning.html" class="baby_shop">Surprise Planning</a> -->
                                            </div>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link" href="video.php">Videos</a></li>
                                        <li class="nav-item"> <a class="nav-link" href="gallery.php">Gallery</a> </li>
                                        <li class="nav-item"> <a class="nav-link" href="contact.php">Contact</a> </li>
                                        <li class="nav-item"> <a class="nav-link"
                                                href="http://happyeventsurat.com/admin/img_login.php">login</a> </li>
                                    </ul>
                                </div>
                                <div class="shopnowbtn">
                                    <p><a href="#order_now" class="down">Order Now</a></p>

                                </div>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<style>
.bethany-navigation-wrap {
    position: relative;
}

h1 {
    display: none;
}

.announcbar {
    background-color: #cba773;
}

.message {
    margin-bottom: 0px;
}

.message {
    display: flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
    padding: 8px 0px;
}

.message span {
    color: #fff;
    font-weight: bolder;
}

.clstargetcontainer {
    max-width: 1600px;
}

.shopnowbtn p {
    margin-bottom: 0px;
}

.shopnowbtn a {
    border: 1px solid #cba773;
    padding: 7px 25px;
    border-radius: 25px;
    background-color: #cba773;
    color: #fff;
    font-weight: bold;
    /*line-height: 50px;*/
}

ul.navbar-nav.ml-auto.py-4.py-md-0 {
    margin: 0 auto;
}

@media only screen and (max-width: 955px) {
    .navbar-light .navbar-nav .nav-link {
        font-size: 12px;
    }

    .navbar-brand img {
        height: 65px;
    }

    .shopnowbtn a {
        padding: 7px 15px;
    }

    @media only screen and (max-width: 767px) {
        .shopnowbtn {
            order: 3;
        }

        .navbar-toggler {
            order: 1;
        }

        a.navbar-brand {
            order: 2;
        }

    }

    @media only screen and (max-width: 375px) {
        .message span {
            font-size: 13px;
            text-align: center;
        }

        .navbar-brand img {
            height: 50px;
        }

        .shopnowbtn a {
            padding: 4px 15px;
            font-size: 12px;
        }
    }
</style>