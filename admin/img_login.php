<?php
include 'img_function.php';
$db = new Register();
session_start();
if (isset($_SESSION['id'])) {
    header("Location:img_gallery.php");
}
?>
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Happy Event | Event planner | Birthday Organizer</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/authentication.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="assets/js/img_ajax1.js"></script>
    </head>
    <style>
        #sign_in .title{
            margin-bottom: 20px;
        }
        .clsbox{
            border-radius: 10px;
        }
        img{
            border-radius: 10px;
        }
        .error-msg{
            color: red;
            font-size: 15px;
        }
        .logo img {
            width: 100%;
        }
        .theme-orange .authentication .card .header{
            background: linear-gradient(
                    45deg, #080808, #6f6b4b);
        }
        .happy{
            color: #b4a263;
        }
        #clssignin{
            background: linear-gradient(
                    45deg, #080808, #6f6b4b) !important;
            color: #b4a263 !important;
        }
    </style>
    <body class="theme-orange">
        <div class="authentication">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="header slideDown clsbox">
                                <div class="logo"><img src="assets/images/xs/happyevent (10).png" alt="Codelock"></div>
                                <h1 class="happy">Happy Events Admin</h1>
                            </div>                        
                        </div>
                        <form class="col-lg-12" id="sign_in" method="POST">
                            <h5 class="title">Sign in to your Account</h5>
                            <span class="error-msg userName1"></span>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="email">
                                    <label class="form-label">Email</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" class="form-control" id="password">
                                    <label class="form-label">Password</label>
                                </div>
                            </div>                      
                        </form>
                        <div class="col-lg-12">
                            <a class="btn btn-raised" id="clssignin">SIGN IN</a>
                        </div>                   
                    </div>
                </div>
            </div>
        </div>
        <!-- Jquery Core Js -->
        <script src="assets/bundles/libscripts.bundle.js"></script>
        <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/vendorscripts.bundle.js"></script>
        <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/jvectormap.bundle.js"></script>
        <!-- JVectorMap Plugin Js -->
        <script src="assets/bundles/morrisscripts.bundle.js"></script>
        <!-- Morris Plugin Js -->
        <script src="assets/bundles/sparkline.bundle.js"></script>
        <!-- Sparkline Plugin Js -->
        <script src="assets/bundles/knob.bundle.js"></script>
        <!-- Jquery Knob Plugin Js -->
        <script src="assets/bundles/mainscripts.bundle.js"></script>
        <script src="assets/js/pages/index.js"></script>
        <script src="assets/js/pages/charts/jquery-knob.min.js"></script>
    </body>
</html>