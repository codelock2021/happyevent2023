<?php
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Happy Event | Event planner | Birthday Organizer</title>
    <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="../css/style_css_admin.css">
    <link rel="stylesheet" href="assets/css/imggallery.css">

    <link rel="stylesheet" href="assets/css/color_skins.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="assets/js/img_ajax1.js"></script>
</head>

<body class="theme-orange">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div><!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <?php
    //        Top Bar
    include 'navbar.php';
    //        Left Sidebar
    include 'sidebar.php';
    ?>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Video Gallery
                        <small class="text-muted">Welcome to Happy Event Video Gallery Page</small>
                    </h2>
                </div>
            </div>
        </div>
        <!--<center><span class="clsvideo"></span></center>-->
        <div class="alert alert-success clsvideo" role="alert">
        </div>
        <!-- <div class="video_input form-wrap ">
                <input type="text" class="video_link" placeholder="Enter Video Link" name="video_link">
                <button class="video_link_add">Add</button>
            </div> -->
        <div class="form-wrap">
            <div class="d-flex">
                <div class="col-md-12">
                    <div class="form-group">
                        <label id="name-label" for="name">Video Link</label>
                        <input type="text" name="video_link" placeholder="Enter Video Link" aria-describedby="emailHelp" class="form-control padd_10_all_d video_link video_link_new">
                    </div>
                    <span class="err client-name"></span>
                </div>
            </div>
            <div class="d-flex jus__cont_end">
                <div class="col-md-4">
                    <button class="btn btn-primary btn-block btn_back_color_set video_link_add width100_video" id="clientReviewForm"><i id="clsicon" class="fa fa-spinner fa-spin clsicon" style="display: none"></i><span>Submit</span></button>
                </div>
            </div>
        </div>
        <div class="card">
            <table class="table table-hover video_table" data-listing="true">
                <thead class="clsvideothead">
                    <tr>
                        <th class="th_id">Id</th>
                        <th>Video Links</th>
                        <th class="th_action">Action</th>
                    </tr>
                </thead>
                <tbody class="video_tbody">

                </tbody>
            </table>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js -->
    <script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
    <script src="assets/js/pages/forms/basic-form-elements.js"></script>
    </form>
</body>

</html>
<script>
    $(document).ready(function() {
        set_video_pages();
    });
</script>