<?php
$con = mysqli_connect('localhost', 'u402017191_image_gallery', 'Image_gallery@99', 'u402017191_image_gallery');
$clsid = (isset($_GET['id']) && $_GET['id'] != "") ? $_GET['id'] : '';
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Happy Event | Event planner | Birthday Organizer</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" />
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/morrisjs/morris.css" />
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link rel="stylesheet" href="assets/css/multistep_order.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="assets/js/img_ajax1.js"></script>
    </head>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore CodeLock...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>


        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>

        <section class="content home">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <input type="hidden" name="id" id="clshidden" value="<?php echo $clsid; ?>">
                        <h2>Invoice
                            <small class="text-muted">Welcome to Happy Event Invoice Data Page</small>
                        </h2>
                        <!--<center><span class="error-msg clsdel clsinsert"></span></center>-->
                    </div>
                </div>
                <button class="export_to_pdf">Generate PDF</button>
            </div>
            <div class="container">
                <div class="row">
                    <div class="span4">
                        <img src="assets\images\xs\happyevent (10).png" class="img-rounded logo">
                        <address>
                            <strong>Happy Event</strong><br>

                            <strong>Phone:</strong> <a href="tel:+917600464414">+917600464414</a><br>
                            <strong>Phone:</strong> <a class="phone_a" href="tel:+919714779996">+919714779996</a><br>
                            <strong>Phone:</strong> <a href="tel:+919774727717">+919774727717</a>
                        </address>
                    </div>
                    <div class="span4 well customer_div">
                        <table class="table table-hover invoice-head" data-listing="true">
                            <thead invoice_head_cls>
                                <tr>
                                    <th class="pull-right">Order Number</th>
                                    <th class="pull-right">Customer Name</th>
                                    <th class="pull-right">Customer Number</th>
                                    <th class="pull-right">Address</th>
                                    <th class="pull-right">Date</th>
                                    <th class="pull-right">Time</th>
                                </tr>
                            </thead>
                            <tbody class="invoice_head_body">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row customer_row_div">
                    <div class="span8 well invoice-body">
                        <div class="span4 well customer_mobile_div">
                            <table class="table table-hover invoice-head" data-listing="true">
                                <thead class="invoice_head_cls">
                                    <tr>
                                        <th class="pull-right">Title</th>
                                        <th class="pull-right">Value</th>
                                    </tr>
                                </thead>
                                <tbody class="customerDetailsHtml">

                                </tbody>
                            </table>
                        </div>
                        <table class="table table-hover table-bordered" data-listing="true">
                            <thead classs="invoice_thead">
                                <tr>
                                    <th class="invoice_pro">Product</th>
                                    <th class="invoice_pro">Details</th>
                                    <th class="invoice_pro">Quantity</th>
                                </tr>
                            </thead>
                            <tbody class="invoice_body_pro">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row invoice_thanks">
                    <div class="span8 well invoice-thank">
                        <h5 style="text-align:center;">Thank You!</h5>
                    </div>
                    <div class="span8 well invoice-thank">
                        <a>Note : All items except  Balloon and real flower are returnable, if any item will 
                        damage or lost , cost of items will be charged.</a><br>
                        <a>Cancellation charges 500 rupees.</a>
                    </div>
                </div>
                <div class="row cls_address">
                    <div class="span3">
                        <strong>Email:</strong> <a href="mailto:happyeventsurat@gmail.com">happyeventsurat@gmail.com</a>
                    </div>
                    <div class="span3">
                        <strong>Website:</strong> <a href="http://happyeventsurat.com/">http://happyeventsurat.com/</a>
                    </div>
                </div>
            </div>
        </section>
        <script src="assets/bundles/vendorscripts.bundle.js"></script>
        <script src="assets/bundles/knob.bundle.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script>
        <!--<script src="assets/js/pages/index.js"></script>-->
    </body>
</html>
<script>
    $(document).ready(function () {
        customer_order_data();
    });
</script>