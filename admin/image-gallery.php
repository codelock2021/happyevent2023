<!doctype html>
<html class="no-js " lang="en">

    <!-- Mirrored from www.wrraptheme.com/templates/nexa/html/image-gallery.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 01 Jan 2020 09:25:23 GMT -->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

        <title>:: Nexa :: Image Gallery</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Favicon-->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <!-- Light Gallery Plugin Css -->
        <link href="assets/plugins/light-gallery/css/lightgallery.css" rel="stylesheet">
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
    </head>

    <style>
        .theme-orange .user-info{
            display: flex;
            padding-left: 1px;
        }
    </style>
    
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>

        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>

        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#skins">Skins</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat">Chat</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings">Setting</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane in active in active slideRight" id="skins">
                    <div class="slim_scroll">
                        <h6>Flat Color</h6>
                        <ul class="choose-skin">
                            <li data-theme="purple">
                                <div class="purple"></div><span>Purple</span></li>
                            <li data-theme="blue">
                                <div class="blue"></div><span>Blue</span></li>
                            <li data-theme="cyan">
                                <div class="cyan"></div><span>Cyan</span></li>
                        </ul>
                        <h6>Multi Color</h6>
                        <ul class="choose-skin">
                            <li data-theme="black">
                                <div class="black"></div><span>Black</span></li>
                            <li data-theme="deep-purple">
                                <div class="deep-purple"></div><span>Deep Purple</span></li>
                            <li data-theme="red">
                                <div class="red"></div><span>Red</span></li>
                        </ul>
                        <h6>Gradient Color</h6>
                        <ul class="choose-skin">
                            <li data-theme="green">
                                <div class="green"></div><span>Green</span> </li>
                            <li data-theme="orange" class="active">
                                <div class="orange"></div><span>Orange</span></li>
                            <li data-theme="blush">
                                <div class="blush"></div><span>Blush</span></li>
                        </ul>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane pullUp" id="chat">
                    <div class="right_chat slim_scroll">
                        <div class="search">
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Search..." required autofocus>
                                </div>
                            </div>
                        </div>
                        <h6>Recent</h6>
                        <ul class="list-unstyled">
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar4.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Sophia</span>
                                            <span class="message">There are many variations of passages of Lorem Ipsum available</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Grayson</span>
                                            <span class="message">All the Lorem Ipsum generators on the</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Isabella</span>
                                            <span class="message">Contrary to popular belief, Lorem Ipsum</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="me">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/1.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">John</span>
                                            <span class="message">It is a long established fact that a reader</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar3.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Alexander</span>
                                            <span class="message">Richard McClintock, a Latin professor</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <h6>Contacts</h6>
                        <ul class="list-unstyled">
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar10.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar6.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar7.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar8.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar9.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar4.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar3.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="assets/images/xs/avatar1.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane slideLeft" id="settings">
                    <div class="settings slim_scroll">
                        <p class="text-left">General Settings</p>
                        <ul class="setting-list">
                            <li><span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li><span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p class="text-left">System Settings</p>
                        <ul class="setting-list">
                            <li><span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li><span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p class="text-left">Account Settings</p>
                        <ul class="setting-list">
                            <li><span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li><span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>

        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Image Gallery
                            <small class="text-muted">Welcome to Nexa Application</small>
                        </h2>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <ul class="breadcrumb float-md-right">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Nexa</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Medias</a></li>
                            <li class="breadcrumb-item active">Image Gallery</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2>Tags</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more-vert"></i> </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="tag-list">
                                    <a href="javascript:void(0);" class="btn btn-raised btn-primary btn-sm">Animals</a>
                                    <a href="javascript:void(0);" class="btn btn-raised btn-success btn-sm">fashion</a>
                                    <a href="javascript:void(0);" class="btn btn-raised btn-info btn-sm">Lifestyle</a>
                                    <a href="javascript:void(0);" class="btn btn-raised bg-black btn-sm">bootstrap</a>
                                    <a href="javascript:void(0);" class="btn btn-raised btn-warning btn-sm">politics</a>
                                    <a href="javascript:void(0);" class="btn btn-raised bg-blush btn-sm">photography</a>
                                    <a href="javascript:void(0);" class="btn btn-raised bg-green btn-sm">Music</a>
                                    <a href="javascript:void(0);" class="btn btn-raised bg-orange btn-sm">Food &amp; drink</a>
                                    <a href="javascript:void(0);" class="btn btn-raised bg-purple btn-sm">Car</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2> GALLERY <small>All pictures taken from <a href="https://pexels.com/" target="_blank">pexels.com</a></small> </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more-vert"></i> </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Action</a></li>
                                            <li><a href="javascript:void(0);">Another action</a></li>
                                            <li><a href="javascript:void(0);">Something else here</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/1.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-1.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/2.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-2.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/3.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-3.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/4.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-4.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/5.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-5.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/6.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-6.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/7.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-7.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/8.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-8.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/9.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-9.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/10.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-10.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/11.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-11.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/12.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-12.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/13.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-13.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/14.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-14.jpg" alt=""> </a>
                                    </div>
                                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                        <a href="assets/images/image-gallery/15.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-15.jpg" alt=""> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Jquery Core Js -->
        <script src="assets/bundles/libscripts.bundle.js"></script>
        <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/vendorscripts.bundle.js"></script>
        <!-- Lib Scripts Plugin Js -->

        <script src="assets/plugins/light-gallery/js/lightgallery-all.js"></script>
        <!-- Light Gallery Plugin Js -->

        <script src="assets/bundles/mainscripts.bundle.js"></script>
        <!-- Custom Js -->
        <script src="assets/js/pages/medias/image-gallery.js"></script>
    </body>

    <!-- Mirrored from www.wrraptheme.com/templates/nexa/html/image-gallery.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 01 Jan 2020 09:26:32 GMT -->

</html>