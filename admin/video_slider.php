<?php
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Happy Event | Event planner | Birthday Organizer</title>
    <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/imggallery.css">
    <link rel="stylesheet" href="../css/style_css_admin.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="assets/js/img_ajax1.js"></script>
</head>

<body class="theme-orange">
    <style>
        .center-button {
            margin: auto;
            display: block;
        }

        /* .video:-webkit-full-page-media {
                width: 100%;
                height: 100%;
            } */
    </style>
    <form id="video_slider" name="video_slider" method="POST" enctype="multipart/form-data" onsubmit="">
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div><!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <?php
        //        Top Bar
        include 'navbar.php';
        //        Left Sidebar
        include 'sidebar.php';
        ?>
        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Autoplay Slider Video
                            <small class="text-muted">Welcome to Happy Event one video Page</small>
                        </h2>
                    </div>
                </div>
            </div>
            <!--<center><span class="clsvideo"></span></center>-->
            <div class="alert alert-success clsvideo clsbvideo clsslider_vdo" role="alert">
            </div>
            </div>
            <!-- <div class="block-header video_input"> -->
            <!-- <h6>upload video :</h6> -->
            <!-- <input type="file" name="file22" class="LaunchInput img_file addimage_q"> -->


            <!-- <div class="input-group mb-3 width_div_sp">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input LaunchInput img_file addimage_q" id="inputGroupFile01" name="a_video_slider" accept=".jpeg, .jpg, .jpe" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div> -->

            <!-- <button  class="video_link_add_home_page_upload">Add</button>     -->

            <!-- </div> -->
            <!-- <button type="submit" class="autoplay_video_slider center-button">Add</button> -->

            <div class="form-wrap">
                <div class="d-flex">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Upload Video</label>
                            <div class="preview-zone hidden">
                                <div class="box222 box-solid">
                                    <div class="box-header with-border">
                                        <div><b>Preview</b></div>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-danger btn-xs remove-preview">
                                                <i class="fa fa-times"></i> Reset This Image
                                            </button>
                                        </div>
                                    </div>
                                    <div class="box-body"></div>
                                </div>
                            </div>
                            <div class="dropzone-wrapper">
                                <div class="dropzone-desc">
                                    <i class="glyphicon glyphicon-download-alt"></i>
                                    <p>Choose an video or drag it here.</p>
                                </div>
                                <!-- <input type="file" name="img_logo" class="dropzone"> -->
                                <input type="file" class="custom-file-input dropzone LaunchInput img_file addimage_q" id="inputGroupFile01" name="a_video_slider" accept=".jpeg, .jpg, .jpe" aria-describedby="inputGroupFileAddon01">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex jus__cont_end">
                    <div class="col-md-4">
                        <button type="submit" class="btn btn-primary btn-block btn_back_color_set  width100_video autoplay_video_slider"><i id="clsicon" class="fa fa-spinner fa-spin clsicon" style="display: none"></i><span>Add</span></button>
                    </div>
                </div>
            </div>

        </section>
        <!-- Jquery Core Js -->
        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
        <script src="assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js -->
        <script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
        <script src="assets/js/pages/forms/basic-form-elements.js"></script>
    </form>
</body>

</html>
<script>
    $(document).ready(function() {
        // set_one_video_section();
    });
</script>