<!DOCTYPE html>
<html>
    <head>
        <title>Product</title>
        <link href="assets/style.css" type="text/css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="assets/style.js"></script> 
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>
    <body>

        <div class="container">
            <div class="row clearfix">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover" id="tab_logic">
                        <thead>
                            <tr>
                                <th class="text-center"> # </th>
                                <th class="text-center"> Product </th>
                                <th class="text-center"> Qty </th>
                                <th class="text-center"> Price </th>
                                <th class="text-center"> Total </th>
                                <th class="text-center"> Button </th>
                            </tr>
                        </thead>
                        <tbody class="clstbody">
                            <tr id='addr0'>
                                <td>1</td>
                                <td><input type="text" name='product[]'  placeholder='Enter Product Name' class="form-control"/></td>
                                <td><input type="number" name='qty[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0"/></td>
                                <td><input type="number" name='price[]' placeholder='Enter Unit Price' class="form-control price" step="0.00" min="0"/></td>
                                <td><input type="number" name='total[]' placeholder='0.00' class="form-control total" readonly/></td>
                                <td><button id='delete_row' class='pull-right btn btn-default'>Delete Row</button></td>
                            </tr>
                            <!-- <tr id='addr1'></tr> -->
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12">
                    <input type="number" class="size" id="size" placeholder="0">
                    <button id="add_row" class="btn btn-default pull-left">Add Product</button>
                    <!-- <button id='delete_row' class="pull-right btn btn-default">Delete Row</button> -->
                </div>
            </div>
            <div class="row clearfix" style="margin-top:20px">
                <div class="pull-right col-md-4">
                    <table class="table table-bordered table-hover" id="tab_logic_total">
                        <tbody>
                            <tr>
                                <th class="text-center">Sub Total</th>
                                <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                            </tr>
                            <tr>
                                <th class="text-center">Tax</th>
                                <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                                        <input type="number" class="form-control" id="tax" placeholder="0">
                                        <div class="input-group-addon">%</div>
                                    </div></td>
                            </tr>
                            <tr>
                                <th class="text-center">Tax Amount</th>
                                <td class="text-center"><input type="number" name='tax_amount' id="tax_amount" placeholder='0.00' class="form-control" readonly/></td>
                            </tr>
                            <tr>
                                <th class="text-center">Discount</th>
                                <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                                        <input type="number" class="form-control" id="discount" placeholder="0">
                                        <div class="input-group-addon">%</div>
                                    </div></td>
                            </tr>
                            <tr>
                                <th class="text-center">Discount Amount</th>
                                <td class="text-center"><input type="number" name='discount_amount' id="discount_amount" placeholder='0.00' class="form-control" readonly/></td>
                            </tr>
                            <tr>
                                <th class="text-center">Grand Total</th>
                                <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly/></td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

    </body>
</html>
<script>
    $(document).ready(function () {
        var i = 0;
        $("#add_row").click(function () {
            // $('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
            // $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
            // i++; 
            // $('table').append("<tr id='addr0'></tr>");
            var str = "";
            $id = "";
            for (var j = 0; j < $('#size').val(); j++) {
                // console.log(j);
                $id = j;
                console.log($id);
                str += "<tr id='addr0' >";
                str += "<td>' . $id . '</td>";
                str += "<td><input type='text' name='product[]'  placeholder='Enter Product Name' class='form-control'/></td>";
                str += "<td><input type='number' name='qty[]' placeholder='Enter Qty' class='form-control qty' step='0' min='0'/></td>";
                str += "<td><input type='number' name='price[]' placeholder='Enter Unit Price' class='form-control price' step='0.00' min='0'/></td>";
                str += "<td><input type='number' name='total[]' placeholder='0.00' class='form-control total' readonly/></td>";
                str += "<td><button id='delete_row' class='pull-right btn btn-default'>Delete Row</button></td>";
                str += "</tr>";
                
                $output.="<tr id='addr0'>
                <td>" . $id . "</td>
                <td><input type="text" name="product[]"  placeholder="Enter Product Name" class="form-control"/></td>
                <td><input type="number" name="qty[]" placeholder="Enter Qty" class="form-control qty" step="0" min="0"/></td>
                <td><input type="number" name="price[]" placeholder="Enter Unit Price" class="form-control price" step="0.00" min="0"/></td>
                <td><input type="number" name="total[]" placeholder="0.00" class="form-control total" readonly/></td>
                <td>" . $chk . "</td>
                <td>" . $row["about"] . "</td>
                <td>" . $row["Role"] . "</td>
            </tr>";
            }
            $('.clstbody tr:last').after(str);
        });
        //    $("#delete_row").click(function(){
        //    	if(i>1){
        // 	$("#addr"+(i-1)).html('');
        // 	i--;
        // 	}
        // 	calc();
        // });
        $('#tab_logic #delete_row').on('click', function () {
            $(this).closest('#addr0').remove();
        });
        // $("#tab_logic #delete_row").click(function () {
        // 	alert("fgfgf");
        //                                $(this).closest('#tab_logic #addr0').remove();
        //                            });

        $('#tab_logic tbody').on('keyup change', function () {
            calc();
        });
        $('#tax').on('keyup change', function () {
            calc_total();
        });
        $('#discount').on('keyup change', function () {
            calc_total();
        });


    });

    function calc()
    {
        $('#tab_logic tbody tr').each(function (i, element) {
            var html = $(this).html();
            if (html != '')
            {
                var qty = $(this).find('.qty').val();
                var price = $(this).find('.price').val();
                $(this).find('.total').val(qty * price);

                calc_total();
            }
        });
    }

    function calc_total()
    {
        total = 0;
        $('.total').each(function () {
            total += parseInt($(this).val());
        });
        $('#sub_total').val(total.toFixed(2));
        tax_sum = total / 100 * $('#tax').val();
        // console.log(tax_sum);
        $('#tax_amount').val(tax_sum.toFixed(2));
        clstotal = tax_sum + total;
        // console.log(clstotal);
        discount_amount = (tax_sum + total) / 100 * $('#discount').val();
        console.log(discount_amount);
        $('#discount_amount').val(discount_amount.toFixed(2));
        // alert(var clstotal);
        $('#total_amount').val((clstotal - discount_amount).toFixed(2));
    }
</script>


