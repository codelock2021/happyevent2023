
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Employee Dashboard</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/avatar7.jpg" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css"/>
        <link rel="stylesheet" href="assets/plugins/morrisjs/morris.css"/>
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link rel="stylesheet" href="assets/css/color_skins.css">
    </head>
    <style>
        table{
            justify-content: center;
            margin-top: 10px;
            border-style: solid;
            border-color: black;
            border-width: 5px;
            border-radius: 5px;
            background-color: silver;
        }
        #subBtn{
            border-style: solid;
            border-color: black;
            border-width: 2px;
            border-radius: 5px;
        }
        #taxBtn{
            border-style: solid;
            border-color: black;
            border-width: 2px;
            border-radius: 5px;
        }
        #gtotalBtn{
            border-style: solid;
            border-color: black;
            border-width: 2px;
            border-radius: 5px;
        }
        td {
            border: black solid 1px;
        }
        .clsdiv{
            margin-top: 4%;
        }
        .clstr{
            margin-top: 10px;
            font-weight: 800;
            display: flex;
        }
        .clsqu{
            font-weight: 800;
            display: flex;
        }
        .clssub{
            font-weight: 800;
            display: flex;
        }
        tr{
            display: flex;
        }
        label{
            margin-left: 6px;
            padding: 10px;
        }
        .clsin{
            margin-right: 10px;
        }
        .clsbody{
            font-weight: 800;
        }
        .clsbtn{
            margin-top: 15px;
        }
        .material-icons{
            font-size: 20px;
        }
    </style>
    <body class="theme-orange">
        <?php
        include 'navbar.php';
        ?>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
        </aside>
        <!-- Main Content -->
    <center>
        <div class ="form">
            <div class="clsdiv">
                <h5> Sales Tax Calculator</h5>
                <input type="number" id="size" placeholder="Size" />
                <button id="tableMaker" type="button">Generate Products</button>
                <br/>
                <div class="container">
                </div>
            </div>
            <div>
                <form name="orderform" id="orderform">
                    <table class="clstable" id="clstable">
                        <tbody class="clsbody">
                            <tr class="clstr">
                                <th><label>Product: </label></th>
                                <th class="clsqu"><label>Quantity: </label></th>
                                <th><label>Subtotal: </label></th>
                                <th><label>Tax: </label></th>
                                <th><label>Total: </label></th>
                            </tr>
                        </tbody>
<!--                        <tbody class="clsbody">

                        </tbody>-->
                    </table>
                    <div class="clsbtn">
                        <input name="subBtn" onchange="subTotal();" type="button" id="subBtn" value="Subtotal">
                        <input name="taxBtn" onchange="calculateTax();" type="button" id="taxBtn" value="Tax">
                        <input name="gtotalBtn" onchange="grandTotal();" type="button" id="gtotalBtn" value="Grand Total">
                    </div>
                </form>      
            </div>
        </div>
    </center>
</section>
<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/jvectormap.bundle.js"></script>
<!-- JVectorMap Plugin Js -->
<script src="assets/bundles/morrisscripts.bundle.js"></script>
<!-- Morris Plugin Js -->
<script src="assets/bundles/sparkline.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
<script>
//                            $(document).ready(function () {
                            $("#tableMaker").click(function () {
                                $('.container').html("");
                                $('.container').append("<div class='clsproduct'></div>");
                                $('table').append("<tr class='clstrs'></tr>");
                                var str = 0;
                                for (var j = 0; j < $('#size').val(); j++) {
                                    str += '<tr class="clstrs">';
                                    str += "<td><input name='price' type='text' id='price' class='price' size='8'/></td>";
                                    str += "<td><input name='quantity' type='text' id='quantity' class='quantity' size='8'/></td>";
                                    str += "<td><input name='subtotal' type='text' id='subtotal' onFocus='this.form.elements[1].focus()' size='8'/></td>";
                                    str += "<td><input name='salestax' type='text' id='salestax' onFocus='this.form.elements[1].focus()' size='8'/></td>";
                                    str += "<td><input name='gtotal' type='text' id='gtotal' onFocus='this.form.elements[2].focus()' size='8'/><button class='clsremove'>x</button></td>";
                                    str += '</tr>';
                                }
                                $('.clsbody tr:last').after(str);
                            });
                            $("#clstable .clsremove").click(function () {
                                $(this).closest('td').remove();
                            });
                            $(document).on('change', '.price', function () {
                                console.log("price on change");
                                subTotal();
                                calculateTax();
                                grandTotal();
                            });
                            $(document).on('change', '.quantity', function () {
                                console.log("price on change");
                                subTotal();
                                calculateTax();
                                grandTotal();
                            });
//                            $(".price").on("change", function () {
//                                console.log("price on change");
//                                subTotal();
//                                calculateTax();
//                                grandTotal();
//                            });
//                            $(".quantity").on("change", function () {
//                                console.log("price on change");
//                                subTotal();
//                                calculateTax();
//                                grandTotal();
//                            });
//                            });
                            function subTotal() {
                                var sum = 0;
                                var clssum = 0;
                                var price = document.orderform.price.value;
                                var quantity = document.orderform.quantity.value;
                                $(".clstable").each(function () {
                                    $("#price").each(function () {
                                        sum += +$(this).val();
                                    });
                                    console.log(sum);
                                    $("#quantity").each(function () {
                                        clssum += +$(this).val();
                                    });
                                    console.log(clssum);
                                });
                                productPrice = sum * clssum;
                                document.orderform.subtotal.value = productPrice.toFixed(2);
                                return productPrice;
                            }
//calculateTax() takes result of subTotal function but has to refer to the result to move forward as opposed to the previous function
//.toFixed() is the decimal points
                            function calculateTax() {
                                //var subTotal = document.orderform.subtotal.value; OR for dryer code:
                                var subtotal = subTotal();
                                var stax = 0.25;
                                tax = subtotal * stax;
                                console.log(tax);
                                document.orderform.salestax.value = tax.toFixed(3);
                                return tax;
                            }
//takes the HTML output results from the previous two functions and adds them together. 
                            function grandTotal() {
                                var subtotal = subTotal();
                                var tax = calculateTax();
                                document.orderform.subtotal.value = subtotal.toFixed(2);
                                document.orderform.salestax.value = tax.toFixed(2);
                                var gtotal = subtotal + tax;
                                console.log(gtotal);
                                document.orderform.gtotal.value = gtotal.toFixed(2);
                            }
</script>
</html>
