<?php
include('../connection.php');
// if ($_SERVER['SERVER_NAME'] == 'localhost') {
//     $con = mysqli_connect('localhost', 'root', '', 'happyeventsurat');
// }elseif ($_SERVER['SERVER_NAME'] == 'www.happyeventsurat.com') {
//     $con = mysqli_connect('localhost', 'u402017191_image_gallery', 'Image_gallery@99', 'u402017191_image_gallery');
// }
include('smtp/PHPMailerAutoload.php');
class Register
{

    // public $host = "localhost";
    // public $user = "u402017191_image_gallery";
    // public $password = "Image_gallery@99";
    // public $db = "u402017191_image_gallery";
    // public $host = "localhost";
    // public $user = "root";
    // public $password = "";
    // public $db = "image_gallery";
    // var $connect_db;

    function __construct()
    {

        // $this->connect_db = new mysqli($this->host, $this->user, $this->password, $this->db);
        $db_connection = new DB_Class();
        $this->connect_db = $GLOBALS['conn'];
        if (mysqli_connect_errno()) {
            printf("Connection failed:", mysqli_connect_error());
            exit();
        }
        return true;
    }

    function clssignin()
    {
        session_start();
        $useremail = $_POST['email'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM image_login WHERE email='$useremail' AND  password='$password'";
        $result = $this->connect_db->query($sql);
        $ud = $result->fetch_assoc();
        $total_records = mysqli_num_rows($result);
        if ($total_records > 0) {
            $_SESSION['id'] = $ud['id'];
            $_SESSION['email'] = $ud['email'];
            $_SESSION['password'] = $ud['password'];
            $response["msg"] = "success";
            $response["data"] = "Login Successfully";
        } else {
            $response["msg"] = "fail";
            $response["data"] = "Invalid Username & Password";
        }
        echo json_encode($response);
    }

    function set_image()
    {
        $response = array();
        $ctgryid = $_POST['value'];
        $i = 1;
        $clsout = '';
        $record_per_page = 24;
        $page = '';
        $output = '';
        if (isset($_POST["page"])) {
            $page = $_POST["page"];
        } else {
            $page = 1;
        }
        $start_from = ($page - 1) * $record_per_page;
        if ($ctgryid == '100') {
            $query = "SELECT * FROM cls_image ORDER BY status =1, id DESC LIMIT $start_from, $record_per_page";
        } else {
            $query = "SELECT * FROM cls_image where category='$ctgryid' AND status = '1' LIMIT $start_from, $record_per_page";
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $category = $row['category'];
            $cate = ($category == 1) ? "Babyshower/" : (($category == 2) ? "BabyWelcome/" : (($category == 3) ? "BirthdayDecoration/" : (($category == 4) ? "NamingCeremony/" : (($category == 5) ? "RoomDecoration/" : (($category == 6) ? "ThemeDecoration/" : (($category == 7) ? "HaldiCeremony/" : (($category == 8) ? "BridetobeDecoration/" : (($category == 9) ? "EngagementDecoration/" : (($category == 10) ? "ShopInauguration/" : (($category == 11) ? "CorporateEvent/" : (($category == 12) ? "SurprisePlanning/" : (($category == 13) ? "SliderImages/" : (($category == 14) ? "PhotoGallery/" : (($category == 15) ? "OurServices/" : (($category == 16) ? "BottomSlider/" : (($category == 17) ? "Anniversary/" : (($category == 18) ? "NationalFestival/" : (($category == 20) ? "SliderImagesformobile/" : "none"))))))))))))))))));

            $id = $row['id'];
            $image = $row['file'];
            $imagefull = "$cate$image";
            $output .= "<div class='clsdels' id='clsdels" . $id . "'>
            <img class='clsprev' src='assets/images/" . $imagefull . "' height='220' width='220' alt='" . $row['image_alt'] . "'/>
            <button class='btn  btn-raised btn-danger waves-effect clsremove' data-id='" . $id . "'>X</button>
            </div>";
        };
        $output_page = "<div class='page_center'>";
        if ($ctgryid == '100') {
            $page_query = "SELECT * FROM cls_image WHERE status='1' ORDER BY id DESC";
        } else {
            $page_query = "SELECT * FROM cls_image where category='$ctgryid' AND status='1'";
        }
        $page_result = mysqli_query($this->connect_db, $page_query);
        $total_records = mysqli_num_rows($page_result);
        if ($total_records == 0) {
            $response['result'] = "Fail";
            $response['error'] = "Data Not Found";
        } else if ($total_records > $record_per_page) {
            $total_pages = ceil($total_records / $record_per_page);
            for ($i = 1; $i <= $total_pages; $i++) {
                $output_page .= '<li class="page-item clsCls' . $i . '" aria-current="page">
                <span class="page-link pgntn_link" id="' . $i . '">' . $i . '</span>
            </li>';
            }
            $output_page .= "</div>";
            $response["result"] = "success";
            $response["msg"] = "success";
        }
        $response["data"] = $output;
        $response["pagination"] = $output_page;
        echo json_encode($response);
    }

    function clsdelete_data()
    {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "update cls_image set status ='0' where id=$clsid";
        // $clsdelete = "DELETE  FROM cls_image WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }
    function clsdelete_data_services()
    {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "update our_services set status ='0' where id=$clsid";
        // $clsdelete = "DELETE  FROM image_data WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }
    function clsdelete_data_review()
    {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "update client_review set status ='0' where id=$clsid";
        // $clsdelete = "DELETE  FROM image_data WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }
    function edit_data_services_fun()
    {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $service_name = $_POST['title'];
        $url = $_POST['url'];
        $images = $_POST['image'];
        $clsdelete = "update our_services set service_name =$service_name,url=$url,images=$images where id=$clsid";
        // $clsdelete = "DELETE  FROM image_data WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Edit Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Edit Fail! ";
        }
        echo json_encode($deletemsg);
    }
    function clsimage()
    {
        $response = array();
        $error_array = array();
        $t = time();
        // echo"<pre>";
        // print_r($_POST);
        // print_r($_FILES);
        // print_r($_FILES['image']['name']);

        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
            $file_name = $_FILES['image']['name'];
        } else {
            $error_array["image"] = "Image is Require!";
        }
        if (isset($_POST['category']) && $_POST['category'] != "") {
            $category = $_POST['category'];
        } else {
            $error_array["category"] = "Category is Require!";
        }
        if (isset($_POST['alt_name']) && $_POST['alt_name'] != "") {
            $image_alt = $_POST['alt_name'];
        } else {
            $error_array["alt_name"] = "Image alt Name is Require!";
        }
        $count = count($file_name);
        if ($count < 16) {
        } else {
            $error_array["size"] = "You Only add 15 number of Image at Time !";
        }
        if (!empty($error_array)) {
            $response["result"] = "fail";
            $response["error"] = $error_array;
        } else {
            if ($category == '1') {
                $categorys = "Baby shower";
            } elseif ($category == '2') {
                $categorys = "Baby Welcome";
            } elseif ($category == '3') {
                $categorys = "Birthday Decoration";
            } elseif ($category == '4') {
                $categorys = "Naming Ceremony";
            } elseif ($category == '5') {
                $categorys = "Room Decoration";
            } elseif ($category == '6') {
                $categorys = "Theme Decoration";
            } elseif ($category == '7') {
                $categorys = "Haldi Ceremony";
            } elseif ($category == '8') {
                $categorys = "Bride to be Decoration";
            } elseif ($category == '9') {
                $categorys = "Engagement Decoration";
            } elseif ($category == '10') {
                $categorys = "Shop Inauguration";
            } elseif ($category == '11') {
                $categorys = "Corporate Event";
            } elseif ($category == '12') {
                $categorys = "Surprise Planning";
            } elseif ($category == '13') {
                $categorys = "Slider Images";
            } elseif ($category == '14') {
                $categorys = "Photo Gallery";
            } elseif ($category == '15') {
                $categorys = "Our Services";
            } elseif ($category == '16') {
                $categorys = "Bottom Slider";
            } elseif ($category == '17') {
                $categorys = "Anniversary";
            } elseif ($category == '18') {
                $categorys = "National Festival";
            } elseif ($category == '20') {
                $categorys = "Slider Images for mobile";
            }
            foreach ($_FILES['image']["name"] as $key => $value) {
                $src = $_FILES['image']['tmp_name'][$key];
                $dest = $_FILES['image']['name'][$key];

                $imgType = $_FILES["image"]["type"][$key];

                $imageFileType = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
                if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif" || $imageFileType == "bmp" || $imageFileType == "webp") {
                    // $imgType = $_FILES["image"]["type"];
                    $newname = str_replace('image/', '', $imgType);
                    $imageName = $_FILES["image"]["name"] = $categorys . '_' . $t . '.' . $newname;
                    // print_r($imageName);
                    // echo'<br>';
                    $replace = str_replace(' ', '', $categorys);
                    $upload = move_uploaded_file($src, "assets/images/" . $replace . "/" . $imageName);
                    // die;
                    $sql = "INSERT INTO cls_image (id,file,category,image_alt) VALUES (' ','$imageName','$category','$image_alt')";
                    if ($this->connect_db->query($sql) === TRUE) {
                        $response["result"] = "success";
                        $response["msg"] = "Uploaded Images Successfully! ";
                    } else {
                        $response["result"] = "fail";   
                        $response["msg"] = "Upload Image Fail! ";
                    }
                } else {
                    $response["msg"] = "Please Insert jpg, jpeg, gif, bmp, webp and png Files!";
                    $response["result_image"] = "Image Is Require !";
                }
            }
        }
        
        echo json_encode($response);
    }

    function reviewform()
    {
        $response = array();
        $error_array = array();
        // echo"<pre>";
        // print_r($_POST);
        // print_r($_FILES);
        // print_r($_FILES["image"]["name"] );
        // print_r($_FILES["image"]["name"] = "10.jpg");

        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
            $file_name = $_FILES['image']['name'];
        } else {
            $error_array["image"] = "Image is Require!";
        }
        if (isset($_POST['category']) && $_POST['category'] != "") {
            $category = $_POST['category'];
        } else {
            $error_array["category"] = "Category is Require!";
        }
        if (isset($_POST['name']) && $_POST['name'] != "") {
            $name = $_POST['name'];
        } else {
            $error_array["name"] = "Name is Require!";
        }
        if (isset($_POST['description']) && $_POST['description'] != "") {
            $description = $_POST['description'];
        } else {
            $error_array["description"] = "Description is Require!";
        }
        if (!empty($error_array)) {
            $response["result"] = "fail";
            $response["error"] = $error_array;
        } else {
            $src = $_FILES['image']['tmp_name'];
            $dest = $_FILES['image']['name'];
            $imageFileType = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
            if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif" || $imageFileType == "bmp") {
                move_uploaded_file($src, "assets/images/" . $dest);
                $sql = "INSERT INTO `client_review`(
                        `name`,
                        `short_description`,
                        `long_description`,
                        `image`
                    )
                    VALUES(
                        '$name',
                        '$category',
                        '$description',
                        '$file_name'
                    )";
                if ($this->connect_db->query($sql) === TRUE) {
                    $response["result"] = "success";
                    $response["msg"] = "Review Added Successfully! ";
                } else {
                    $response["result"] = "fail";
                    $response["msg"] = "Upload Image Fail! ";
                }
            } else {
                $response["msg"] = "Please Insert jpg, jpeg, gif, bmp and png Files!";
            }
        }
        echo json_encode($response);
    }

    function bottomslider()
    {
        $response = array();
        $error_array = array();
        $t = time();
        // echo"<pre>";
        // print_r($_POST);
        // print_r($_FILES);
        // print_r($_FILES["image"]["name"]);
        // echo'<br>';
        // print_r($_FILES["image"]["type"]);
        // $imgType = $_FILES["image"]["type"];
        // $newname = str_replace('image/','',$imgType);
        // echo'<br>';
        // echo $newname;
        // echo'<br>';
        // print_r($_FILES["image"]["name"] = $_POST['title'].'.'.$t.'.'.$newname);
        // $imageName = $_FILES["image"]["name"] = $_POST['title'].'.'.$t.'.'.$newname;
        // echo'<br>';
        // echo $imageName;
        // die;
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "") {
            $imgType = $_FILES["image"]["type"];
            $newname = str_replace('image/', '', $imgType);
            $imageName = $_FILES["image"]["name"] = $_POST['title'] . '.' . $t . '.' . $newname;
        } else {
            $error_array["image"] = "Image is Require!";
        }
        if (isset($_POST['shortdescription']) && $_POST['shortdescription'] != "") {
            $shortdescription = $_POST['shortdescription'];
        } else {
            $error_array["shortdescription"] = "Short Description is Require!";
        }
        if (isset($_POST['title']) && $_POST['title'] != "") {
            $title = $_POST['title'];
        } else {
            $error_array["title"] = "Title is Require!";
        }
        if (isset($_POST['longdescription']) && $_POST['longdescription'] != "") {
            $longdescription = $_POST['longdescription'];
        } else {
            $error_array["longdescription"] = "Long Description is Require!";
        }
        if (!empty($error_array)) {
            $response["result"] = "fail";
            $response["error"] = $error_array;
        } else {
            $src = $_FILES['image']['tmp_name'];
            $dest = $_FILES['image']['name'];
            $imageFileType = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
            if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif" || $imageFileType == "bmp") {
                move_uploaded_file($src, "assets/images/BottomSlider/" . $imageName);
                $sql = "INSERT INTO `bottom_slider`(
                        `title`,
                        `short_description`,
                        `long_description`,
                        `image`
                    )
                    VALUES(
                        '$title',
                        '$longdescription',
                        '$longdescription',
                        '$imageName'
                    )";
                if ($this->connect_db->query($sql) === TRUE) {
                    $response["result"] = "success";
                    $response["msg"] = "Bottom Slider Added Successfully! ";
                } else {
                    $response["result"] = "fail";
                    $response["msg"] = "Upload Image Fail! ";
                }
            } else {
                $response["msg"] = "Please Insert jpg, jpeg, gif, bmp and png Files!";
            }
        }
        echo json_encode($response);
    }
    function our_services_fun()
    {
        $response = array();
        $error_array = array();
        $t = time();
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != "" && $_FILES['image']['name'] != "") {
            $imgType = $_FILES["image"]["type"];
            $newname = str_replace('image/', '', $imgType);
            $imageName = $_FILES["image"]["name"] = $_POST['title'] . '.' . $t . '.' . $newname;
        } else {
            if($_POST['services_new_name'] =="" || $_FILES['image']['name'] != ""){
                $error_array["image"] = "Image is Require!";
            }
            
        }
        if (isset($_POST['url']) && $_POST['url'] != "") {
            $url = $_POST['url'];
        } else {
            $error_array["url"] = "URL is Require!";
        }
        if (isset($_POST['title']) && $_POST['title'] != "") {
            $title = $_POST['title'];
        } else {
            $error_array["title"] = "Title is Require!";
        }
        if (!empty($error_array)) {
            $response["result"] = "fail";
            $response["error"] = $error_array;
        } else {
            if(isset($_POST['services_new_name']) && $_POST['services_new_name'] !="" && $_FILES['image']['name'] == "" ){
                $imageName=$_POST['services_new_name'];
            }
            $src = $_FILES['image']['tmp_name'];
            $dest = $_FILES['image']['name'];
            $imageFileType = strtolower(pathinfo($dest, PATHINFO_EXTENSION));
            if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif" || $imageFileType == "bmp" || $imageFileType == "webp" || $_POST['services_new_name'] !="") {
                move_uploaded_file($src, "assets/images/OurServices/" . $imageName);
                $sql = "INSERT INTO `our_services`(
                        `service_name`,
                        `url`,
                        `images`
                    )
                    VALUES(
                        '$title',
                        '$url',
                        '$imageName'
                    )";
                if (isset($_POST['services_id']) && $_POST['services_id'] != "") {
                    $services_id = $_POST['services_id'];
                    $sql = "update our_services set service_name ='$title',url ='$url',images='$imageName' where id='$services_id'";
                }

                if ($this->connect_db->query($sql) === TRUE) {
                    $response["result"] = "success";
                    $response["msg"] = "Our Servicesr Added Successfully! ";
                    if (isset($_POST['services_id']) && $_POST['services_id'] != "") {
                        $response["msg"] = "Our Services Update Successfully! ";
                    }
                } else {
                    $response["result"] = "fail";
                    $response["msg"] = "Upload Image Fail! ";
                }
            } else {
                $response["msg"] = "Please Insert jpg, jpeg, gif, bmp, webp and png Files!";
            }
        }
        echo json_encode($response);
    }
    function get_services_data()
    {

        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : '';
        $clsselect = "select * from our_services where id=" . $clsid;
        $cls = $this->connect_db->query($clsselect);
        $row = $cls->fetch_assoc();
        echo json_encode($row);
    }

    function set_services_image()
    {
        $output = '';
        $titleourServices = '';
        $query = "SELECT * FROM our_services where status='1'";

        $result = mysqli_query($this->connect_db, $query);
        $i = 0;
        while ($row = mysqli_fetch_array($result)) {
            $image = $row['images'];
            $url = $row['url'];
            $image = $row['images'];
            $output .= "<div class='width19'>
                    <div class='services-item newservices'>
                        <img class='services-img' src='../admin/assets/images/OurServices/" . $image . "' alt='Wedding planner, balloon decoration, wedding planner, room decoration'>
                            <div class='services-caption caption_ser'>
                                    <button class='btn  btn-raised btn-danger waves-effect clsremove_services sp_class_b' data-id='" . $row["id"] . "'><i class='fa fa-times' aria-hidden='true'></i></button>
                                    <button class='btn  btn-raised btn-danger waves-effect edit_services sp_class_b' data-id='" . $row["id"] . "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button>
                            </div>
                    </div>
                </div>";
            $i++;
        }
        $response['result'] = "success";
        $response['data'] = $output;
        $response['msg'] = "Update Image Successfully";
        echo json_encode($response);
    }
    function list_all_review_fun()
    {
        $output = '';
        $titleourServices = '';
        $query = "SELECT * FROM client_review where status='1'";



        $result = mysqli_query($this->connect_db, $query);
        $i = 0;
        while ($row = mysqli_fetch_array($result)) {
            $name = $row['name'];
            $long_description = $row['long_description'];
            $output .= " <div class='width19'><div class='services-item newservices box_reiwes'>
                    
            <h3 class='heading_h'>" . $name . "</h3>
            <p class='para_revies'>" . $long_description . "'</p>
                <div class='services-caption caption_ser'>
                        <button class='btn  btn-raised btn-danger waves-effect clsremove_review sp_class_b' data-id='" . $row["id"] . "'><i class='fa fa-times' aria-hidden='true'></i></button>
                </div>
        </div>
        </div>";
            $i++;
        }
        $response['result'] = "success";
        $response['data'] = $output;
        $response['msg'] = "Reviews  Successfully Feach";
        echo json_encode($response);
    }

    //for Multistep form
    function insert_data()
    {
        $account_id = (isset($_POST['account_id']) && $_POST['account_id'] != "") ? $_POST['account_id'] : '';
        $response = array();
        if ($account_id == '') {
            $error_array = array() == "";
            if (isset($_POST['order_by']) && $_POST['order_by'] != "") {
                $order_by = $_POST['order_by'];
            } else {
                $error_array .= "Order by is Require! ,";
            }
            if (isset($_POST['date']) && $_POST['date'] != "") {
                $date = $_POST['date'];
            } else {
                $error_array .= "Date is Require! ,";
            }
            if (isset($_POST['time']) && $_POST['time'] != "") {
                $time = $_POST['time'];
            } else {
                $error_array .= "Time is Require! ,";
            }
            if (isset($_POST['cls_type']) && $_POST['cls_type'] != "") {
                $type = $_POST['cls_type'];
            } else {
                $error_array .= "Type is Require! ,";
            }
            if (isset($_POST['client_name']) && $_POST['client_name'] != "") {
                $client_name = $_POST['client_name'];
            } else {
                $error_array .= "Client Name is Require! ,";
            }
            if (isset($_POST['client_num']) && $_POST['client_num'] != "") {
                $client_num = $_POST['client_num'];
            } else {
                $error_array .= "Client Number is Require! ,";
            }
            if (isset($_POST['advance_amount']) && $_POST['advance_amount'] != "") {
                $advance_amount = $_POST['advance_amount'];
            } else {
                $error_array .= "Advance Amount is Require! ,";
            }
            if (isset($_POST['remain']) && $_POST['remain'] != "") {
                $remain = $_POST['remain'];
            } else {
                $error_array .= "Remain Amount is Require! ,";
            }
            if (isset($_POST['total_amount']) && $_POST['total_amount'] != "") {
                $total_amount = $_POST['total_amount'];
            } else {
                $error_array .= "Total Amount is Require!";
            }
            $destination_array = explode(',', $error_array);
            if (!empty($error_array)) {
                $response["result"] = "fail";
                $response["data"] = $destination_array;
            } else {
                if ($_POST["id"] == '') {
                    $order_num = $_POST['order_num'];
                    $address = $_POST['address'];
                    $sql = "INSERT INTO multistep_form (id, order_num, order_by, date, time, type, client_name, client_contact, advance_amount, remain_amount, total_amount, address) VALUES "
                        . "('', '$order_num', '$order_by', '$date', '$time', '$type', '$client_name', $client_num, $advance_amount, $remain, $total_amount, '$address')";
                    if ($this->connect_db->query($sql) === TRUE) {
                        $response["result"] = "success";
                        $response["msg"] = "Insert Successfully! ";
                        $select = "select * from multistep_form where order_num ='$order_num'";
                        $result = $this->connect_db->query($select);
                        $row = mysqli_fetch_array($result);
                        $formid = $row['id'];
                        $_SESSION['id'] = $formid;
                        $response["account_id"] = $formid;
                    } else {
                        $response["result"] = "fail";
                        $response["msg"] = "Insert Fail! ";
                    }
                }
            }
        } else {
            $foil_ball = array();
            $ball_color = $_POST['ball_color'];
            //            $foil_ball = array(
            //                "foil_input" => $_POST['foil_ball'],
            //                "foil_qty" => $_POST['foil_ball_qty'],
            //                "foil_color" => $_POST['foil_ball_color']
            //            );
            $foil_ball = $_POST['foil_ball'] . ', ' . $_POST['foil_ball_qty'] . ', ' . $_POST['foil_ball_color'];
            $gate = $_POST['gate'];
            $backdrop = $_POST['backdrop'];
            $banner = $_POST['banner_select'];
            $pillar = $_POST['pillar_lable'] . ', ' . $_POST['pillar_qty'];
            $props = $_POST['props'] . ', ' . $_POST['props_select'];
            $stage = $_POST['stage'] . ', ' . $_POST['stage_input'];
            $sofa = $_POST['sofa'];
            $fog_machine = $_POST['fog'];
            $matka_qty = $_POST['matka_qty'];
            $payro_qty = $_POST['payro_qty'];
            $light_qty = $_POST['light_qty'];
            $other_info = $_POST['other_info'];
            $customer_img_name = $_FILES['customer_img']['name'];
            $customer_img_tmp_name = $_FILES['customer_img']['tmp_name'];
            $upload = move_uploaded_file($customer_img_tmp_name, "assets/images/" . $customer_img_name);
            $clsup = "update multistep_form set balloon_colors='$ball_color' ,foil_balloon='$foil_ball' ,gate='$gate', backdrop='$backdrop', Banner='$banner', pillar='$pillar', props='$props', stage='$stage', sofo='$sofa', fog_matka='$matka_qty', fog_machine='$fog_machine', cold_payro='$payro_qty', par_light='$payro_qty', other_info='$other_info', customer_image='$customer_img_name' where id='$account_id'";
            if ($this->connect_db->query($clsup) === TRUE) {
                $select = "select * from multistep_form where id ='$account_id'";
                $result = $this->connect_db->query($select);
                $row = mysqli_fetch_array($result);
                $formid = $row['id'];
                $_SESSION['id'] = $formid;
                $response["account_id"] = $formid;
                $response["result"] = "success";
                $response["msg"] = "Update Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Update Fail! ";
            }
        }
        echo json_encode($response);
    }

    //multiple order
    function set_data()
    {
        $response = array();
        $filter_check = false;
        $i = $page = 1;
        $clsout = $output = '';
        $limit = 10;
        $start = 0;
        if (isset($_POST["page"]) && $_POST["page"] != "") {
            $start = (($_POST['page'] - 1) * $limit);
            $page = $_POST["page"];
        }
        if (isset($_POST['call_from']) && $_POST['call_from'] == "on_load") {
            $query = "SELECT * FROM multistep_form ORDER BY status = 1, id DESC LIMIT $start, $limit";
        } else if (isset($_POST['call_from']) && ($_POST['call_from'] == "on_search" || $_POST['call_from'] == "on_date_filter")) {
            $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%" OR date LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
            $response["pagination"] = "";
        } else if (isset($_POST['call_from']) && $_POST['call_from'] == "on_status_change") {
            $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
            $response["pagination"] = "";
        }

        if (isset($_POST["filter_data"]) && !empty($_POST["filter_data"])) {
            foreach ($_POST["filter_data"] as $key => $value) {
                if ($value != "") {
                    $filter_check = true;
                }
            }
            if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] == "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] == "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] == "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] == "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] == "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "" && isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%"';
                $response["pagination"] = "";
            } else if (isset($_POST["filter_data"]["serach_value"]) && $_POST["filter_data"]["serach_value"] != "" && isset($_POST["filter_data"]["date_value"]) && $_POST["filter_data"]["date_value"] != "" && isset($_POST["filter_data"]["status_value"]) && $_POST["filter_data"]["status_value"] != "") {
                $query = 'SELECT * FROM multistep_form WHERE status = "1" AND order_num LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["serach_value"]) . '%" AND date LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["date_value"]) . '%" AND order_status LIKE "%' . str_replace(' ', '%', $_POST["filter_data"]["status_value"]) . '%"';
                $response["pagination"] = "";
            }
        }

        if (!isset($_POST['filter_data']) || $filter_check == false) {
            $query = "SELECT * FROM multistep_form ORDER BY status = 1, id DESC LIMIT $start, $limit";
            $page_query = "SELECT * FROM multistep_form ORDER BY id DESC";
            $page_result = mysqli_query($this->connect_db, $page_query);
            $total_data = mysqli_num_rows($page_result);
            $total_links = ceil($total_data / $limit);
            $previous_link = $next_link = $page_link = $output_page = '';
            $page_array = array();
            if ($total_links > 4) {
                if ($page < 5) {
                    for ($count = 1; $count <= 5; $count++) {
                        $page_array[] = $count;
                    }
                    $page_array[] = '...';
                    $page_array[] = $total_links;
                } else {
                    $end_limit = $total_links - 5;
                    if ($page > $end_limit) {
                        $page_array[] = 1;
                        $page_array[] = '...';
                        for ($count = $end_limit; $count <= $total_links; $count++) {
                            $page_array[] = $count;
                        }
                    } else {
                        $page_array[] = 1;
                        $page_array[] = '...';
                        for ($count = $page - 1; $count <= $page + 1; $count++) {
                            $page_array[] = $count;
                        }
                        $page_array[] = '...';
                        $page_array[] = $total_links;
                    }
                }
            } else {
                for ($count = 1; $count <= $total_links; $count++) {
                    $page_array[] = $count;
                }
            }
            for ($count = 0; $count < count($page_array); $count++) {
                if ($page == $page_array[$count]) {
                    $previous_id = $page_array[$count] - 1;
                    $page_link .= '<li class="page-item  active clsClass"><a class="page-link " href="javascript:void(0)">' . $page_array[$count] . ' <span class="sr-only pagination_link" id="' . $page_array[$count] . '">(current)</span></a>
                                    </li>';
                    if ($previous_id > 0) {
                        $previous_link = '<li class="page-item clsClass"><a class="page-link pagination_link" id="' . $previous_id . '"  href="javascript:void(0)" data-page_number="' . $previous_id . '">Previous</a></li>';
                    } else {
                        $previous_link = '<li class="page-item disabled"><a class="page-link pagination_link" id="' . $previous_id . '" href="javascript:void(0)">Previous</a>
                                            </li>';
                    }
                    $next_id = $page_array[$count] + 1;
                    if ($next_id > $total_links) {
                        $next_link = '<li class="page-item disabled clsClass"><a class="page-link pagination_link" id="' . $next_id . '" href="#">Next</a></li>';
                    } else {
                        $next_link = '<li class="page-item clsClass"><a class="page-link pagination_link" id="' . $next_id . '" href="javascript:void(0)" data-page_number="' . $next_id . '">Next</a></li>';
                    }
                } else {
                    if ($page_array[$count] == '...') {
                        $page_link .= '<li class="page-item disabled clsClass"><a class="page-link pagination_link" id="' . $page_array[$count] . '" href="#">...</a>
                                                    </li>';
                    } else {
                        $page_link .= '<li class="page-item clsClass"><a class="page-link pagination_link" id="' . $page_array[$count] . '" href="javascript:void(0)" data-page_number="' . $page_array[$count] . '">' . $page_array[$count] . '</a></li>';
                    }
                }
            }
            if ($total_data > 10) {
                $output_page .= $previous_link . $page_link . $next_link;
            }
            $response["pagination"] = $output_page;
        } else {
            $response["pagination"] = "";
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $id = $row['id'];
            $slNo = $i + $start;
            $selected_status = $row['order_status'];
            $status_selected = "selected='selected'";
            $selectd_bg_color = ($selected_status == "0") ? 'panding_color' : (($selected_status == "1") ? 'complete_color' : (($selected_status == "2") ? 'cancel_color' : 'done_color'));
            $selection_html = "<select class='show-tick show_order_status $selectd_bg_color'  name='clsorder_status'>
                            <option value='0' data-id=" . $id . " name='clsorder_status' class='panding_color' " . ($selected_status == '0' ? $status_selected : '') . ">Panding</option>
                            <option value='1' data-id=" . $id . " name='clsorder_status' class='complete_color' " . ($selected_status == '1' ? $status_selected : '') . ">Complete</option>
                            <option value='2' data-id=" . $id . " name='clsorder_status' class='cancel_color' " . ($selected_status == '2' ? $status_selected : '') . ">Cancel</option>
                            <option value='3' data-id=" . $id . " name='clsorder_status' class='done_color' " . ($selected_status == '3' ? $status_selected : '') . ">Done</option>
                    </select>";
            $output .= "<tr id=tr_" . $id . ">   
                <td><input type='checkbox' class='muldel' name='checkbox[]' id=" . $id . "></td>
                <td>" . $slNo . "</td>
                <td>" . $row["order_num"] . "</td>
                <td class='clsme'>" . $row["date"] . "</td>
                <td class='clsme'>" . $row["time"] . "</td>
                <td class='clsme'>" . $row["client_contact"] . "</td>
                <td class='clsme'>" . $row["advance_amount"] . "</td>
                <td class='clsme'>" . $row["remain_amount"] . "</td>
                <td class='clsme'>" . $row["total_amount"] . "</td>
                <td class='btn_td'><a class='btn btn-danger clsdelete btn-outline-dark' data-id='" . $id . "'><i class='fa fa-trash-o' aria-hidden='true'></i></a>   
                    <a class='btn btn1 btn-outline-dark' href='multistep_form.php?id=" . $id . "'><i class='fa fa-pencil' aria-hidden='true'></i></a>
                    <a class='btn  btn-raised bg-black waves-effect waves-light clsview' href='invoice_data.php?id=" . $id . "'><i class='fa fa-eye' aria-hidden='true'></i></a>
                    " . $selection_html . "   
                </td>
            </tr>";
            $i++;
        };
        $response["result"] = "success";
        $response["msg"] = "success";
        $response["data"] = $output;
        echo json_encode($response);
    }

    function set_order_status()
    {
        $order_value = $_POST['dropdown'];
        $order_id = $_POST['id'];
        $order_status = $_POST['class'];
        $query = "update multistep_form set order_status ='$order_value' where id='$order_id'";
        if ($this->connect_db->query($query) === TRUE) {
            $response["result"] = "success";
            $response["class"] = $order_status;
            $response["msg"] = "Update Successfully! ";
        } else {
            $response["result"] = "fail";
            $response["msg"] = "Update Fail! ";
        }
        echo json_encode($response);
    }

    function multidelete()
    {
        $delete = $_POST['id'];
        $deletemsg = array();
        foreach ($delete as $id) {
            $query = "DELETE FROM multistep_form WHERE id = '" . $id . "'";
            $result = mysqli_query($this->connect_db, $query) or die("Invalid query");
        }
        if (mysqli_affected_rows($this->connect_db) > 0) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Delete Successfully";
            $deletemsg["id"] = $delete;
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail";
        }
        echo json_encode($deletemsg);
    }

    function delete_data()
    {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "DELETE  FROM multistep_form WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Fail! ";
        }
        echo json_encode($deletemsg);
    }

    function get_data()
    {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : '';
        $clsselect = "select * from multistep_form where id=" . $clsid;
        $cls = $this->connect_db->query($clsselect);
        $row = $cls->fetch_assoc();
        echo json_encode($row);
    }

    function set_order_num()
    {
        $response = '';
        $pre_number = "2022080";
        $max_number = "SELECT max(order_num) from multistep_form";
        $get_max_num = $this->connect_db->query($max_number);
        $row = mysqli_fetch_assoc($get_max_num);
        $max_order_id = $row['max(order_num)'];
        if ($max_order_id == "") {
            $max_order_id = "2022080";
            $response = $max_order_id;
        } else {
            $max_order_id++;
            $response = $max_order_id;
        }
        echo json_encode($response);
    }

    function invoice_data()
    {
        $clsoutput = $customer_html = $output = $item_html = '';
        $response = array("result" => "fail", "msg" => "Something went wrong");
        $id = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : '';
        $sql = "select * from multistep_form where id=" . $id;
        $cls = $this->connect_db->query($sql);
        $ingore_array = array("id", "order_num", "order_by", "date", "time", "type", "client_name", "client_contact", "address", "advance_amount", "total_amount", "remain_amount", "created_at", "updated_at");
        $amount_array = array("advance_amount", "remain_amount", "total_amount");
        while ($clsrow = mysqli_fetch_object($cls)) {
            foreach ($clsrow as $key => $value) {
                if (in_array($key, $ingore_array)) {
                    continue;
                } else {
                    if ($key == 'balloon_colors') {
                        $key = ucfirst($key);
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->balloon_colors . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'foil_balloon') {
                        $key = ucfirst($key);
                        $comma = ',';
                        $foil_ball = $clsrow->foil_balloon;
                        if ($foil_ball == "") {
                            $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td></td>
                        </tr>";
                        } else if (strpos($foil_ball, $comma) !== false) {
                            $explod_foil_ball = explode(",", $foil_ball);
                            $foil_deatails = $explod_foil_ball[0] . ',' . $explod_foil_ball[2];
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $foil_deatails . "</td>
                                <td class='clsme'>" . $explod_foil_ball[1] . "</td>
                            </tr>";
                        } else {
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $foil_ball . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'gate') {
                        $key = ucfirst($key);
                        $cls_gate = $clsrow->gate;
                        if ($cls_gate == '1') {
                            $cls_gate = 'Yes';
                        } else {
                            $cls_gate = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_gate . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'backdrop') {
                        $key = ucfirst($key);
                        $cls_backdrop = $clsrow->backdrop;
                        if ($cls_backdrop == '1') {
                            $cls_backdrop = 'Yes';
                        } else {
                            $cls_backdrop = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_backdrop . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'Banner') {
                        $key = ucfirst($key);
                        $cls_Banner = $clsrow->Banner;
                        if ($cls_Banner == '1') {
                            $cls_Banner = 'Baby shower';
                        } else if ($cls_Banner == '2') {
                            $cls_Banner = 'Baby welcome';
                        } else if ($cls_Banner == '3') {
                            $cls_Banner = 'Oh Baby';
                        } else if ($cls_Banner == '4') {
                            $cls_Banner = 'Its Girl';
                        } else if ($cls_Banner == '5') {
                            $cls_Banner = 'Its Boy';
                        } else if ($cls_Banner == '6') {
                            $cls_Banner = 'Happy birthday';
                        } else if ($cls_Banner == '7') {
                            $cls_Banner = 'Welcome';
                        } else if ($cls_Banner == '8') {
                            $cls_Banner = 'Anniversary';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_Banner . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'pillar') {
                        $key = ucfirst($key);
                        $comma = ',';
                        $pillar = $clsrow->pillar;
                        if ($pillar == "") {
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td></td>
                                <td></td>
                            </tr>";
                        } else if (strpos($pillar, $comma) !== false) {
                            $explod_pillar = explode(",", $pillar);
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $explod_pillar[0] . "</td>
                                <td class='clsme'>" . $explod_pillar[1] . "</td>
                            </tr>";
                        } else {
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $pillar . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'props') {
                        $key = ucfirst($key);
                        $cls_props = $clsrow->props;
                        $explod_props = explode(",", $cls_props);
                        if ($explod_props[0] == '1') {
                            $explod_props[0] = "Yes";
                            if ($explod_props[1] == '1') {
                                $explod_props[1] = 'Baby shower';
                            } else if ($explod_props[1] == '2') {
                                $explod_props[1] = 'Baby welcome';
                            } else if ($explod_props[1] == '3') {
                                $explod_props[1] = 'Oh Baby';
                            } else if ($explod_props[1] == '4') {
                                $explod_props[1] = 'Its Girl';
                            } else if ($explod_props[1] == '5') {
                                $explod_props[1] = 'Its Boy';
                            } else if ($explod_props[1] == '6') {
                                $explod_props[1] = 'Happy birthday';
                            } else if ($explod_props[1] == '7') {
                                $explod_props[1] = 'Welcome';
                            } else if ($explod_props[1] == '8') {
                                $explod_props[1] = 'Anniversary';
                            }
                            $cls_props = $explod_props[0] . ',' . $explod_props[1];
                            $item_html .= "<tr>
                                <td>" . str_replace("_", " ", $key) . "</td>
                                <td class='clsme'>" . $cls_props . "</td>
                                <td></td>
                            </tr>";
                        } else {
                            $explod_props[0] = "No";
                            $item_html .= "<tr>
                                <td>" . ucfirst($key) . "</td>
                                <td class='clsme'>" . $explod_props[0] . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'stage') {
                        $cls_stage = $clsrow->stage;
                        $explod_stage = explode(",", $cls_stage);
                        if ($explod_stage[0] == '1') {
                            $explod_stage[0] = "Yes";
                            $cls_stage = $explod_stage[0] . ',' . $explod_stage[1];
                            $item_html .= "<tr>
                                <td>" . ucfirst($key) . "</td>
                                <td class='clsme'>" . $cls_stage . "</td>
                                <td></td>
                            </tr>";
                        } else {
                            $explod_stage[0] = "No";
                            $item_html .= "<tr>
                                <td>" . ucfirst($key) . "</td>
                                <td class='clsme'>" . $explod_props[0] . "</td>
                                <td></td>
                            </tr>";
                        }
                    }
                    if ($key == 'sofo') {
                        $cls_sofo = $clsrow->sofo;
                        if ($cls_sofo == '1') {
                            $cls_sofo = 'Yes';
                        } else {
                            $cls_sofo = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . ucfirst($key) . "</td>
                            <td class='clsme'>" . $cls_sofo . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'fog_machine') {
                        $key = ucfirst($key);
                        $cls_fog_machine = $clsrow->fog_machine;
                        if ($cls_fog_machine == '1') {
                            $cls_fog_machine = 'Yes';
                        } else {
                            $cls_fog_machine = 'No';
                        }
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_fog_machine . "</td>
                            <td></td>
                        </tr>";
                    }
                    if ($key == 'fog_matka') {
                        $key = ucfirst($key);
                        $cls_fog_matka = $clsrow->fog_matka;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td class='clsme'>" . $cls_fog_matka . "</td>
                        </tr>";
                    }
                    if ($key == 'cold_payro') {
                        $key = ucfirst($key);
                        $cls_cold_payro = $clsrow->cold_payro;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td class='clsme'>" . $cls_cold_payro . "</td>
                        </tr>";
                    }
                    if ($key == 'par_light') {
                        $key = ucfirst($key);
                        $cls_par_light = $clsrow->par_light;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td></td>
                            <td class='clsme'>" . $cls_par_light . "</td>
                        </tr>";
                    }
                    if ($key == 'other_info') {
                        $key = ucfirst($key);
                        $cls_other_info = $clsrow->other_info;
                        $item_html .= "<tr>
                            <td>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $cls_other_info . "</td>
                            <td></td>
                        </tr>";
                    }
                }
            }
            foreach ($clsrow as $key => $value) {
                if ($key == 'advance_amount') {
                    $key = ucfirst($key);
                    $item_html .= "<tr><td></td></tr><tr>
                            <td></td>
                            <td class='clsme'>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->advance_amount . "</td>
                        </tr>";
                }
                if ($key == 'remain_amount') {
                    $key = ucfirst($key);
                    $item_html .= "<tr>
                            <td></td>
                            <td class='clsme'>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->remain_amount . "</td>
                        </tr>";
                }
                if ($key == 'total_amount') {
                    $key = ucfirst($key);
                    $item_html .= "<tr>
                            <td></td>
                            <td class='clsme'>" . str_replace("_", " ", $key) . "</td>
                            <td class='clsme'>" . $clsrow->total_amount . "</td>
                        </tr>";
                }
            }
            $customer_html .= "<tr>
                <td>" . $clsrow->order_num . "</td>
                <td>" . ucfirst($clsrow->client_name) . "</td>
                <td>" . $clsrow->client_contact . "</td>
                <td class='customer_address'>" . $clsrow->address . "</td>
                <td class='order_date'>" . $clsrow->date . "</td>
                <td>" . $clsrow->time . "</td>
            </tr>";
            $customer_mobile_html = "<tr><th>Order Number</th><td>" . $clsrow->order_num . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Customer Name</th><td>" . ucfirst($clsrow->client_name) . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Customer Number</th><td>" . $clsrow->client_contact . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Address</th><td class='customer_address'>" . $clsrow->address . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Date</th><td>" . $clsrow->date . "</td></tr>";
            $customer_mobile_html .= "<tr><th>Time</th><td>" . $clsrow->time . "</td></tr>";
        }
        if ($customer_html != "") {
            $response["result"] = "success";
            $response["msg"] = "success";
            $response["customer_data"] = $customer_html;
            $response["customer_mobile_data"] = $customer_mobile_html;
            $response["item_data"] = $item_html;
        }
        echo json_encode($response);
    }



    //cashbook page functions
    //cashbook data insert
    function cashbook_insertdata()
    {
        $date = $_POST['date'];
        $user_name = $_POST['user'];
        $cashbook_amount = $_POST['amount'];
        $remark_amount = $_POST['remark'];
        $button_value = $_POST['cash_status'];
        $sql = "INSERT INTO cashbook_data (id,user_id,user_name,date,amount,remark,cash_status) VALUES (' ',' ','$user_name','$date',$cashbook_amount,'$remark_amount','$button_value')";
        if ($this->connect_db->query($sql) === TRUE) {
            $response["result"] = "success";
            $response["msg"] = "Insert cash book Successfully! ";
        } else {
            $response["result"] = "fail";
            $response["msg"] = "Inser cash book Fail! ";
        }
        echo json_encode($response);
    }

    function cashbook_show_data()
    {
        $data_html = '';
        if (isset($_POST['filter_data']) && $_POST['filter_data']['status_value'] != "") {
            $part_category = $_POST['filter_data']['status_value'];
            $in_amount = "select sum(amount) from cashbook_data where cash_status = '0' and user_name='$part_category'";

            $out_amount = "select sum(amount) from cashbook_data where cash_status = '1' and user_name='$part_category'";
        } else {
            $in_amount = "select sum(amount) from cashbook_data where cash_status = '0'";
            $out_amount = "select sum(amount) from cashbook_data where cash_status = '1'";
        }
        $in_result = mysqli_query($this->connect_db, $in_amount);
        $in_row = mysqli_fetch_array($in_result);
        $out_result = mysqli_query($this->connect_db, $out_amount);
        $out_row = mysqli_fetch_array($out_result);
        $total_amount = $in_row['0'] - $out_row['0'];
        $query = "SELECT * FROM cashbook_data ORDER BY id DESC";
        if (isset($_POST['call_from']) && ($_POST['call_from'] == "oncashbook_search" || $_POST['call_from'] == "oncashbook_date")) {
            $query = 'SELECT * FROM cashbook_data WHERE remark LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%" OR date LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
        } else if (isset($_POST['call_from']) && $_POST['call_from'] == "oncashbook_status") {
            $query = 'SELECT * FROM cashbook_data WHERE user_name LIKE "%' . str_replace(' ', '%', $_POST['value']) . '%"';
        }
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $user_name = $row['user_name'];
            $amount = $row['amount'];
            $date = $row['date'];
            $remark = $row['remark'];
            $cash_status = $row['cash_status'];
            if ($cash_status == '0') {
                $cash_status = "Cash In";
                $cash_color = "cash_in_color";
            } else {
                $cash_status = "Cash out";
                $cash_color = "cash_out_color";
            }
            $data_html .= "<div class='user_data_show'><span class='user_name_add'>" . $user_name . "</span><span class='user_amount_add $cash_color'>₹ " . $amount . "</span></div>
                <div class='user_date_add'><span>" . $date . "</span></div>
                <div class='user_remark_add'><span>" . $remark . "</span></div>
                <div class='user_status_add $cash_color'><span>" . $cash_status . "</span></div>";
            $response["result"] = "success";
            $response["data"] = $data_html;
        }
        $response["total_amount"] = $total_amount;
        echo json_encode($response);
    }


    //All Category page data show image
    function babyshower_image_set()
    {
        $category = $_POST['data'];
        $category_name = "SELECT category FROM category_data where id='$category'";
        $result = mysqli_query($this->connect_db, $category_name);
        $rowctgry = mysqli_fetch_array($result);
        $title = $rowctgry['category'];
        $output = '';
        $query = "SELECT * FROM cls_image where category='$category' AND status = '1' ORDER BY ID DESC";
        $result = mysqli_query($this->connect_db, $query);

        if ($result->num_rows > 0) {
            while ($row = mysqli_fetch_array($result)) {
                $id = $row['id'];
                $image = $row['file'];
                $category_n = $row['category'];
                $image_alt = $row['image_alt'];
                if ($category_n == 12) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/SurprisePlanning/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 18) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/NationalFestival/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 17) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/Anniversary/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 11) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/CorporateEvent/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 10) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/ShopInauguration/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 9) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/EngagementDecoration/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 8) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/BridetobeDecoration/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 7) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/HaldiCeremony/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 6) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/ThemeDecoration/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 5) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/RoomDecoration/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 4) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/NamingCeremony/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 3) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/BirthdayDecoration/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 2) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/BabyWelcome/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
                if ($category_n == 1) {
                    $output .= "<div class='baby " . $id . " babyimage'><img src='admin/assets/images/Babyshower/" . $image . "' alt='" . $image_alt . "' class='event_img' title='" . $title . "'></div>";
                }
            }
        } else {
            $output = "<center><span>No data found</span></center>";
        }

        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }







    //Happy event index.php page photo gallery image set
    function photo_gallery_img_set()
    {
        $start = 0;
        $limit = 9;
        $output = '';
        $titlephotoGalleryImg = '';
        $query = "SELECT * FROM cls_image where category='14' && `status` = '1' ORDER BY ID DESC LIMIT $start, $limit";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $image = $row['file'];
            $output .= "<div class='col-md-4 gallery-item'>
                    <a href='admin/assets/images/PhotoGallery/" . $image . "' title='decoration' class='img-zoom'>
                        <div class='gallery-box'>
                            <div class='gallery-img'> <img src='admin/assets/images/PhotoGallery/" . $image . "' class='img-fluid mx-auto d-block' alt='Event planner, balloon decoration, wedding planner' title='decoration'> </div>
                            <div class='gallery-detail text-center'> <i class='ti-heart'></i> </div>
                        </div>
                    </a>
                </div>";
        }
        $titlephotoGalleryImg .= '<div>
        <h2 title="gallary">Photo Gallery</h2>
        <div class="image_with_txt"></div>
        </div>';
        $response['result'] = "success";
        $response['data'] = $output;
        $response['titlephotoGalleryImg'] = $titlephotoGalleryImg;
        echo json_encode($response);
    }

    function slider_img_set()
    {
        $start = 0;
        $limit = 9;
        $output = '';
        $query = "SELECT * FROM cls_image where category='13' && `status` = '1'";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $image = rawurlencode($row['file']);
            $output .= '<div class="slider_banner ">
            <div style="background-image:url(admin/assets/images/Sliderimages/' . $image . ');" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div>';
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }
    function slider_mobile_view_img_set()
    {
        $start = 0;
        $limit = 9;
        $output = '';
        $abc = '';
        $query = "SELECT * FROM cls_image where category='20' && `status` = '1'";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $image = rawurlencode($row['file']);
            $output .= '<div class="slider_banner ">
            <div style="background-image:url(admin/assets/images/SliderImagesformobile/' . $image . ');" class="eventbg" title="event">
                <h2 class="text decor_title"></h2>
            </div>
        </div>';
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }
    function bottom_slider_img_set()
    {
        $start = 0;
        $limit = 9;
        $output = '';
        $query = "SELECT * FROM bottom_slider where status = '1'";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $start++;
            $image = $row['image'];
            $output .= ' <div class="swiper-slide">
            <div class="swiper-slide-block">
                <div class="swiper-slide-block-img animate-box" data-animate-effect="fadeInLeft" data-swiper-parallax-y="70%">
                    <a href="project-page.html"> <img src="admin/assets/images/BottomSlider/' . $image . '" alt="Event planner, balloon decoration, wedding planner"> </a>
                </div>
                <div class="swiper-slide-block-text animate-box" data-animate-effect="fadeInRight">
                    <h2 data-swiper-parallax-x="-60%" class="next-main-title">' . $row["title"] . '</h2>
                    <h3 data-swiper-parallax-x="-50%" class="next-main-subtitle">' . $row["short_description"] . '</h3>
                    <p data-swiper-parallax-x="-40%" class="next-paragraph">' . $row["long_description"] . '</p>  <span data-swiper-parallax-y="60%" class="next-number">' . $start . '</span>
                </div>
            </div>
        </div>';
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }

    function client_review()
    {
        $start = 0;
        $limit = 9;
        $output = '';
        $title_image_testtim = '';
        $title_image_testtim .= '    <div class="testimonials section-padding sec_main">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 title="Client Love">Client Reviews</h2>
                    <div class="image_with_txt"></div>
                </div>
            </div>
        </div>
        <div class="container unset_container_b">
            <div class="row">
                <div class="col-md-12">
                    <div class="testim">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>';
        $query = "SELECT * FROM client_review where `status` = '1'";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $start++;
            $review_descr = $row["long_description"];
            $max_length = 105;
            if (strlen($review_descr) > $max_length) {
                $review_descr = substr($review_descr, 0, $max_length) . ".....";
            }

            $image = $row['image'];
            $output .= '<div class="box">
            <div class="item">
                <div class="cont">
                    <div class="img"> <img src="admin/assets/images/' . $image . '" alt="Event planner, balloon decoration, wedding planner, room decoration" class="event_img"> </div>
                
                </div>
                <div class="info">
                <h5>' . $row["name"] . '</h5> 
                </div>
                <div class="d_flex_bb">
                    <i class="fa fa-star cl_star" aria-hidden="true"></i>
                    <i class="fa fa-star cl_star" aria-hidden="true"></i>
                    <i class="fa fa-star cl_star" aria-hidden="true"></i>
                    <i class="fa fa-star cl_star" aria-hidden="true"></i>
                    <i class="fa fa-star cl_star" aria-hidden="true"></i>
                </div>
           
                <p>' . $review_descr . '</p>
            </div>
        </div>';
        }

        $response['result'] = "success";
        $response['title_image_testtim'] = $title_image_testtim;
        $response['data'] = $output;
        echo json_encode($response);
    }

    function our_services_img_set()
    {
        $output = '';
        $titleourServices = '';
        $query = "SELECT * FROM our_services where status='1'";

        $result = mysqli_query($this->connect_db, $query);
        $i = 0;
        while ($row = mysqli_fetch_array($result)) {
            $image = $row['images'];
            $url = $row['url'];
            $image = $row['images'];
            $output .= "<div class='col-md-4'>
                    <div class='services-item mar_less_services'>
                        <img class='services-img' src='admin/assets/images/OurServices/" . $image . "' alt='Wedding planner, balloon decoration, wedding planner, room decoration'>
                            <div class='services-caption'>
                                <div class='services-subtitle'><a href=" . $row["url"] . ">Events</a></div>
                                    <div class='services-title'><h4 title='Baby Welcome'><a href=" . $row["url"] . ">" . $row["service_name"] . " </a></h4></div>
                            </div>
                    </div>
                </div>";
            $i++;
        }
        $titleourServices .= '  <div>
        <h2 title="Services">Our Services</h2>
        <div class="image_with_txt"></div>
        </div>';
        $response['result'] = "success";
        $response['data'] = $output;
        $response['titleourServices'] = $titleourServices;
        echo json_encode($response);
    }

    //insex page video gallery set function
    function vidoe_gallery_img_set()
    {
        $start = 0;
        $limit = 4;
        $output = '';
        $title_videogallery = '';
        $query = "SELECT * FROM video_gallery ORDER BY ID DESC LIMIT $start, $limit";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $video_link = $row['video_link'];
            $output .= "<div class='video50'>
                                <div class='evenr' title='video'>
                                   <iframe width='530' height='315' src='" . $video_link . "' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                                </div>
                            </div>";
        }
        $title_videogallery = '<div class="baby_title">
        <h2 class="title">Video Gallery</h2>
        <div class="image_with_txt"></div>
        </div>';
        $response['result'] = "success";
        // $response['data'] = $output;
        $response['title_videogallery'] = $title_videogallery;
        echo json_encode($response);
    }





    //video link insert function
    function video_link_insert()
    {
        if (isset($_POST['video']) && $_POST['video'] != "") {
            $video_link = $_POST['video'];
            $query = "INSERT INTO video_gallery (id,video_link) VALUES (' ','$video_link')";
            if ($this->connect_db->query($query) === TRUE) {
                $response["result"] = "success";
                $response["msg"] = "Uploaded Video Link Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Uploaded Video Link Fail! ";
            }
        } else {
            $response["msg"] = "Video Link is Require!";
        }
        echo json_encode($response);
    }
    //one video home page link insert function
    function video_link_insert_home_page()
    {
        $link = '1';
        if (isset($_FILES["file22"]["name"]) && $_FILES["file22"]["name"] != "" || $_POST['video_link'] != '') {
            if ($_FILES["file22"]["name"] != '') {
                // for settings background desktop image
                $file = $_FILES["file22"]["name"];
                $temp_file = $_FILES["file22"]["tmp_name"];
                $img_ex = pathinfo($file, PATHINFO_EXTENSION);
                $img_ex_lc = strtolower($img_ex);
                $allowed_exs = array("mp4", "png");
                if (in_array($img_ex_lc, $allowed_exs)) {
                    move_uploaded_file($temp_file, "../admin/assets/images/" . $file);
                }
            }
            if ($_POST['video'] != '') {
                $file = $_POST['video'];
                $link = '0';
            }
            $query = "SELECT * FROM video_gallery where status='1' AND one_video='1' AND id";
            $result = mysqli_query($this->connect_db, $query);
            if ($result->num_rows == 0) {
                $query = "INSERT INTO video_gallery (id,video_link,one_video,video_type	) VALUES (' ','$file','1','$link') ";
            } else {
                $query = "update video_gallery set video_link ='$file',video_type='$link' where status='1' AND one_video='1'";
            }

            // $query = "INSERT INTO video_gallery (id,video_link,one_video) VALUES (' ','$video_link','1')";
            if ($this->connect_db->query($query) === TRUE) {
                $response["result"] = "success";
                $response["msg"] = "Uploaded Video Link Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Uploaded Video Link Fail! ";
            }
        } else {
            $response["msg"] = "Video Link is Require!";
        }

        echo json_encode($response);
    }

    //autoplay video slider insert function
    function autoplay_video_slider()
    {
        $link = '1';
        if (isset($_FILES["a_video_slider"]["name"]) && $_FILES["a_video_slider"]["name"] != "") {
            if ($_FILES["a_video_slider"]["name"] != '') {
                // for settings background desktop image
                $file = $_FILES["a_video_slider"]["name"];
                $temp_file = $_FILES["a_video_slider"]["tmp_name"];
                $img_ex = pathinfo($file, PATHINFO_EXTENSION);
                $img_ex_lc = strtolower($img_ex);
                $allowed_exs = array("mp4", "png");
                if (in_array($img_ex_lc, $allowed_exs)) {
                    move_uploaded_file($temp_file, "../admin/assets/images/video_slider/" . $file);
                }
            }
            $query = "INSERT INTO video_gallery (id,video_link,autoplay_slider	) VALUES (' ','$file','1') ";
            if ($this->connect_db->query($query) === TRUE) {
                $response["result"] = "success";
                $response["msg"] = "Uploaded Video Slider Link Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Uploaded Video Slider Link Fail! ";
            }
        } else {
            $response["msg"] = "Video Link is Require!";
        }

        echo json_encode($response);
    }

    //video gallery page table data set in table
    function video_get_data()
    {
        $output = '';
        $i = 1;
        $query = "SELECT * FROM video_gallery where status='1' AND id";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $id = $row['id'];
            $video_link = $row['video_link'];
            $output .= "<tr id=tr_" . $id . ">
                <td>" . $i . "</td>
                <td>" . $video_link . "</td>
                <td class='btn_td'><a class='btn btn-danger clsVideoDelete btn-outline-dark' data-id='" . $id . "'><i class='fa fa-trash-o' aria-hidden='true'></i></a> 
                </td>
            </tr>";
            $i++;
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }


    //contact us page mailer function
    function contact_mailer()
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $subject = $_POST['subject'];
        $messages = $_POST['messages'];
        $mail = new PHPMailer();
        // $to = 'vasu.codelock99@gmail.com';
        $body = "$messages";
        // 	$mail->SMTPDebug  = 3;
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPDebug = 3;
        $mail->Host = "smtp.hostinger.com";
        $mail->Port = 587;
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Username = "tushar@codelocksolutions.in";
        $mail->Password = "Tushar@99";
        $mail->SetFrom('tushar@codelocksolutions.in');
        $mail->addReplyTo($email);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress('tushar@codelocksolutions.in');
        $mail->SMTPOptions = array('ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => false
        ));
        if (!$mail->Send()) {
            echo $mail->ErrorInfo;
            $response['result'] = "fail";
            $response['msg'] = "Mail Not Sent";
        } else {
            $response['result'] = "success";
            $response['msg'] = "Mail Send Successfully !";
        }
        echo json_encode($response);
    }



    //Video gallery page set function
    function video_gallery_set_data()
    {
        $output = '';
        $query = "SELECT * FROM video_gallery where status='1' AND one_video='0' AND id";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $video_link = $row['video_link'];
            $output .= "<div class='video50'>
                            <div class='evenr'>
                                <iframe width='560' height='400' src='" . $video_link . "' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>
                            </div>
                        </div>";
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }

    //autoplay video slider set function
    function autoplay_video_slider_set_data()
    {
        $output = '';
        $query = "SELECT * FROM video_gallery where status='1' AND autoplay_slider='1'";
        $result = mysqli_query($this->connect_db, $query);
        $i = 1;
        while ($row = mysqli_fetch_array($result)) {

            $video_link = $row['video_link'];
            $output .= "<div class='mainslider'><figure>
                            <video   class='bgv' id='video" . $i . "'  muted loop autoplay>
                          
                                <source src='./admin/assets/images/video_slider/" . $video_link . "' type='video/mp4'>
                            </video>
                            <figcaption>
                                <label id='timer' for='progress' role='timer'></label>
                                <progress id='progress" . $i . "' max='100' value='0'>Progress</progress>
                                </figcaotion>
                            </figure></div>";
            $i++;
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }

    // $video_link = $row['video_link'];
    // $output .= "<figure>
    // <video   class='bgv' id='video'  muted loop autoplay>

    //     <source src='./admin/assets/images/video_slider/". $video_link . "' type='video/mp4'>
    // </video>
    // <figcaption>
    //     <label id='timer' for='progress' role='timer'></label>
    //     <progress id='progress' max='100' value='0'>Progress</progress>
    //     </figcaotion>
    // </figure>";

    function set_one_video_section_fun()
    {
        $output = '';
        $output222 = '';

        $query = "SELECT * FROM video_gallery where status='1' AND one_video='1' AND id";
        $result = mysqli_query($this->connect_db, $query);


        while ($row = mysqli_fetch_array($result)) {
            $video_link = $row['video_link'];
            $video_type = $row['video_type'];
            if ($video_type == 0) {
                $output .= '<div class="ResponsiveYTPlayer"><iframe class="youtube-player" id="player" type="text/html" src="' . $video_link . '?wmode=opaque&autohide=1&autoplay=1&enablejsapi=1&loop=1" frameborder="0" muted="muted"></iframe></div>';
            }
            if ($video_type == 1) {
                $video_link = 'admin/assets/images/' . $video_link;
                $output .= "<video class='upload_video' src='" . $video_link . "' muted autoplay loop playsinline></video>";
            }
        }

        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }



    //photo gallery page set data function	
    function photo_gallery_set_data()
    {
        $output = '';
        $query = "SELECT * FROM cls_image WHERE status='1' ORDER BY ID DESC ";
        $result = mysqli_query($this->connect_db, $query);
        while ($row = mysqli_fetch_array($result)) {
            $image = $row['file'];
            $alt_name = $row['image_alt'];
            if ($row['category'] == 13) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/Sliderimages/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 20) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/SliderImagesformobile/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 18) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/NationalFestival/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 17) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/Anniversary/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 16) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/BottomSlider/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 15) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/OurServices/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            // if($row['category']== 14){	
            //     $output .= "<div class='baby25'>	
            //     <img src='admin/assets/images/PhotoGallery/". $image . "' alt='".$alt_name."' class='event_img' title='galary'>	
            // </div>";	
            // }	
            if ($row['category'] == 12) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/SurprisePlanning/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 11) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/CorporateEvent/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 10) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/ShopInauguration/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 9) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/EngagementDecoration/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 8) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/BridetobeDecoration/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 7) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/HaldiCeremony/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 6) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/ThemeDecoration/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 5) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/RoomDecoration/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 4) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/NamingCeremony/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 3) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/BirthdayDecoration/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 2) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/BabyWelcome/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
            if ($row['category'] == 1) {
                $output .= "<div class='baby25'>	
                <img src='admin/assets/images/Babyshower/" . $image . "' alt='" . $alt_name . "' class='event_img' title='galary'>	
            </div>";
            }
        }
        $response['result'] = "success";
        $response['data'] = $output;
        echo json_encode($response);
    }

    //video gallery page delete function
    function video_delete_data()
    {
        $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
        $deletemsg = array();
        $clsdelete = "update video_gallery set status ='0' where id =" . $clsid;
        //        $clsdelete = "DELETE  FROM multistep_form WHERE id=" . $clsid;
        if ($this->connect_db->query($clsdelete) === TRUE) {
            $deletemsg["result"] = "Success";
            $deletemsg["msg"] = "Deleted Video Link Successfully! ";
        } else {
            $deletemsg["result"] = "Fail";
            $deletemsg["msg"] = "Delete Video Link Fail! ";
        }
        echo json_encode($deletemsg);
    }

    //multistep form set data
    function multistep_get_data()
    {
        $id = $_POST['id'];
        $query = "SELECT * FROM multistep_form where id = '$id'";
        $cls = $this->connect_db->query($query);
        $row = $cls->fetch_assoc();
        echo json_encode($row);
    }
    function smtp_mailer($to, $subject, $body)
    {
        $mail = new PHPMailer();
        $mail->SMTPDebug = 0;
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Host = "smtp.hostinger.com";
        $mail->Port = 587;
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->Username = "tushar@codelocksolutions.in";
        $mail->Password = "Tushar@99";
        $mail->SetFrom("tushar@codelocksolutions.in");
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress("nightndayevents11@gmail.com");
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => false
            )
        );

        if (!$mail->Send()) {
            echo $mail->ErrorInfo;
        }
    }
    function addcontact()
    {
        $error_array = $response_data = array();
        if (isset($_POST['name']) && $_POST['name'] == '') {
            $error_array['name'] = "Please Enter Your Name";
        }
        if (isset($_POST['email']) && $_POST['email'] == '') {
            $error_array['email'] = "Please Enter email";
        }
        if (isset($_POST['phone']) && $_POST['phone'] == '') {
            $error_array['phone'] = "Please Enter phone";
        }
        if (isset($_POST['text']) && $_POST['text'] == '') {
            $error_array['text'] = "Please Enter text";
        }
        if (isset($_POST['message']) && $_POST['message'] == '') {
            $error_array['message'] = "Please Enter message";
        }
        if (empty($error_array)) {
            $name =  $_POST["name"];
            $email =  $_POST["email"];
            $phone =  $_POST["phone"];
            $text =  $_POST["text"];
            $message =  $_POST["message"] . '<br>' . ' Phone no : ' . $phone;
            $data = $this->smtp_mailer($email, $text, $message);
            $response_data = array('result' => 'success', 'msg' => "sent mail successfully");
        } else {
            $response_data = array('result' => 'fail', 'msg' => $error_array);
        }
        echo json_encode($response_data);
    }
}
