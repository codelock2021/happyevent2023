<?php

$con = mysqli_connect('localhost', 'root', '', 'vasu');

if (isset($_POST['method']) && $_POST['method'] == "insert_data") {
    $id = (isset($_POST['id']) && $_POST['id'] != "") ? $_POST['id'] : '';
    $email = (isset($_POST['email']) && $_POST['email'] != "") ? $_POST['email'] : 'NULL';
    $uname = '';
    $response = array();
    $chk = '';
    if ($id == '') {
        $error_array = array();
        if (isset($_POST['uname']) && $_POST['uname'] != "") {
            $uname = $_POST['uname'];
        } else {
            $error_array["username"] = "Username is Require!";
        }
        $password = $_POST['password'];
        $number = preg_match('@[0-9]@', $password);
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);
        if ($password != '') {
            if (strlen($password) < 8) {
                $error_array["password"] = "Password must be at least 8 characters.!";
            } else if (!$number || !$uppercase || !$lowercase || !$specialChars) {
                $error_array["password"] = "Password should be Strong.!";
            }
        } else {
            $error_array["password"] = "password is Require!";
        }
        if (isset($_POST['skill']) && $_POST['skill'] != "") {
            $skill = $_POST['skill'];
        } else {
            $error_array["skill"] = "Skill is Require!";
        }
        if (isset($_POST['gender']) && $_POST['gender'] != "") {
            $gender = $_POST['gender'];
        } else {
            $error_array["gender"] = "Gender is Require!";
        }
        if (isset($_POST['email']) && $_POST['email'] != "") {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error_array["email"] = "Invalid E-mail Address!";
            }
        } else {
            $error_array["email"] = "Email is Require!";
        }
        if (isset($_POST['pnumber']) && $_POST['pnumber'] != "") {
            $pnumber = $_POST['pnumber'];
        } else {
            $error_array["pnumber"] = "Phone Number is Require!";
        }
        if (isset($_POST['books']) && $_POST['books'] != "") {
            $books = $_POST['books'];
            foreach($books as $chk1){
                $chk .= $chk1.",";
            }
        } else {
            $error_array["books"] = "Books is Require!";
        }
        if (isset($_POST['message']) && $_POST['message'] != "") {
            $about = $_POST['message'];
        } else {
            $error_array["message"] = "About is Require!";
        }
        if (!empty($error_array)) {
            $response["result"] = "fail";
            $response["data"] = $error_array;
        } else {
            if ($_POST["id"] == '') {
                $sql = "INSERT INTO register_data (Id, username, password, email, gender, skill, number, books, about) VALUES ('', '$uname', '$password', '$email', $gender, $skill, $pnumber, '$chk', '$about')";
                if ($con->query($sql) === TRUE) {
                    $response["result"] = "success";
                    $response["msg"] = "Insert Successfully! ";
                } else {
                    $response["result"] = "fail";
                    $response["msg"] = "Insert Fail! ";
                }
            }
        }
    } else {
        $uname = $_POST['uname'];
        $email = $_POST['email'];
        $skill = $_POST['skill'];
        $pnumber = $_POST['pnumber'];
        if (isset($_POST['books']) && $_POST['books'] != "") {
            $books = $_POST['books'];
            foreach($books as $chk1){
                $chk .= $chk1.",";
            }
        } else {
            $error_array["books"] = "Books is Require!";
        }
        $about = $_POST['message'];
        $clsup = "update register_data set username='$uname' ,email='$email' ,skill='$skill', number='$pnumber', books='$chk', about='$about' where id='$id'";
        if ($con->query($clsup) === TRUE) {
            $response["result"] = "success";
            $response["msg"] = "Update Successfully! ";
        } else {
            $response["result"] = "fail";
            $response["msg"] = "Update Fail! ";
        }
    }
    echo json_encode($response);
} else if (isset($_POST['method']) && $_POST['method'] == "get_data") {
    $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : '';
    $clsselect = "select * from register_data where id=" . $clsid;
    $cls = $con->query($clsselect);
    $row = $cls->fetch_assoc();
    echo json_encode($row);
} else if (isset($_POST['method']) && $_POST['method'] == "delete_data") {
    $clsid = (isset($_POST['id']) && $_POST['id'] != "") ? "'" . $_POST['id'] . "'" : 'NULL';
    $deletemsg = array();
    $clsdelete = "DELETE  FROM register_data WHERE id=" . $clsid;
    if ($con->query($clsdelete) === TRUE) {
        $deletemsg["result"] = "Success";
        $deletemsg["msg"] = "Deleted Successfully! ";
    } else {
        $deletemsg["result"] = "Fail";
        $deletemsg["msg"] = "Delete Fail! ";
    }
    echo json_encode($deletemsg);
} else if (isset($_POST['method']) && $_POST['method'] == "set_data") {
    $response = array();
    $i = 1;
    $clsout = '';
    
    $record_per_page = 5;
    $page = '';
    $output = '';
    if (isset($_POST["page"])) {
        $page = $_POST["page"];
    } else {
        $page = 1;
    }
    $start_from = ($page - 1) * $record_per_page;
    $query = "SELECT * FROM register_data LIMIT $start_from, $record_per_page";
   
    if(isset($_POST['value']) && $_POST['value'] != ''){
        $query = 'SELECT * FROM register_data WHERE username LIKE "%'.str_replace(' ', '%', $_POST['value']).'%" LIMIT '.$start_from.','. $record_per_page;				
    }
    $result = mysqli_query($con, $query);
   
    while ($row = mysqli_fetch_array($result)) {
        $chk = '';
        $id = $row['id'];
        $books = explode(",",$row["books"]);
        foreach($books as $chk1){
            
                $chk .= $chk1.",";
            }
        $slNo = $i + $start_from;
        $output.="<tr id=tr_" .$id.">   
                <td>".$slNo."</td>
                <td>".$row["username"]."</td>
                <td>".$row["email"]."</td>
                <td>".$row["number"]."</td>
                <td>".$chk."</td>
                <td>".$row["about"]."</td>
                <td><a class='btn btn-danger clsdelete btn-outline-dark' data-id='".$id."'>Delete</a>   
                    <a class='btn btn1 btn-outline-dark' href='reform.php?id=".$id."'>Update</a>
                </td>
        </tr>";
        $i++;
    };
    $output_page = "<div class='page_center'>";
    $page_query = "SELECT * FROM register_data ORDER BY id DESC";
    
    $page_result = mysqli_query($con, $page_query);
    
    $total_records = mysqli_num_rows($page_result);
    $total_pages = ceil($total_records / $record_per_page);
    
    for ($i = 1; $i <= $total_pages; $i++) {
        $output_page .= '<li class="page-item clsClass' . $i . '" aria-current="page">
            <span class="page-link pagination_link" id="' . $i . '">' . $i . '</span>
        </li>';
    }
    $output_page .= "</div>";
    $response["result"] = "success";
    $response["msg"] = "success";
    $response["data"] = $output;
    $response["pagination"] = $output_page;

    echo json_encode($response);
}
?>
