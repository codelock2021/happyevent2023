<?php

class Register {

    public $host = "localhost";
    public $user = "u402017191_image_gallery";
    public $password = "Image@99";
    public $db = "u402017191_image_gallery";
    var $connect_db;

    function __construct() {
        $this->connect_db = new mysqli($this->host, $this->user, $this->password, $this->db);
        if (mysqli_connect_errno()) {
            printf("Connection failed:", mysqli_connect_error());
            exit();
        }
        return true;
    }

    function insert_data() {
        $account_id = (isset($_POST['account_id']) && $_POST['account_id'] != "") ? $_POST['account_id'] : '';
        $response = array();

        if ($account_id == '') {
            $error_array = array() == "";
            if (isset($_POST['order_by']) && $_POST['order_by'] != "") {
                $order_by = $_POST['order_by'];
            } else {
                $error_array .= "Order by is Require! ,";
            }
            if (isset($_POST['date']) && $_POST['date'] != "") {
                $date = $_POST['date'];
            } else {
                $error_array .= "Date is Require! ,";
            }
            if (isset($_POST['time']) && $_POST['time'] != "") {
                $time = $_POST['time'];
            } else {
                $error_array .= "Time is Require! ,";
            }
            if (isset($_POST['cls_type']) && $_POST['cls_type'] != "") {
                $type = $_POST['cls_type'];
            } else {
                $error_array .= "Type is Require! ,";
            }
            if (isset($_POST['client_name']) && $_POST['client_name'] != "") {
                $client_name = $_POST['client_name'];
            } else {
                $error_array .= "Client Name is Require! ,";
            }
            if (isset($_POST['client_num']) && $_POST['client_num'] != "") {
                $client_num = $_POST['client_num'];
            } else {
                $error_array .= "Client Number is Require! ,";
            }
            if (isset($_POST['advance_amount']) && $_POST['advance_amount'] != "") {
                $advance_amount = $_POST['advance_amount'];
            } else {
                $error_array .= "Advance Amount is Require! ,";
            }
            if (isset($_POST['remain']) && $_POST['remain'] != "") {
                $remain = $_POST['remain'];
            } else {
                $error_array .= "Remain Amount is Require! ,";
            }
            if (isset($_POST['total_amount']) && $_POST['total_amount'] != "") {
                $total_amount = $_POST['total_amount'];
            } else {
                $error_array .= "Total Amount is Require!";
            }
            $destination_array = explode(',', $error_array);
            if (!empty($error_array)) {
                $response["result"] = "fail";
                $response["data"] = $destination_array;
            } else {
                if ($_POST["id"] == '') {
                    $order_num = $_POST['order_num'];
                    $sql = "INSERT INTO multistep_form (id, order_num, order_by, date, time, type, client_name, client_contact, advance_amount, remain_amount, total_amount) VALUES "
                            . "('', '$order_num', '$order_by', '$date', '$time', '$type', '$client_name', $client_num, $advance_amount, $remain, $total_amount)";
                    if ($this->connect_db->query($sql) === TRUE) {
                        $response["result"] = "success";
                        $response["msg"] = "Insert Successfully! ";
                        $select = "select * from multistep_form where order_num ='$order_num'";
                        $result = $this->connect_db->query($select);
                        $row = mysqli_fetch_array($result);
                        $formid = $row['id'];
                        $_SESSION['id'] = $formid;
                        $response["account_id"] = $formid; 
                    } else {
                        $response["result"] = "fail";
                        $response["msg"] = "Insert Fail! ";
                    }
                }
            }
//            echo json_encode($response);
        }else{
            $foil_ball = array();
            $ball_color = $_POST['ball_color'];
//            $foil_ball = array(
//                "foil_input" => $_POST['foil_ball'],
//                "foil_qty" => $_POST['foil_ball_qty'],
//                "foil_color" => $_POST['foil_ball_color']
//            );
            $foil_ball = $_POST['foil_ball'] . ', ' . $_POST['foil_ball_qty']. ', ' . $_POST['foil_ball_color'];
            $gate = $_POST['gate'];
            $backdrop = $_POST['backdrop'];
            $banner = $_POST['banner_select'];
            $pillar = $_POST['pillar_lable'] . ', ' . $_POST['pillar_qty'];
            $props = $_POST['props'] . ', ' . $_POST['props_select'];
            $stage = $_POST['stage'] . ', ' . $_POST['stage_input'];
            $sofa = $_POST['sofa'];
            $fog_machine = $_POST['fog'];
            $matka_qty = $_POST['matka_qty'];
            $payro_qty = $_POST['payro_qty'];
            $light_qty = $_POST['light_qty'];
            $clsup = "update multistep_form set balloon_colors='$ball_color' ,foil_balloon='$foil_ball' ,gate='$gate', backdrop='$backdrop', Banner='$banner', pillar='$pillar', props='$props', stage='$stage', sofo='$sofa', fog_matka='$matka_qty', fog_machine='$fog_machine', cold_payro='$payro_qty', par_light='$payro_qty' where id='$account_id'";
            if ($this->connect_db->query($clsup) === TRUE) {
                $response["result"] = "success";
                $response["msg"] = "Update Successfully! ";
            } else {
                $response["result"] = "fail";
                $response["msg"] = "Update Fail! ";
            }
        }
        echo json_encode($response);
    }

    function set_order_num() {
        $response = '';
        $max_number = "SELECT max(order_num) from multistep_form";
        $get_max_num = $this->connect_db->query($max_number);
        $row = mysqli_fetch_assoc($get_max_num);
        $max_order_id = $row['max(order_num)'];
        $max_order_id++;
        $response = $max_order_id;
        echo json_encode($response);
    }

}

?>