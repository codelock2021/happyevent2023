<?php
$con = mysqli_connect('localhost', 'u402017191_image_gallery', 'Image_gallery@99', 'u402017191_image_gallery');
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Happy Event | Event planner | Birthday Organizer</title>
        <!-- Favicon-->
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css" />
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/morrisjs/morris.css" />
        <!-- Custom Css -->
        <link href="assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link rel="stylesheet" href="assets/css/cashbook.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.js"></script>
        <script src="assets/js/img_ajax1.js"></script>
    </head>
    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore CodeLock...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>


        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>
        <section class="content home">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Cash Book
                            <small class="text-muted">Welcome to Happy Event Cash Book Data Page</small>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="alert alert-danger dataBlank" role="alert"></div>
            <form id="cashbook_form" enctype="multipart/form-data" method="POST">
                <input type="hidden" name="method" id="cls_cashbook" value="cashbook_insertdata">
                <div class="dropdown_user_div">
                    <div class="filter_div"><input type="date" class="user_date" name="date"/></div>
                    <div class="user_dropdown">
                        <select class='show-tick show_user_Data'  name='cls_user_name'>
                            <option value='' name='cls_user_name' selected disabled class='User'>User</option>
                            <?php $query = "SELECT * from user order by id asc";
                                    $result = mysqli_query($con, $query);
                                     while($row = mysqli_fetch_array($result)){
                                          ?>
                            <option value="<?php echo $row['id'];?>"><?php echo $row['user_name'];?></option>
                                     <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="user_amount_div">
                    <div class="user_amount">
                        <input type="number" name="cashbook_amount" class="cashbook_amount" placeholder="Amount" />
                    </div>
                    <div class="user_remark">
                        <input type="text" name="remark_amount" class="remark_amount" placeholder="Remark" />
                    </div>
                </div>
                <div class="buttons_div">
                    <div class="cash_in_button_div">
                        <button class="btn  btn-raised btn-success waves-effect clscash_in cashClick" data-value="0">+ Cash In</button>
                    </div>
                    <div class="cash_out_button_div">
                        <button class="btn  btn-raised btn-danger waves-effect clscash_out cashClick" data-value="1">- Cash Out</button>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2 class="cbdash"><b>Cash Book Dashboard</b></h2>
                        <a class="totalAmount"></a>
                    </div>
                    <div class="cls_function_class">
                        <div class="cls_cashbook_search_div">
                            <input type="search" class="cls_cashbook_search" data-table="order-table" placeholder="search at least 3 characters"/>
                        </div>
                        <div class="cashbook_filter_div"><input type="date" id="cashbook_filter_date" name="date" placeholder="Filter By Date" />
                            <div class="show_user_fltr_div">
                                <select class='show-tick show_user_data_filter'  name='clsorder_data_status_fltr'>
                                    <option value='' selected disabled>User</option>
                                    <?php $query = "SELECT * from user order by id asc";
                                    $result = mysqli_query($con, $query);
                                     while($row = mysqli_fetch_array($result)){
                                          ?>
                            <option value="<?php echo $row['id'];?>"><?php echo $row['user_name'];?></option>
                                     <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="cashbook_html"></div>
                        <div class="cashbook_data_html"></div>
                    </div>
                </div>
            </form>
        </section>
        <script src="assets/bundles/libscripts.bundle.js"></script>
        <script src="assets/bundles/vendorscripts.bundle.js"></script>
        <script src="assets/bundles/knob.bundle.js"></script>
        <!--<script src="assets/bundles/mainscripts.bundle.js"></script>-->
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>
        <script src="assets/js/pages/ui/dialogs.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script>
        <!--<script src="assets/js/pages/index.js"></script>-->
    </body>
</html>
<script>
    $(document).ready(function () {
        clscashbook_show_data();
        clscashbook_insertdata();
    });
</script>