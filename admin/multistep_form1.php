
<html class="no-js " lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Genos&display=swap" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link  rel="stylesheet" href="assets/css/multistep.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="assets/plugins/momentjs/moment.js"></script>
        <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="../nexa/assets/js/multistep_form.js"></script>
<!--<script src="cls_pgntnjs.js"></script>-->
        <title>Steps/Progress Bar </title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
                    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                        <h2 id="heading">Sign Up Your User Account</h2>
                        <p>Fill all form field to go to next step</p>
                        <!--<span class="error-msg clsinsert"></span>-->
                        <form id="msform" enctype="multipart/form-data" >
                            <input type="hidden" name="id" id="cls_id">
                            <input type="hidden" name="method" id="cls_insertdata" value="insert_data">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="account"><strong>Account</strong></li>
                                <li id="personal"><strong>Personal</strong></li>
                                <li id="payment"><strong>Image</strong></li>
                                <li id="confirm"><strong>Finish</strong></li>
                            </ul>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> <br> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                    <div class="alert alert-success clsinsert" role="alert">
                                    </div>
                                    <div class="alert alert-danger error_msg" role="alert">
                                        <ul class="cls_ul"></ul>
                                    </div>
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="fs-title">Order Information:</h2>
                                        </div>
                                        <div class="col-5">
                                            <h2 class="steps">Step 1 - 4</h2>
                                        </div>
                                    </div> 
                                    <span class="error-msg order_num_error"></span>
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Order number : *</label>
                                        </div>
                                    </div>
                                    <input type="number" name="order_num" readonly class="cls_or_num"/> 
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Order By : *</label>
                                        </div>
                                    </div> 
                                    <!--<span class="error-msg order_by_error"></span>-->
                                    <select class="js-example-disabled-results order_by" id="cls_order" name="order_by">
                                        <option value="" selected disabled>Choose Order by</option>
                                        <option value="0">Cash on Delivery(0)</option>
                                        <option value="1">UPI(1)</option>
                                        <option value="2">Pay Pal(2)</option>
                                        <option value="2">Credit card(3)</option>
                                    </select>
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Date: *</label>
                                        </div>
                                    </div>
                                    <input type="date" name="date" placeholder="DD/MM/YYYY" /> 
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Time : *</label>
                                        </div>
                                    </div>
                                    <input type="time" name="time" placeholder="Time" />
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Type : *</label>
                                        </div>
                                    </div>
                                    <select class="js-example-disabled-results cls_type" id="cls_order" name="cls_type">
                                        <option value="" selected disabled>Choose Type</option>
                                        <option value="0">Cash on Delivery(0)</option>
                                        <option value="1">UPI(1)</option>
                                        <option value="2">Pay Pal(2)</option>
                                        <option value="2">Credit card(3)</option>
                                    </select>
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Client Name : *</label> 
                                        </div>
                                    </div>
                                    <input type="text" name="client_name" placeholder="Client Name" />
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Client Contact :  *</label> 
                                        </div>
                                    </div>
                                    <input type="number" name="client_num" placeholder="Client Contact" />
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels" id="adv_amo">Advance Amount : *</label> 
                                        </div>
                                    </div>
                                    <input type="number" name="advance_amount" placeholder="Advance Amount" />
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Remain amount :  *</label> 
                                        </div>
                                    </div>
                                    <input type="number" name="remain" placeholder="Remain Amount" />
                                    <div class="order_number_div">
                                        <div>
                                            <label class="fieldlabels">Total amount :   *</label> 
                                        </div>
                                    </div>
                                    <input type="number" name="total_amount" placeholder="Total Amount" />
                                </div> <input type="button" name="next" class="next action-button" value="Next" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="fs-title">Order Items</h2>
                                        </div>
                                        <div class="col-5">
                                            <h2 class="steps">Step 2 - 4</h2>
                                        </div>
                                    </div>
                                    <label class="fieldlabels clsball_color">Balloon Colors :  </label>
                                    <input type="hidden" name="account_id" value="" class="accountId">
                                    <input type="text" id="ball_color_tag" value="" data-role="tagsinput" name="ball_color">
                                    <div class="foil_ball_div">
                                        <label class="fieldlabels foil_ball">Foil balloon :  </label> 
                                        <label class="fieldlabels foil_ball_qty">Foil balloon Quantity:  </label>
                                        <input type="text" name="foil_ball" placeholder="Foil balloon :" class="foil_ball_input"/>
                                        <div class="wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="cls_count" name="foil_ball_qty" type="text" id="1" value="0" min="0" max="100" readonly/>
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                    </div>
                                    <label class="fieldlabels">Foil balloon Color:  </label>
                                    <input type="text" id="foilball_color_tag" value="" data-role="tagsinput" name="foil_ball_color"> 
                                    <div class="clsgate_div">
                                        <label class="fieldlabels cls_gate">Gate:  </label>
                                        <div class="clsget_radio_div">
                                            <label class="fieldlabels clsgate_no" for="no_gate">No</label>
                                            <input name="gate" value="0" type="radio" class="gate_radio_no" id="no_gate" checked>
                                            <label class="fieldlabels clsgate_yes" for="yes_gate">Yes</label>
                                            <input name="gate" value="1" type="radio" class="gate_radio_yes" id="yes_gate">
                                        </div>
                                        <div class="clsback_main_div">
                                            <label class="fieldlabels cls_back">Backdrop :  </label>
                                            <div class="clsback_radio_div">
                                                <label class="fieldlabels backdrop_no">No</label>
                                                <input name="backdrop" value="0" type="radio" class="radio_col_no" id="no_back" checked>
                                                <label class="fieldlabels backdrop_yes">Yes</label>
                                                <input name="backdrop" value="1" type="radio" class="radio_col_yes" id="yes_back">
                                            </div>
                                        </div>
                                    </div>
                                    <label class="fieldlabels">Banner :  </label>
                                    <div class="mm-dropdown">
                                        <div class="textfirst">Select<img src="../nexa/assets/images/xs/avatar3.jpg" width="10" height="10" class="down" /></div>
                                        <ul>
                                            <li class="input-option" data-value="1">
                                                <img src="assets/images/5.jpg" alt="" width="20" height="20" /> Text1
                                            </li>
                                            <li class="input-option" data-value="2">
                                                <img src="assets/images/6.jpg" alt="" width="20" height="20" /> Text2
                                            </li>
                                            <li class="input-option" data-value="3">
                                                <img src="assets/images/avatar7.jpg" alt="" width="20" height="20" /> Text3
                                            </li>
                                        </ul>
                                        <input type="hidden" class="option" name="banner_select" value="" />
                                    </div>
                                    <div class="pillar_div">
                                        <label class="fieldlabels pillar_lable">Pillar :  </label> 
                                        <label class="fieldlabels pillar_qty">Pillar Quantity:  </label>
                                        <input type="text" name="pillar_lable" placeholder="Pillar Size:" class="pillar_input"/>
                                        <div class="wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="pillar_qty" readonly/>
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                    </div>
                                    <div class="clsprop_main_div">
                                        <label class="fieldlabels props_lable">Props :  </label> 
                                        <div class="clsprop_radio">
                                            <label class="fieldlabels props_no">No</label>
                                            <input name="props" value="0" type="radio" class="props_col_no" checked="">
                                            <label class="fieldlabels props_yes">Yes</label>
                                            <input name="props" value="1" type="radio" class="props_col_yes">
                                        </div>
                                        <div class="yes_select" style="display: none;">
                                            <div class="mm-dropdown1">
                                                <div class="textfirst">Select<img src="../nexa/assets/images/xs/avatar3.jpg" width="10" height="10" class="down" /></div>
                                                <ul>
                                                    <li class="input-option" data-value="1">
                                                        <img src="assets/images/5.jpg" alt="" width="20" height="20" /> Text1
                                                    </li>
                                                    <li class="input-option" data-value="2">
                                                        <img src="assets/images/6.jpg" alt="" width="20" height="20" /> Text2
                                                    </li>
                                                    <li class="input-option" data-value="3">
                                                        <img src="assets/images/avatar7.jpg" alt="" width="20" height="20" /> Text3
                                                    </li>
                                                </ul>
                                                <input type="hidden" class="option" name="props_select" value="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clsstage_main_div">
                                        <label class="fieldlabels stage_lable">Stage :  </label> 
                                        <div class="clsstage_radio">
                                            <label class="fieldlabels clsball_no">No</label>
                                            <input name="stage" value="0" type="radio" class="radio_col_no" checked="">
                                            <label class="fieldlabels clsball_yes">Yes</label>
                                            <input name="stage" value="1" type="radio" class="radio_stage_yes">
                                        </div>
                                        <div class="yes_input" style="display: none;">
                                            <input type="text" name="stage_input" placeholder="Enter Your Stage Size:" />
                                        </div>
                                    </div>
                                    <div class="clssofa_main_div">
                                        <label class="fieldlabels sofa_lable">Sofa :  </label>
                                        <div class="clssofa_div">
                                            <label class="fieldlabels clsball_no">No</label>
                                            <input name="sofa" value="0" type="radio" class="sofa_radoi_no" checked="">
                                            <label class="fieldlabels clsball_yes">Yes</label>
                                            <input name="sofa" value="1" type="radio" class="sofa_radoi_yes">
                                        </div>
                                        <label class="fieldlabels machine_lable">Fog machine :  </label>
                                        <div class="machine_div">
                                            <label class="fieldlabels clsball_no">No</label>
                                            <input name="fog" value="0" type="radio" class="fog_radoi_yes" checked="">
                                            <label class="fieldlabels clsball_yes">Yes</label>
                                            <input name="fog" value="1" type="radio" class="fog_radoi_yes">
                                        </div>
                                    </div>
                                    <label class="fieldlabels" for="matka_qty">Fog matka :  </label>
                                    <div class="wrap fog_wrap">
                                        <button type="button" id="sub" class="sub">-</button>
                                        <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="matka_qty" readonly/>
                                        <button type="button" id="add" class="add">+</button>
                                    </div>
                                    <div class="machine_main_div">

                                    </div>
                                    <label class="fieldlabels" for="payro_qty">Cold payro :  </label>
                                    <div class="wrap fog_wrap">
                                        <button type="button" id="sub" class="sub">-</button>
                                        <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="payro_qty" readonly/>
                                        <button type="button" id="add" class="add">+</button>
                                    </div>
                                    <label class="fieldlabels" for="light_qty">Par light :  </label>
                                    <div class="wrap fog_wrap">
                                        <button type="button" id="sub" class="sub">-</button>
                                        <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="light_qty" readonly/>
                                        <button type="button" id="add" class="add">+</button>
                                    </div>
                                    <label class="fieldlabels other_info">Other Info</label>
                                    <textarea rows="4" class="form-control other_info" placeholder="Enter Other Info..!" name="other_info"></textarea>
                                </div> 
                                <input type="button" name="next" class="next action-button" value="Next" /> 
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <div class="row">
                                        <div class="col-9">
                                            <h2 class="fs-title">Image Upload:</h2>
                                        </div>
                                        <div class="col-5">
                                            <h2 class="steps">Step 3 - 4</h2>
                                        </div>
                                    </div> <label class="fieldlabels">Upload Your Photo:</label> <input type="file" name="pic" accept="image/*"> <label class="fieldlabels">Upload Signature Photo:</label> <input type="file" name="pic" accept="image/*">
                                </div> 
                                <!--<input type="button" name="next" class="next action-button" value="Submit" />--> 
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="fs-title">Finish:</h2>
                                        </div>
                                        <div class="col-5">
                                            <h2 class="steps">Step 4 - 4</h2>
                                        </div>
                                    </div> <br><br>
                                    <h2 class="purple-text text-center"><strong>SUCCESS !</strong></h2> <br>
                                    <div class="row justify-content-center">
                                        <div class="col-3"> <img src="https://i.imgur.com/GwStPmg.png" class="fit-image"> </div>
                                    </div> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-7 text-center">
                                            <h5 class="purple-text text-center">You Have Successfully Signed Up</h5>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script>
    $(document).ready(function () {
        set_order_num();
    });
</script>