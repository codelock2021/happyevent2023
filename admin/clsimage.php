<?php
$con = mysqli_connect('localhost', 'root', '', 'vasu');
$clsid = (isset($_GET['id']) && $_GET['id'] != "") ? $_GET['id'] : '';
$dispplay_data = ($clsid == '') ? "block" : "none";
?>
﻿<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="">
        <title>:: CLS :: Image Gallery</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Favicon-->
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Light Gallery Plugin Css -->
        <link href="assets/plugins/light-gallery/css/lightgallery.css" rel="stylesheet">
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link type="text/css" rel="stylesheet" href="magicscroll/magicscroll.css"/>
        <script type="text/javascript" src="magicscroll/magicscroll.js"></script>
        <script src="../nexa/assets/js/ajax.js"></script>
    </head>

    <style>
        .theme-orange .user-info{
            display: flex;
            padding-left: 1px;
        }
        .icon_upload{
            font-size: 25px;
        }
    </style>

    <body class="theme-orange">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>

        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>

        <section class="content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Image Gallery</h2>
                    </div>
                </div>
            </div>
            <div class="MagicScroll" data-options="mode: carousel; height: 275px;">
                <img src="assets/images/avatar1.jpg"/>
                <img src="assets/images/avatar2.jpg" />
                <img src="assets/images/avatar3.jpg" />
                <img src="assets/images/avatar4.jpg" />
                <img src="assets/images/avatar5.jpg" />
                <img src="assets/images/avatar6.jpg" />
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <div class="tag-list">
                                    <input type="hidden" name="method" id="clshidden2" value="clsimage">
                                    <input id="fileupload" type="file" multiple="multiple" name="image[]" style="display:none">
                                    <div class="upld">
                                        <label class="clslbl">Upload Image : </label>
                                        <a href="#">
                                            <i id="icon_upload" class="fa fa-picture-o fa-5" onclick="upload()"></i>
                                        </a>
                                    </div>
                                    <select class="show-tick clscategory" name="category" placeholder="Choose Category">
                                        <option value="" disabled selected hidden>Choose Category..</option>
                                        <option value="0" name="category" id="act">Actor</option>
                                        <option value="1" name="category" id="emp">Employee</option>
                                        <option value="2" name="category"id="acts">Actress</option>
                                        <option value="3" name="category" id="nat">Nature</option>
                                        <option value="4" name="category" id="lan">Landscapes</option>
                                    </select>
                                    <button class="btn btn-raised btn-success waves-effect clssubmit btnupload_img" onclick="icon()"><i id="clsicon" class="fa fa-spinner fa-spin clsicon" style="display: none"></i><span>Upload</span></button>
                                </div>
                                <div id="dvPreview">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2> GALLERY </h2>
                        </div>
                        <div class="body" id="clsbody">
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/1.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-1.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/2.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-2.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/3.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-3.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/4.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-4.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/5.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-5.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/6.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-6.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/7.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-7.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/8.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-8.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/9.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-9.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/10.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-10.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/11.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-11.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/12.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-12.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/13.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-13.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/14.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-14.jpg" alt=""> </a>
                                </div>
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                                    <a href="assets/images/image-gallery/15.jpg" data-sub-html="Demo Description"> <img class="img-fluid img-thumbnail" src="assets/images/image-gallery/thumb/thumb-15.jpg" alt=""> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="assets/bundles/libscripts.bundle.js"></script>
    <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script>
    <!-- Lib Scripts Plugin Js -->

    <script src="assets/plugins/light-gallery/js/lightgallery-all.js"></script>
    <!-- Light Gallery Plugin Js -->

    <script src="assets/bundles/mainscripts.bundle.js"></script>
    <!-- Custom Js -->
    <script src="assets/js/pages/medias/image-gallery.js"></script>
</body>

<!-- Mirrored from www.wrraptheme.com/templates/nexa/html/image-gallery.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 01 Jan 2020 09:26:32 GMT -->

</html>