<?php
// include('../connection.php'); 
include 'img_function.php';
$db_connection = new DB_Class();
$con = $GLOBALS['conn'];
// $con = mysqli_connect('localhost', 'u402017191_image_gallery', 'Image_gallery@99', 'u402017191_image_gallery');
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Happy Event | Event planner | Birthday Organizer</title>
    <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/imggallery.css">
    <link rel="stylesheet" href="../css/style_css_admin.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="assets/js/img_ajax1.js"></script>
</head>

<body class="theme-orange">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <p>Please wait...</p>
            <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="80" height="80" alt="Happy Event"></div>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div><!-- Search  -->
    <div class="search-bar">
        <div class="search-icon"> <i class="material-icons">search</i> </div>
        <input type="text" placeholder="Explore Nexa...">
        <div class="close-search"> <i class="material-icons">close</i> </div>
    </div>
    <?php
    //        Top Bar
    include 'navbar.php';
    //        Left Sidebar
    include 'sidebar.php';
    ?>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Bottom Slider
                        <small class="text-muted">Welcome to Happy Event Bottom Slider Page</small>
                    </h2>
                </div>
                <center><span class="slider-success"></span></center>
                <center><span class="error-msg clssize"></span></center>
                <center><span class="error-msg fileupload1"></span></center>
                <center><span class="error-msg clsdel"></span></center>
                <center><span class="error-msg clsname"></span></center>
            </div>
        </div>
        <!-- <div class="container">
            <div class="bottom-slider">
                <form id="bottmSliderForm">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control" name="title" id="clientName" aria-describedby="emailHelp" placeholder="Title">
                    </div>
                    <span class="err title-slider"></span>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Short Description</label>
                        <input type="text" class="form-control" name="shortdescription" id="clientName" aria-describedby="emailHelp" placeholder="Short Description">
                    </div>
                    <span class="err short-description"></span>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Long Description</label>
                        <textarea class="form-control" name="longdescription" id="longDescription" placeholder="Long Description"></textarea>
                    </div>
                    <span class="err long-description"></span>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" name="image" accept=".jpeg, .jpg, .jpe" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                    <span class="err slider-img"></span>
                    <div class="form-group">
                        <button class="btn btn-primary" id="bottomslider">Submit</button>
                    </div>
                </form>
            </div>
        </div> -->
        <div>
            <div class="form-wrap">
                <form id="bottmSliderForm">
                    <div class="d-flex">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="name-label" for="name">Title</label>
                                <input type="text" name="title" placeholder="Enter Title" aria-describedby="emailHelp" class="form-control padd_10_all_d">
                            </div>
                            <span class="err title-slider"></span>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label id="name-label" for="name">Short Description</label>
                                <input type="text" name="shortdescription" placeholder="Enter Short Description" aria-describedby="emailHelp" class="form-control padd_10_all_d">
                            </div>
                            <span class="err short-description"></span>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Long Description</label>
                                <textarea class="form-control padd_10_all_d" name="longdescription" id="longDescription" placeholder="Enter Long Description"></textarea>
                            </div>
                            <span class="err long-description"></span>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Upload File</label>
                                <div class="preview-zone hidden">
                                    <div class="box222 box-solid">
                                        <div class="box-header with-border">
                                            <div><b>Preview</b></div>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-danger btn-xs remove-preview">
                                                    <i class="fa fa-times"></i> Reset This Image
                                                </button>
                                            </div>
                                        </div>
                                        <div class="box-body"></div>
                                    </div>
                                </div>
                                <div class="dropzone-wrapper">
                                    <div class="dropzone-desc">
                                        <i class="glyphicon glyphicon-download-alt"></i>
                                        <p>Choose an image file or drag it here.</p>
                                    </div>
                                    <input type="file" class="custom-file-input dropzone" id="inputGroupFile01" name="image" accept=".jpeg, .jpg, .jpe" aria-describedby="inputGroupFileAddon01">
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="col-md-12 err slider-img"></span>

                    <div class="d-flex jus__cont_end">
                        <div class="col-md-4">
                            <button class="btn btn-primary btn-block btn_back_color_set" id="bottomslider"><i id="clsicon" class="fa fa-spinner fa-spin clsicon" style="display: none"></i><span>Submit</span></button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="clsnav">
            <nav class="clsnav">
                <ul class="pagination pagination-lg pagination_html"></ul>
            </nav>
        </div>
    </section>
    <!-- Jquery Core Js -->
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js -->
    <script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
    <script src="assets/js/pages/forms/basic-form-elements.js"></script>
    <script src="assets/js/img_ajax1.js"></script>
</body>

</html>
<script>

</script>