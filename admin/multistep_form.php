<?php
$con = mysqli_connect('localhost', 'u402017191_image_gallery', 'Image_gallery@99', 'u402017191_image_gallery');
$clsid = (isset($_GET['id']) && $_GET['id'] != "") ? $_GET['id'] : '';
include 'img_function.php';
$db = new Register();
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:img_login.php");
}
?>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Happy Event | Event planner | Birthday Organizer</title>
        <link rel="icon" href="assets/images/xs/happyevent(5).png" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link  rel="stylesheet" href="assets/css/main.css">
        <link  rel="stylesheet" href="assets/css/multistep.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="/admin/assets/js/img_ajax1.js"></script>
    </head>
    <body class="theme-orange">
        <div class="page-loader-wrapper">
            <div class="loader">        
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <p>Please wait...</p>
                <div class="m-t-30"><img src="assets/images/xs/happyevent(5).png" width="48" height="48" alt="Nexa"></div>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div><!-- Search  -->
        <div class="search-bar">
            <div class="search-icon"> <i class="material-icons">search</i> </div>
            <input type="text" placeholder="Explore Nexa...">
            <div class="close-search"> <i class="material-icons">close</i> </div>
        </div>
        <?php
//        Top Bar
        include 'navbar.php';
//        Left Sidebar
        include 'sidebar.php';
        ?>
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
                        <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                            <h2 id="heading">Creat Your Order</h2>
                            <p>Fill all form field to go to next step</p>
                            <!--<span class="error-msg clsinsert"></span>-->
                            <form id="msform" enctype="multipart/form-data" >
                                <input type="hidden" name="id" id="cls_id">
                                <input type="hidden" name="method" id="cls_insertdata" value="insert_data">
                                <!-- progressbar -->
                                <ul id="progressbar">
                                    <li class="active" id="account"><strong>Customer Detail</strong></li>
                                    <li id="personal"><strong>Order Detail</strong></li>
                                    <li id="payment"><strong>Image</strong></li>
                                    <li id="confirm"><strong>Finish</strong></li>
                                </ul>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div> <br> <!-- fieldsets -->
                                <fieldset>
                                    <div class="form-card">
                                        <div class="alert alert-success clsinsert" role="alert">
                                        </div>
                                        <div class="alert alert-danger error_msg" role="alert">
                                            <ul class="cls_ul"></ul>
                                        </div>
                                        <div class="row">
                                            <div class="col-7 ds-f-100">
                                                <h2 class="fs-title">Order Information</h2>
                                            </div>
                                            <div class="col-5 step1">
                                                <h2 class="steps">Step 1-4</h2>
                                            </div>
                                        </div> 
                                        <span class="error-msg order_num_error"></span>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Order number : *</label>
                                            </div>
                                        </div>
                                        <input type="number" name="order_num" readonly class="cls_or_num"/> 
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Order By : *</label>
                                            </div>
                                        </div> 
                                        <select class="js-example-disabled-results order_by" id="clsselect" name="order_by">
                                            <option value="" selected disabled>Choose Order by</option>
                                            <?php
                                            $query = "SELECT * from user order by id asc";
                                            $result = mysqli_query($con, $query);
                                            while ($row = mysqli_fetch_array($result)) {
                                                ?>
                                                <option value="<?php echo $row['id']; ?>"><?php echo $row['user_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Date: *</label>
                                            </div>
                                        </div>
                                        <input type="date" name="date" placeholder="DD/MM/YYYY" class="multiDate"/> 
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Time : *</label>
                                            </div>
                                        </div>
                                        <input type="time" name="time" placeholder="Time" class="multiTime"/>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Order Type : *</label>
                                            </div>
                                        </div>
                                        <select class="js-example-disabled-results cls_type" id="clsselect" name="cls_type">
                                            <option value="" selected disabled>Choose Order Type</option>
                                            <?php
                                            $query = "SELECT * from category_data order by id asc";
                                            $result = mysqli_query($con, $query);
                                            $ingore_array = array('Slider images', 'Photo Gallery', 'Our Services', 'Bottom Slider');
                                            while ($row = mysqli_fetch_array($result)) {
                                                if (in_array($row['category'], $ingore_array)) {
                                                    continue;
                                                }
                                                ?>
                                                <option value="<?php echo $row['id']; ?>"><?php echo $row['category']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Client Name : *</label> 
                                            </div>
                                        </div>
                                        <input type="text" name="client_name" placeholder="Client Name" class="clientName"/>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Client Contact :  *</label> 
                                            </div>
                                        </div>
                                        <input type="number" name="client_num" placeholder="Client Contact" class="clientNumber"/>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels" id="adv_amo">Advance Amount : *</label> 
                                            </div>
                                        </div>
                                        <input type="number" name="advance_amount" placeholder="Advance Amount" class="advanceAmount"/>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Remain amount :  *</label> 
                                            </div>
                                        </div>
                                        <input type="number" name="remain" placeholder="Remain Amount" class="remainAmount"/>
                                        <div class="order_number_div">
                                            <div>
                                                <label class="fieldlabels">Total amount :   *</label> 
                                            </div>
                                        </div>
                                        <input type="number" name="total_amount" placeholder="Total Amount" class="totalAmount" readonly/>
                                        <label class="fieldlabels address">Address</label>
                                        <textarea rows="4" class="form-control address" placeholder="Enter Address..!" name="address"></textarea>
                                    </div> <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Order Items</h2>
                                            </div>
                                            <div class="col-5 step2">
                                                <h2 class="steps">Step 2 - 4</h2>
                                            </div>
                                        </div>
                                        <label class="fieldlabels clsball_color">Balloon Colors :  </label>
                                        <input type="hidden" name="account_id" value="" class="accountId">
                                        <input type="text" id="ball_color_tag" value="" data-role="tagsinput" name="ball_color">
                                        <div class="foil_ball_div">
                                            <label class="fieldlabels foil_ball">Foil balloon :  </label>
                                            <input type="text" value="" data-role="tagsinput" name="foil_ball" class="foil_ball_class">
                                            <label class="fieldlabels foil_ball_qty">Foil balloon Quantity:  </label>
                                        </div>
                                        <div class="foil_ball_div_text">
                                        <div class="foil_ball_text">
                                            <!--<input type="text" name="foil_ball" class="foil_ball_input_desktop"/>-->
                                            <input type="text" id="foil_ball_input" value="" data-role="tagsinput" name="foil_ball">
                                        </div>
                                        <div class="wrap cls_wrap_foil">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="cls_count" name="foil_ball_qty" type="text" id="1" value="0" min="0" max="100" readonly/>
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                        </div>
                                        <label class="fieldlabels">Foil balloon Color:  </label>
                                        <input type="text" id="foilball_color_tag" value="" data-role="tagsinput" name="foil_ball_color"> 
                                        <div class="clsgate_div">
                                            <div class="clsget_radio_div">
                                                <label class=" cls_gate">Gate:  </label>
                                                <input name="gate" type="radio" value="0" class="radio-col-black" id="no_gate" checked>
                                                <label for="no_gate" class="">No</label>
                                                <input name="gate" type="radio" value="1" class="radio-col-black" id="yes_gate">
                                                <label for="yes_gate" class="">Yes</label>
                                            </div>
                                            <div class="clsback_main_div">
                                                <label class="fieldlabels cls_back">Backdrop :  </label>
                                                <input name="backdrop" value="0" type="radio" class="radio-col-black" id="no_back" checked>
                                                <label class="" for="no_back">No</label>
                                                <input name="backdrop" value="1" type="radio" class="radio-col-black" id="yes_back">
                                                <label class="" for="yes_back">Yes</label>
                                            </div>
                                        </div>
                                        <label class="fieldlabels">Banner :  </label>
                                        <div class="mm-dropdown">
                                            <div class="textfirst">Select<img src="assets/images/xs/avatar3.jpg" width="10" height="10" class="down" /></div>
                                            <ul>
                                                <li class="input-option" data-value="1">
                                                    <img src="assets/images/avatar5.jpg" alt="" width="20" height="20" /> Baby shower</li>
                                                <li class="input-option" data-value="2">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Baby welcome</li>
                                                <li class="input-option" data-value="3">
                                                    <img src="assets/images/avatar7.jpg" alt="" width="20" height="20" /> Oh Baby</li>
                                                <li class="input-option" data-value="4">
                                                    <img src="assets/images/avatar7.jpg" alt="" width="20" height="20" /> It's Girl </li>
                                                <li class="input-option" data-value="5">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> It's Boy</li>
                                                <li class="input-option" data-value="6">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Happy birthday</li>
                                                <li class="input-option" data-value="7">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Welcome</li>
                                                <li class="input-option" data-value="8">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Anniversary </li>
                                            </ul>
                                            <input type="hidden" class="option" name="banner_select" value="" />
                                        </div>
                                        <div class="pillar_div mobile_disable_class">
                                            <label class="fieldlabels pillar_lable">Pillar :  </label> 
                                            <label class="fieldlabels pillar_qty">Pillar Quantity:  </label>
                                        </div>
                                        <div class="pillar_text_div mobile_disable_class">
                                            <div class="pillar_text">
                                                <input type="text" name="pillar_lable" placeholder="Pillar Size:" class="pillar_input"/>
                                            </div>
                                            <div class="wrap cls_wrap_foil">
                                                <button type="button" id="sub" class="sub">-</button>
                                                <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="pillar_qty" readonly/>
                                                <button type="button" id="add" class="add">+</button>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="pillar_div disable_class">
                                            <label class="fieldlabels pillar_lable">Pillar :  </label> 
                                            <input type="text" name="pillar_lable" placeholder="Pillar Size:" class="pillar_input"/>
                                        </div>
                                        <div class="pillar_text_div disable_class">
                                            <label class="fieldlabels pillar_qty">Pillar Quantity:  </label>
                                            <div class="wrap cls_wrap_foil">
                                                <button type="button" id="sub" class="sub">-</button>
                                                <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="pillar_qty" readonly/>
                                                <button type="button" id="add" class="add">+</button>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        <div class="clsprop_main_div">
                                            <div class="clsprop_radio">
                                                <label class="fieldlabels props_lable">Props:  </label>
                                                <input name="props" value="0" type="radio" class="radio-col-black" id="props_radio_no" checked>
                                                <label class="" for="props_radio_no">No</label>
                                                <input name="props" value="1" type="radio" class="radio-col-black" id="props_radio_yes">
                                                <label class="" for="props_radio_yes">Yes</label>
                                            </div>
                                            <div class="yes_select" style="display: none;">
                                                <div class="mm-dropdown1">
                                                    <div class="textfirst">Select<img src="assets/images/xs/avatar3.jpg" width="10" height="10" class="down" /></div>
                                                    <ul>
                                                        <li class="input-option" data-value="1">
                                                    <img src="assets/images/avatar5.jpg" alt="" width="20" height="20" /> Baby shower</li>
                                                <li class="input-option" data-value="2">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Baby welcome</li>
                                                <li class="input-option" data-value="3">
                                                    <img src="assets/images/avatar7.jpg" alt="" width="20" height="20" /> Oh Baby</li>
                                                <li class="input-option" data-value="4">
                                                    <img src="assets/images/avatar7.jpg" alt="" width="20" height="20" /> it's Girl </li>
                                                <li class="input-option" data-value="5">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> it's Boy</li>
                                                <li class="input-option" data-value="6">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Happy birthday</li>
                                                <li class="input-option" data-value="7">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Welcome</li>
                                                <li class="input-option" data-value="8">
                                                    <img src="assets/images/avatar6.jpg" alt="" width="20" height="20" /> Anniversary </li>
                                                    </ul>
                                                    <input type="hidden" class="option" name="props_select" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clsstage_main_div">
                                            <div class="clsstage_radio">
                                                <label class="fieldlabels stage_lable">Stage:  </label>
                                                <input name="stage" value="0" type="radio" class="radio-col-black" id="stage_radio_no" checked>
                                                <label class="" for="stage_radio_no">No</label>
                                                <input name="stage" value="1" type="radio" class="radio-col-black" id="stage_radio_yes">
                                                <label class="" for="stage_radio_yes">Yes</label>
                                            </div>
                                            <div class="yes_input" style="display: none;">
                                                <input type="text" name="stage_input" placeholder="Enter Your Stage Size:" />
                                            </div>
                                        </div>
                                        <div class="clssofa_main_div">
                                            <div class="clssofa_div">
                                                <label class="fieldlabels sofa_lable">Sofa:  </label>
                                                <input name="sofa" value="0" type="radio" class="radio-col-black" id="sofa_radio_no" checked>
                                                <label class="sofa" for="sofa_radio_no">No</label>
                                                <input name="sofa" value="1" type="radio" class="radio-col-black" id="sofa_radio_yes">
                                                <label class="" for="sofa_radio_yes">Yes</label>
                                            </div>
                                            <div class="machine_div">
                                                <label class="fieldlabels machine_lable">Fog machine:  </label>
                                                <input name="fog" value="0" type="radio" class="radio-col-black" id="fog_radio_no" checked>
                                                <label class="" for="fog_radio_no">No</label>
                                                <input name="fog" value="1" type="radio" class="radio-col-black" id="fog_radio_yes">
                                                <label class="" for="fog_radio_yes">Yes</label>
                                            </div>
                                        </div>
                                        <label class="fieldlabels" for="matka_qty">Fog matka :  </label>
                                        <div class="wrap fog_wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="matka_qty" readonly/>
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                        <div class="machine_main_div">

                                        </div>
                                        <label class="fieldlabels" for="payro_qty">Cold payro :  </label>
                                        <div class="wrap fog_wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="payro_qty" readonly/>
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                        <label class="fieldlabels" for="light_qty">Par light :  </label>
                                        <div class="wrap fog_wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="cls_count" type="text" id="1" value="0" min="0" max="100" name="light_qty" readonly/>
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                        <label class="fieldlabels other_info">Other Info</label>
                                        <textarea rows="4" class="form-control other_info" placeholder="Enter Other Info..!" name="other_info"></textarea>
                                    </div> 
                                    <input type="button" name="next" class="next action-button" value="Next" /> 
                                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>
                                <fieldset>
                                    <div class="form-card">
                                        <input type="hidden" name="account_id" value="" class="accountId">
                                        <div class="row">
                                            <div class="col-9">
                                                <h2 class="fs-title">Image Upload:</h2>
                                            </div>
                                            <div class="col-5 step3">
                                                <h2 class="steps">Step 3 - 4</h2>
                                            </div>
                                        </div> 
                                        <label class="fieldlabels" for="customer_image">Upload Your Photo:</label> 
                                        <input type="file" id="customer_image" name="customer_img" accept="image/*">
                                    </div> 
                                    <input type="button" name="next" class="next action-button invoiceBtn" value="Next" />
                                    <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>
                                 <fieldset>
                                    <div class="form-card">
                                        <input type="hidden" name="account_id" value="" class="accountId">
                                        <div class="row">
                                            <div class="col-7">
                                                <h2 class="fs-title">Finish:</h2>
                                            </div>
                                            <div class="col-5 step4">
                                                <h2 class="steps">Step 4 - 4</h2>
                                            </div>
                                        </div> <br>
                                        <h2 class="purple-text text-center place_sucess"><strong>SUCCESS !</strong></h2> <br>
                                        <div class="row justify-content-center">
                                            <div class="col-3"> <img src="assets/images/25404.png" class="fit-image"> </div>
                                        </div> <br><br>
                                        <div class="row justify-content-center">
                                            <div class="col-7 text-center">
                                                <h5 class="purple-text text-center place_order_msg">You Have Successfully Place an Order</h5>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Jquery Core Js --> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
        <script src="assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
        <script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js --> 
        <!-- Bootstrap Material Datetime Picker Plugin Js --> 
        <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
        <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
        <script src="assets/js/pages/forms/basic-form-elements.js"></script> 
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    </form>
</body>
</html>
<script>
    $(document).ready(function () {
    var id = "<?php echo $clsid; ?>";
    if (id !== '') {
        multistep_update(id);
    }else{
        set_order_num();
    } 
    });
</script>