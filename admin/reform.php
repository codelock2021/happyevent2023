﻿<?php
$con = mysqli_connect('localhost', 'root', '', 'vasu');
$clsid = (isset($_GET['id']) && $_GET['id'] != "") ? $_GET['id'] : '';
$dispplay_data = ($clsid == '') ? "block" : "none";
?>
<!doctype html>
<html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>:: Registration Form</title>
        <link rel="icon" href="assets/images/xs/avatar7.jpg" type="image/x-icon">
        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link href="assets/plugins/waitme/waitMe.css" rel="stylesheet" />
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <link  rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script src="../nexa/assets/js/ajax.js"></script>
    </head>
    <style>
        .theme-orange .user-info{
            display: flex;
            padding-left: 1px;
        }
        .error-msg{
            color: red;
            font-size: 15px;
        }
        .clsinsert{
            color: red;
            font-size: 25px;
        }
        .uploadimg{
            margin-bottom: 21px;
            margin-left: 15px;
        }
        .image{
            display: block;
            margin-bottom: 21px;
            margin-left: 6px;
        }
        .uploadimg1{
            display: block;
            margin-bottom: 10px;
        }
        .sidebar .user-info .image {
            margin-right: 6px; 
            float: left;
        }
    </style>
    <body class="theme-orange">
        <!-- Page Loader -->
        <form method="post" name="lform" class="lform" enctype="multipart/form-data" >
            <input type="hidden" name="id" id="clshidden" value="<?php echo $clsid; ?>">
            <input type="hidden" name="method" id="clshidden2" value="insert_data">
            <div class="page-loader-wrapper">
                <div class="loader">        
                    <div class="line"></div>
                    <div class="line"></div>
                    <div class="line"></div>
                    <p>Please wait...</p>
                    <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Nexa"></div>
                </div>
            </div>
            <!-- Overlay For Sidebars -->
            <div class="overlay"></div><!-- Search  -->
            <div class="search-bar">
                <div class="search-icon"> <i class="material-icons">search</i> </div>
                <input type="text" placeholder="Explore Nexa...">
                <div class="close-search"> <i class="material-icons">close</i> </div>
            </div>
            <?php
//        Top Bar
            include 'navbar.php';
//        Left Sidebar
            include 'sidebar.php';
            ?>
            <section class="content">
                <div class="block-header">
                    <div class="row">
                        <div class="col-lg-7 col-md-6 col-sm-12">
                            <h2>Registration From</h2>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-12">
                            <ul class="breadcrumb float-md-right">
                                <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Code Lock</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <!-- Input -->
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="card">
                                <div class="header">
                                    <h2> Enter Your Details ! </h2>
                                    <center><span class="error-msg clsinsert"></span></center>
                                </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-lg">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" id="uname" placeholder="Username" name="uname"/>
                                                </div>
                                                <span class="error-msg userName1"></span>
                                            </div>
                                            <div class="form-group form-group-lg"  style="display: <?php echo $dispplay_data; ?>">
                                                <div class="form-line">
                                                    <input type="password" class="form-control" id="password" placeholder="Password" name="password"/>
                                                </div>
                                                <span class="error-msg passWord"></span>
                                            </div>
                                            <div class="form-group form-group-lg">
                                                <div class="form-line">
                                                    <input type="email" class="form-control eMail" id="email" placeholder="Email" name="email"/>
                                                </div>
                                                <span class="error-msg eMail"></span>
                                            </div>
                                            <div class="form-group form-group-lg" style="display: <?php echo $dispplay_data; ?>">
                                                <div>
                                                    <label for="gender">Gender</label>
                                                    <input class="rdo radio-col-black" type="radio" id="male" name="gender" value="0">
                                                    <label for="male">Male</label>
                                                    <input class="rdo radio-col-black" type="radio" id="female" name="gender" value="1">
                                                    <label for="female">Female</label>
                                                </div>
                                                <span class="error-msg rdo1 rdo2"></span>
                                            </div>
                                            <div class="form-group form-group-lg">
                                                <div class="form-line">
                                                    <input type="number" class="form-control pNumber" id="pnumber" placeholder="Phone Number" name="pnumber"/>
                                                </div>
                                                <span class="error-msg pNumber"></span>
                                            </div>
                                            <div class="form-group form-group-lg">
                                                <label class="clsskill" name="skill" for="skill">Skill</label>
                                                <select class="form-control show-tick clsskill" name="skill">
                                                    <option value="0" name="skill" id="dev">Developer</option>
                                                    <option value="1" name="skill" id="pro">Programmer</option>
                                                    <option value="2" name="skill"id="doc">Doctor</option>
                                                    <option value="3" name="skill" id="dri">Driver</option>
                                                </select>
                                            </div>
                                            <div class="form-group form-group-lg">
                                                <label id="book" name="books[]">Books</label>
                                                <div class="form-line">
                                                    <input class="chb" type="checkbox" id="book3" name="books[]" value="Maths">
                                                    <label for="book3" name="books">Maths</label>
                                                    <input class="chb" type="checkbox" id="book1" name="books[]" value="Biology">
                                                    <label for="book1" name="books">Biology</label>
                                                    <input class="chb" type="checkbox" id="book2" name="books[]" value="Eco">
                                                    <label for="book2" name="books">Eco</label>
                                                </div>
                                                <span class="error-msg chb1"></span>
                                            </div>
                                            <div class="form-group form-group-lg">
                                                <div class="form-line">
                                                    <label>About</label>
                                                    <textarea rows="4" class="form-control no-resize clsabout" placeholder="Please Enter About your Self..!" name="message"></textarea>
                                                    <span class="error-msg clsmsg1"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-line">
                                            <label class="image">Profile Picture</label>
                                            <input id="uploadImage" type="file" accept="image/*" name="image" class="uploadimg"/>
                                            <input type="hidden" id="clshidden1" value="insert_data">
                                            <span class="error-msg uploadimg1"></span>
                                        </div>
                                    </div>
                                    <button type="submit" value="submit" class="btn  btn-raised bg-black submitbtn">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </section>

            <!-- Jquery Core Js --> 
            <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
            <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

            <script src="assets/plugins/autosize/autosize.js"></script> <!-- Autosize Plugin Js --> 
            <script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js --> 
            <!-- Bootstrap Material Datetime Picker Plugin Js --> 
            <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 

            <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
            <script src="assets/js/pages/forms/basic-form-elements.js"></script> 
        </form>
    </body>

    <!-- Mirrored from www.wrraptheme.com/templates/nexa/html/basic-form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 01 Jan 2020 09:24:28 GMT -->
</html>
<script>
    var id = "<?php echo $clsid; ?>";
    if (id !== '') {
        clsupdate(id);
    }
</script>