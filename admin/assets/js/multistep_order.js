$(document).ready(function () {
    $('table[data-listing="true"]').each(function () {
        load_data(1);
    });
    $(".clsmuldel").click(function () {
        var post_atr = [];
        $(".order_tbody input[type=checkbox]").each(function () {
            if ($(this).is(":checked")) {
                var id = this.id;
                post_atr.push(id);
            }
        });
        $.ajax({
            url: "multistep_ajaxcall.php",
            type: 'POST',
            data: {"id": post_atr, "method": "multidelete"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
                    $.each(json_data["id"], function (i, l) {
                        $("#tr_" + l).remove();
                    });
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                        load_data(1);
                    }, 3000);
                }
            }
        });
    });
});
function load_data(page, value) {
    $.ajax({
        url: "multistep_ajaxcall.php",
        type: 'POST',
        data: {"page": page, "value": value, "method": "set_data"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            console.log(json_data);
            $('.order_tbody').html(json_data["data"]);
            $('.pagination_html').html(json_data["pagination"]);
            $(".page-item").removeClass("active");
            $(".clsClass" + page).addClass("active");
        }
    });
}
//pagination
$(document).on('click', '.pagination_link', function () {
    var page = $(this).attr("id");
    load_data(page);
});
//searching on dashboard
$(document).on('keyup', '.clssearch', function () {
    var value = $(this).val().toLowerCase();
    if (this.value.length >= 3 || this.value.length == 0) {
        load_data(1, value);
    }
});
//delete data
$(document).on("click", ".clsdelete", function () {
    var clsthis = this;
    var clsid = $(this).data("id");
    $.ajax({
        url: "multistep_ajaxcall.php",
        type: 'POST',
        data: {"id": clsid, "method": "delete_data"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            console.log(json_data);
            if (json_data["result"] == "fail") {
                $('.clsdel').html(json_data["msg"]);
            } else {
                $(clsthis).closest("tr").remove();
                $('.clsdel').html(json_data["msg"]);
                setTimeout(function () {
                    $('.clsdel').css("display", "none");
                }, 3000);
            }
        }
    });
});

//get data
function clsupdate(id) {
    $.ajax({
        url: "multistep_ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "get_data"},
        success: function (result) {
            console.log(result);
            var json_data = $.parseJSON(result);
            $('#clshidden').val(json_data["id"]);
            $('#clshidden1').val(json_data["profile"]);
            $('#uname').val(json_data["username"]);
            $('#email').val(json_data["email"]);
            $('.clsskill').val(json_data["skill"]);
            $('#pnumber').val(json_data["number"]);
            $('.clsabout').val(json_data["about"]);
            $('.clsupdate').val(json_data["msg"]);
        }});
}