$(document).ready(function () {
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;
    setProgressBar(current); //set progress bar at 1
    
    function nextClick(thisObj){
         current_fs = $(thisObj).parent();
        next_fs = $(thisObj).parent().next();//Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active"); //show the next fieldset
        next_fs.show(); //hide the current fieldset with style

        current_fs.animate({opacity: 0}, {
            step: function (now) {
// for making fielset appear animation
                opacity = 1 - now;
                current_fs.css({'display': 'none', 'position': 'relative'});
                next_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(++current); //animated progress bar move ahead
    }
   
    $(".previous").click(function () {
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
//Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
//show the previous fieldset
        previous_fs.show();
//hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now) {
// for making fielset appear animation
                opacity = 1 - now;
                current_fs.css({'display': 'none', 'position': 'relative'});
                previous_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(--current);
    });
    function setProgressBar(curStep) {
        var percent = parseFloat(100 / steps) * curStep; //set a progress bar value to 25/50/75/100 like this
        percent = percent.toFixed();
        $(".progress-bar").css("width", percent + "%")
    }
    $(".submit").click(function () {
        return false;
    });

    $(document).on("click", ".action-button", function (event) {
        event.preventDefault();
        var thisObj = $(this);
        var form_data = new FormData($('form')[0]);
        $.ajax({
            url: "multistep_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
//                console.log(result);
                var json_data = $.parseJSON(result);
                console.log(json_data);
                if (json_data.result === 'fail') {
                    console.log(json_data.data);
                    $(".error_msg").find("ul").html('');
                    $.each(json_data.data, function (key, value) {
                        $(".error_msg").css('display', 'block');
                        $(".error_msg").find("ul").append('<li>' + value + '</li>');
                    });
//                    json_data["data"] !== undefined ? $(".error_msg").append(json_data["data"]) : $(".error_msg").html("");
                } else {
                    $(".accountId").val(json_data["account_id"]);
                    nextClick(thisObj);
                    $('.clsinsert').css("display", "block");
                    $('.clsinsert').html(json_data["msg"]);
                }
            }
        });
    });
    $('.add').click(function () {
        var th = $(this).closest('.wrap').find('.cls_count');
        th.val(+th.val() + 1);
    });
    $('.sub').click(function () {
        var th = $(this).closest('.wrap').find('.cls_count');
        if (th.val() > 1)
            th.val(+th.val() - 1);
    });
    //radio button click show select box
    $('.props_col_yes').click(function () {
        console.log($(this).val());
        if ($(this).val() === 'on') {
            $('.yes_select').show();
        } else if ($(this).val() === '') {
            $('.yes_select').hide();
        }
    });
    //radio button click show input field
    $('.radio_stage_yes').click(function () {
        console.log($(this).val());
        if ($(this).val() === 'on') {
            $('.yes_input').show();
        } else if ($(this).val() === '') {
            $('.yes_input').hide();
        }
    });


//tags input fields
//    var tags = $("#inputTag").val();
//    console.log(tags);
//    $("#inputTag").tagsinput('items');

});
//select box with image
$(function () {
    // Set
    var main = $('div.mm-dropdown .textfirst');
    var li = $('div.mm-dropdown > ul > li.input-option');
    var inputoption = $("div.mm-dropdown .option");
    var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="20" height="20" class="down" />';

    // Animation
    main.click(function () {
        main.html(default_text);
        li.toggle('fast');
    });

    // Insert Data
    li.click(function () {
        // hide
        li.toggle('fast');
        var livalue = $(this).data('value');
        var lihtml = $(this).html();
        main.html(lihtml);
        inputoption.val(livalue);
    });
});

$(function () {
    // Set
    var main = $('div.mm-dropdown1 .textfirst');
    var li = $('div.mm-dropdown1 > ul > li.input-option');
    var inputoption = $("div.mm-dropdown1 .option");
    var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="20" height="20" class="down" />';

    // Animation
    main.click(function () {
        main.html(default_text);
        li.toggle('fast');
    });

    // Insert Data
    li.click(function () {
        // hide
        li.toggle('fast');
        var livalue = $(this).data('value');
        var lihtml = $(this).html();
        main.html(lihtml);
        inputoption.val(livalue);
    });
});


function set_order_num() {
    $.ajax({
        url: "multistep_ajaxcall.php",
        type: 'POST',
        data: {"method": "set_order_num"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            console.log(json_data);
            $('.cls_or_num').val(json_data);
        }
    });
}


