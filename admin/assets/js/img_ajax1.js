//multistep_form and order JS
var filter_array = {};
var filter_cash_array = {};
$(document).ready(function () {
    
    //    total amount show basia on advance and ramain amount
    $(document).on("keyup", ".advanceAmount", function () {
        var adamount = $(this).val();
        var reamount = $(".remainAmount").val();
        var total = (+adamount) + (+reamount);
        $(".totalAmount").val(total);
    });
    $(document).on("keyup", ".remainAmount", function () {
        var adamount = $(".advanceAmount").val();
        var reamount = $(this).val();
        var total = (+adamount) + (+reamount);
        $(".totalAmount").val(total);
    });
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;
    setProgressBar(current); //set progress bar at 1

    $(".export_to_pdf").click(function(){
        $("#leftsidebar").hide();
        $(".navbar").hide();
        $(".block-header").hide();
        setTimeout(function(){
            window.print();
            $("#leftsidebar").show();
            $(".navbar").show();
            $(".block-header").show();
        },1000);
    });


    function nextClick(thisObj) {
        current_fs = $(thisObj).parent();
        next_fs = $(thisObj).parent().next();//Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active"); //show the next fieldset
        next_fs.show(); //hide the current fieldset with style

        current_fs.animate({opacity: 0}, {
            step: function (now) {
// for making fielset appear animation
                opacity = 1 - now;
                current_fs.css({'display': 'none', 'position': 'relative'});
                next_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(++current); //animated progress bar move ahead
    }

    $(".previous").click(function () {
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
//Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
//show the previous fieldset
        previous_fs.show();
//hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now) {
// for making fielset appear animation
                opacity = 1 - now;
                current_fs.css({'display': 'none', 'position': 'relative'});
                previous_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(--current);
    });
    function setProgressBar(curStep) {
        var percent = parseFloat(100 / steps) * curStep; //set a progress bar value to 25/50/75/100 like this
        percent = percent.toFixed();
        $(".progress-bar").css("width", percent + "%")
    }
    $(".submit").click(function () {
        return false;
    });

    $(document).on("click", ".action-button", function (event) {
        event.preventDefault();
        var thisObj = $(this);
        var form_data = new FormData($('form')[0]);
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
//                console.log(result);
                var json_data = $.parseJSON(result);
                console.log(json_data);
                if (json_data.result === 'fail') {
                    nextClick(thisObj);
                    console.log(json_data.data);
                    $(".error_msg").find("ul").html('');
                    $.each(json_data.data, function (key, value) {
                        $(".error_msg").css('display', 'block');
                        $(".error_msg").find("ul").append('<li>' + value + '</li>');
                    });
//                    json_data["data"] !== undefined ? $(".error_msg").append(json_data["data"]) : $(".error_msg").html("");
                } else {
                    $(".accountId").val(json_data["account_id"]);
                    nextClick(thisObj);
                    $('.clsinsert').css("display", "block");
                    $('.clsinsert').html(json_data["msg"]);
                }
            }
        });
    });
    $('.add').click(function () {
        var th = $(this).closest('.wrap').find('.cls_count');
        th.val(+th.val() + 1);
    });
    $('.sub').click(function () {
        var th = $(this).closest('.wrap').find('.cls_count');
        if (th.val() > 1)
            th.val(+th.val() - 1);
    });
    //radio button click show select box
    $('#props_radio_yes').click(function () {
        console.log($(this).val());
        if ($(this).val() === '1') {
            $('.yes_select').show();
        } else if ($(this).val() === '') {
            $('.yes_select').hide();
        }
    });
    //radio button click show input field
    $('#stage_radio_yes').click(function () {
        console.log($(this).val());
        if ($(this).val() === '1') {
            $('.yes_input').show();
        } else if ($(this).val() === '') {
            $('.yes_input').hide();
        }
    });
    //complete button click show tag input and submit button
    $('.complete').click(function () {
        var text = $(this).attr('value');
        if (text === '1') {
            $('.yes_complete_tag').show();
        } else if (text === '') {
            $('.yes_complete_tag').hide();
        }
    });
    //payment done button click show select box
    $('.payment_recive').click(function () {
        var text = $(this).attr('value');
        console.log(text);
        if (text === '3') {
            $('.yes_payment_select').show();
        } else if (text === '') {
            $('.yes_payment_select').hide();
        }
    });
    
    $(".clsmuldel").click(function () {
        var post_atr = [];
        $(".order_tbody input[type=checkbox]").each(function () {
            if ($(this).is(":checked")) {
                var id = this.id;
                post_atr.push(id);
            }
        });
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"id": post_atr, "method": "multidelete"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
                    $.each(json_data["id"], function (i, l) {
                        $("#tr_" + l).remove();
                    });
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                        load_data(1);
                    }, 3000);
                }
            }
        });
    });
});


//customer invoice data function call
function customer_order_data() {
    $('table[data-listing="true"]').each(function () {
        var id = $("#clshidden").val();
        invoice_data(id);
        return false;
    });
}
setTimeout(function () {
    $(".customer_address").each(function () {
        text = $(".customer_address").text();
        if (text.length > 40) {
            $(this).html(text.substr(0, 20) + '<span class="elipsis">' + text.substr(20) + '</span><a class="elipsis" href="#">...</a>');
        }
    });
    $(".customer_address > a.elipsis").click(function (e) {
        e.preventDefault(); //prevent '#' from being added to the url
        $(this).prev('span.elipsis').fadeToggle(500);
    });
}, 1000);


function services_image() {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": "set_services_image"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            $(".oursevices_admin").html(json_data["data"]);
        }
    });
}
function list_all_review() {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": "list_all_review_fun"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            $(".clientreview_div").html(json_data["data"]);
        }
    });
}

//customer invoice data
function invoice_data(id) {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "invoice_data"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            console.log(json_data);
            $('.invoice_head_body').html(json_data["customer_data"]);
            $('.customerDetailsHtml').html(json_data["customer_mobile_data"]);
            $('.invoice_body_pro').html(json_data["item_data"]);
        }
    });
}
//multistep order table data function call
function order_details_data() {
    $('table[data-listing="true"]').each(function () {
        load_data(1,'0','on_load');
    });
}

//multistep order table data
function load_data(page, value, call_from) {
    var status = $('.show_order_data_filter option:selected').val();
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"page": page, "value": value,"call_from":call_from,"method": "set_data","filter_data":filter_array},
        success: function (data) {
            var json_data = $.parseJSON(data);
            $('.order_tbody').html(json_data["data"]);
            $('.pagination_html').html(json_data["pagination"]);
            $(".page-item").removeClass("active");
            $(".clsClass" + page).addClass("active");
        }
    });
}

//order form dropdown color change function
function order_status_dropdown_function(){
    var color = $('#order_status_dropdown option:selected').css("background-color");
    $("#order_status_dropdown").attr("#order_status_dropdown", color);
}


//pagination
$(document).on('click', '.pagination_link', function () {
    var page = $(this).attr("id");
    load_data(page,"0","on_load");
});
//searching on dashboard
$(document).on('keyup', '.clssearch', function () {
    var value = $(this).val();
    filter_array["serach_value"] = value;
    if (this.value.length >= 3 || this.value.length == 0) {
         load_data(1, value, "on_search");
    }
});
//delete data
$(document).on("click", ".clsdelete", function () {
    var clsthis = this;
    var clsid = $(this).data("id");
    var result = confirm("Want to delete?");
    if (result == true) {
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"id": clsid, "method": "delete_data"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
                    $(clsthis).closest("tr").remove();
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                    }, 3000);
                }
            }
        });
    }else{
        alert("something went wrong");
    }
});
//get data
function clsupdate(id) {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "get_data"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            $('#clshidden').val(json_data["id"]);
            $('#clshidden1').val(json_data["profile"]);
            $('#uname').val(json_data["username"]);
            $('#email').val(json_data["email"]);
            $('.clsskill').val(json_data["skill"]);
            $('#pnumber').val(json_data["number"]);
            $('.clsabout').val(json_data["about"]);
            $('.clsupdate').val(json_data["msg"]);
        }});
}
//select box with image
$(function () {
    // Set
    var main = $('div.mm-dropdown .textfirst');
    var li = $('div.mm-dropdown > ul > li.input-option');
    var inputoption = $("div.mm-dropdown .option");
    var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="20" height="20" class="down" />';

    // Animation
    main.click(function () {
        main.html(default_text);
        li.toggle('fast');
    });

    // Insert Data
    li.click(function () {
        // hide
        li.toggle('fast');
        var livalue = $(this).data('value');
        var lihtml = $(this).html();
        main.html(lihtml);
        inputoption.val(livalue);
    });
});

$(function () {
    // Set
    var main = $('div.mm-dropdown1 .textfirst');
    var li = $('div.mm-dropdown1 > ul > li.input-option');
    var inputoption = $("div.mm-dropdown1 .option");
    var default_text = 'Select<img src="https://cdn4.iconfinder.com/data/icons/ionicons/512/icon-arrow-down-b-128.png" width="20" height="20" class="down" />';

    // Animation
    main.click(function () {
        main.html(default_text);
        li.toggle('fast');
    });

    // Insert Data
    li.click(function () {
        // hide
        li.toggle('fast');
        var livalue = $(this).data('value');
        var lihtml = $(this).html();
        main.html(lihtml);
        inputoption.val(livalue);
    });
});


function set_order_num() {
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": "set_order_num"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            console.log(json_data);
            $('.cls_or_num').val(json_data);
        }
    });
}

//multistep order page dropdown update call
$(document).on("change", ".show_order_status", function () {
    var thisObj = this;
    var value = $(this).find('option:selected').val();
    var id = $(this).find('option:selected').attr("data-id");
    var clsclass = $(this).find('option:selected').attr("class");
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"dropdown": value, "id": id, "class": clsclass, "method": "set_order_status"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            if (json_data["result"] == "Fail") {
                $('.cls_order_msg').html(json_data['msg']);
            } else {
                if(json_data["class"] == "panding_color"){
                    $(thisObj).css("background-color","yellow");
                }else if (json_data["class"] == "complete_color"){
                    $(thisObj).css("background-color","#b3e5b3");
                }else if (json_data["class"] == "cancel_color"){
                    $(thisObj).css("background-color","red");
                }else if (json_data["class"] == "done_color"){
                    $(thisObj).css("background-color","green");
                }
//                $(thisObj).closest("tr").find(".show_order_status").addClass(json_data["class"]);
                $('.cls_order_msg').html(json_data["msg"]);
                setTimeout(function () {
                    $('.cls_order_msg').css("display", "none");
                }, 3000);
            }
        }
    });
});

//login process
$(document).on("click", "#clssignin", function (event) {
    event.preventDefault();
    var email = $('#email').val();
    var password = $('#password').val();
    if (email !== '' && password !== '') {
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'post',
            data: {"email": email, "password": password, "method": "clssignin"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                if (json_data["msg"] == "success") {
                    $('.userName1').html(json_data["data"]);
                    window.location.href = "img_gallery.php";
                } else {
                    $('.userName1').html(json_data["data"]);
                    return false;
                }
            }
        });
    } else {
        $(".userName1").text("Please Enter Email And password !");
    }
});
function set_image() {
    var value = $('.lvpvctgry option:selected').val();
    pagination_image(1, value);
}

$(document).on("click", ".clsremove", function (event) {
    event.preventDefault();
    var clsthis = this;
    var clsid = $(this).data("id");
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": clsid, "method": "clsdelete_data"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            if (json_data["result"] == "Fail") {
                $('.clsdel').html(json_data['error']);
            } else {
                $(clsthis).closest('#clsdels' + clsid).remove();
                $('.clsdel').html(json_data["msg"]);
                setTimeout(function () {
                    $('.clsdel').css("display", "none");
                }, 3000);
                set_image();
            }
        }
    });
});
$(document).on("click", ".clsremove_review", function (event) {
    event.preventDefault();
    var clsthis = this;
    var clsid = $(this).data("id");
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": clsid, "method": "clsdelete_data_review"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            if (json_data["result"] == "Fail") {
                $('.clsdel').html(json_data['error']);
            } else {
                $(clsthis).closest('#clsdels' + clsid).remove();
                $('.clsdel').html(json_data["msg"]);
                setTimeout(function () {
                    $('.clsdel').css("display", "none");
                }, 3000);
                list_all_review();
            }
        }
    });
});

$(document).on("click", ".clsremove_services", function (event) {
    event.preventDefault();
    var clsthis = this;
    var clsid = $(this).data("id");
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": clsid, "method": "clsdelete_data_services"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            if (json_data["result"] == "Fail") {
                $('.clsdel').html(json_data['error']);
            } else {
                $(clsthis).closest('#clsdels' + clsid).remove();
                $('.clsdel').html(json_data["msg"]);
                setTimeout(function () {
                    $('.clsdel').css("display", "none");
                }, 3000);
                services_image();
            }
        }
    });
});
$(document).on("click", ".edit_services", function (event) {
    event.preventDefault();
    var clsthis = this;
    var clsid = $(this).data("id");
    $(".clsicon").css("display", "inherit");
    $(".img_our_services_ ").removeClass('hidden');
    $(".box-tools,.box-header").addClass('hidden');
    var form_data = new FormData($('#ourservicesForm')[0]);
    form_data.append("id", clsid);
    form_data.append("method", "get_services_data");
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        processData: false,
        contentType: false,
        data: form_data,
        success: function (result) {
            var json_data = $.parseJSON(result);
            // var image =json_data['images'];
            $("#services_title").val(json_data['service_name']);
            $("#services_url").val(json_data['url']);
            $("#services_id").val(json_data['id']);
            $(".preview-zone").show();
            $(".box222 .box-body .img_our_services_").attr("src",'../admin/assets/images/OurServices/'+ json_data['images']);
            $("#services_new_name").val(json_data['images']);

            if (json_data["result"] == "fail") {
                $('#clsicon').css("display", "none");   
                $('.client-name').html(json_data["error"]["name"]);
                $('.category-review').html(json_data["error"]["category"]);
                $('.description').html(json_data["error"]["description"]);
                $('.review-img').html(json_data["error"]["image"]);
            } else {
                $('#clsicon').css("display", "none");   
                $('.fileupload1').html(json_data["msg"]);
                $("#reviewForm")[0].reset();
                setTimeout(function () {
                    $('.fileupload1').html("");
                }, 2000);
            }
        }
    });
});

$(document).on("change", ".lvpvctgry", function (event) {
    event.preventDefault();
    var value = $('.lvpvctgry option:selected').val();
    set_image();
    pagination_image(1, value);
});

//multistep order page dropdown data call
$(document).on("change", ".bootstrap-select.show_order_data_filter", function (event) {
    event.preventDefault();
    var status = $('.show_order_data_filter option:selected').val();
   filter_array["status_value"] = status;
    load_data(1, status, "on_status_change");
});

//Filter By Date
$(document).on("change", '#filter_date', function () {
    var date = new Date($('#filter_date').val());
    var date_value = $("#filter_date").val();
    filter_array["date_value"] = date_value;
    load_data(1, date_value ,"on_date_filter");
});


function pagination_image(page) {
    var value = $('.lvpvctgry option:selected').val();
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"value": value, "page": page, "method": "set_image"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            console.log(json_data["error"]);
            if (json_data["result"] == "Fail") {
                $('.clspreview').html("");
                $('.pagination_html').html("");
                $('.clsdata').html(json_data["error"]);
            } else {
                $('.clsdata').html("");
                $('.clspreview').html(json_data["data"]);
                $('.pagination_html').html(json_data["pagination"]);
                $(".page-item").removeClass("active");
                $(".clsCls" + page).addClass("active");
            }
        }
    });
}

$(document).on('click', '.pgntn_link', function () {
    var page = $(this).attr("id");
    pagination_image(page);
});


function upload() {
    $('#fileupload').click();
}

$(function () {
    $("#fileupload").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#dvPreview");
            var i = 1;
            var clstag = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp|.webp)$/;
            $($(this)[0].files).each(function () {
                if (i > 10) {
                    $("#dvPreview").addClass("scrollbar");
                }
                var file = $(this);
                if (clstag.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var div = $("<div class='clsdiv'>");
                        var img = $("<img class='clsimg' />");
                        img.attr("src", e.target.result);
                        var btn = $("<button class='btn  btn-raised btn-danger waves-effect remove'>X</button>");
                        div.attr("</div>");
                        div.append(img);
                        div.append(btn);
                        dvPreview.append(div);
                        $(".remove").click(function () {
                            var clsthis = this;
                            $(clsthis).closest(".clsdiv").remove();
                        });
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    setTimeout(function () {
                        $('.clsfile').css("display", "none");
                    }, 2000);
                    return false;
                }
                i++;
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
    $(".btnupload_img").click(function (event) {
        event.preventDefault();
        $(".clsicon").css("display", "inherit");
        var form_data = new FormData($('form')[0]);
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
                var json_data = $.parseJSON(result);
                $('.clsname').html("");
                $('.clscate').html("");
                $('.clssize').html("");
                if (json_data["result"] == "fail") {
                    $('.clsname').html(json_data["error"]["image"]);
                    $('.alt_tag_img').html(json_data["error"]["alt_name"]);
                    $('.clscate_error').html(json_data["error"]["category"]);
                    $('.result_image').html(json_data["result_image"]);
                    $('.clssize').html(json_data["error"]["size"]);
                    $('#clsicon').css("display", "none");
                } else {
                    set_image();
                    $('.fileupload1').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.fileupload1').css("display", "none");
                        $('#clsicon').css("display", "none");
                        $('.display_none_preview').hide();
                        $('#clientName').val("");
                    }, 2000);
                }
            }
        });
    });
});
    $("#clientReviewForm").click(function (event) {
        event.preventDefault();
        $(".clsicon").css("display", "inline-block");
        var form_data = new FormData($('#reviewForm')[0]);
        form_data.append("method", "reviewform");
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                $('.client-name').html("");
                $('.category-review').html("");
                $('.description').html("");
                $('.review-img').html("");
                if (json_data["result"] == "fail") {
                    $('#clsicon').css("display", "none");
                    $('.client-name').html(json_data["error"]["name"]);
                    $('.category-review').html(json_data["error"]["category"]);
                    $('.description').html(json_data["error"]["description"]);
                    $('.review-img').html(json_data["error"]["image"]);
                } else {
                    list_all_review();
                    $('.preview-zone').hide();
                    $('.fileupload1').html(json_data["msg"]);
                    $("#reviewForm")[0].reset();
                    setTimeout(function () {
                        $('#clsicon').css("display", "none");
                        $('.fileupload1').html("");
                    }, 2000);
                }
            }
        });
    });

    $("#bottomslider").click(function (event) {
        event.preventDefault();
        $(".clsicon").css("display", "inline-block");
        var form_data = new FormData($('#bottmSliderForm')[0]);
        form_data.append("method", "bottomslider");
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                $('.title-slider').html("");
                $('.short-description').html("");
                $('.long-description').html("");
                $('.slider-img').html("");
                if (json_data["result"] == "fail") {
                    $('#clsicon').css("display", "none");
                    $('.title-slider').html(json_data["error"]["title"]);
                    $('.short-description').html(json_data["error"]["shortdescription"]);
                    $('.long-description').html(json_data["error"]["longdescription"]);
                    $('.slider-img').html(json_data["error"]["image"]);
                } else {
                    $('.fileupload1').html(json_data["msg"]);
                    $("#bottmSliderForm")[0].reset();
                    setTimeout(function () {
                        $('#clsicon').css("display", "none");
                        $('.fileupload1').html("");
                    }, 2000);
                }
            }
        });
    });

    $("#btn_our_services").click(function (event) {
        event.preventDefault();
        $(".clsicon").css("display", "inline-block");
        var form_data = new FormData($('#ourservicesForm')[0]);
        form_data.append("method", "our_services_fun");
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            processData: false,
            contentType: false,
            data: form_data,
            success: function (result) {
                var json_data = $.parseJSON(result);  
                services_image();   
                $('#services_title').html("");
                $('#services_url').html("");
                if (json_data["result"] == "fail") {
                    $('#clsicon').css("display", "none");
                    $('.title_ser').html(json_data["error"]["title"]);
                   if(json_data["error"]["url"] != undefined){$('.url_ser').html(json_data["error"]["url"]);}else{$('.url_ser').html("");}
                   if(json_data["error"]["image"] != undefined){$('.image_ser').html(json_data["error"]["image"]);}else{$('.image_ser').html("");}
                   if(json_data["error"]["title"] != undefined){$('.title_ser').html(json_data["error"]["title"]);}else{$('.title_ser').html("");}
                } else {
                    $('.preview-zone').addClass('hidden');
                    $('.img_our_services_').addClass('hidden');
                    $('.url_ser,.title_ser,.image_ser').html("");
                    $(".ovr_services").hide();
                    $(".box-tools,.box-header").addClass('hidden');
                    $('.fileupload1').html(json_data["msg"]);
                    $("#ourservicesForm")[0].reset();
                    $("#services_id").val("");
                    $(".img_our_services_").attr("src","");
                    $("#services_new_name").val("");
                    // location.reload(true); 
                    $(".our_srvs").attr("src","");
                    setTimeout(function () {
                        $('#clsicon').css("display", "none");
                        $('.fileupload1').html("");
                    }, 2000);
                }
            }
        });
    });


//step-3 next button click send to invoice page
$(document).on('click', '.invoiceBtn', function () {
    var id = $(".accountId").val();
    window.location = 'invoice_data.php?id=' + id;
});



//cashbook page js

//cashbook page insert data
function clscashbook_insertdata() {
    $(".cashClick").click(function (event) {
        event.preventDefault();
        var cash_status = $(this).attr("data-value");
        if (cash_status == '0') {
            cash_status = 'Cash In';
            var classs_name = "cash_in_entry";
        } else {
            cash_status = 'Cash Out';
            var classs_name = "cash_out_entry";
        }
        var amount = $(".cashbook_amount").val();
        var date = $(".user_date").val();
        var user = $(".show_user_Data option:selected").html();
        var remark = $(".remark_amount").val();
        if (amount != '' && date != '' && user != '') {
            var html = "<div class='user_data_show'><span class='user_name_add'>" + user + "</span><span class='user_amount_add "+classs_name+"'>₹ " + amount + "</span></div>";
            html += "<div class='user_date_add'><span>" + date + "</span></div>";
            html += "<div class='user_remark_add'><span>" + remark + "</span></div>";
            html += "<div class='user_status_add "+classs_name+"'><span>" + cash_status + "</span></div>";
            $(".cashbook_html").html(html);
        } else {
            $(".alert-danger").css({'display': 'block'});
            $(".dataBlank").html("Please Fill All The Field");
        }
        if (cash_status == 'Cash In') {
            cash_status = '0';
        } else {
            cash_status = '1';
        }
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"amount": amount, "date": date, "user": user, "remark": remark, "cash_status": cash_status, "method": "cashbook_insertdata"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
            }
        });
    });
}
function clscashbook_show_data(page, value, call_from){
    $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"page":page, "value":value, "call_from":call_from,"method": 'cashbook_show_data', "filter_data":filter_cash_array},
            success: function (result) {
                 var json_data = JSON.parse(result);
                $(".cashbook_data_html").html(json_data["data"]);
                $(".totalAmount").html("Total = " + json_data["total_amount"]);
                if(json_data["total_amount"] < 0){
                    $(".totalAmount").css("color","red");
                }else{
                    $(".totalAmount").css("color","green");
                }
            }
        });
}
$(document).on('keyup', '.cls_cashbook_search', function () {
    var value = $(this).val();
    filter_cash_array["serach_value"] = value;
    if (this.value.length >= 3 || this.value.length == 0) {
        clscashbook_show_data(1, value, "oncashbook_search");
    }
});
$(document).on("change", '#cashbook_filter_date', function () {
    var date = new Date($('#cashbook_filter_date').val());
    var date_value = $("#cashbook_filter_date").val();
    filter_cash_array["date_value"] = date_value;
    clscashbook_show_data(1, date_value, "oncashbook_date");
});
$(document).on("change", ".bootstrap-select.show_user_data_filter", function (event) {
    event.preventDefault();
    var status = $('.show_user_data_filter option:selected').html();
    filter_cash_array["status_value"] = status;
    clscashbook_show_data(1, status, 'oncashbook_status');
});




//baby shower image load page
function set_babyshower_image(data){
    $.ajax({
            url: "admin/img_ajaxcall.php",
            type: 'POST',
            data: {"method": 'babyshower_image_set',"data":data},
            success: function (result) {
                 var json_data = JSON.parse(result);
                $(".baby_flex").html(json_data["data"]);
            }
        });
}

//index.php page photo gallery image set
function set_photo_gallery(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'photo_gallery_img_set'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".titlephotoGalleryImg").html(json_data["titlephotoGalleryImg"]);
            $(".photoGalleryImg").html(json_data["data"]);
        }
    });
}

function set_slider_images(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'slider_img_set'},
        success: function (result) {
            var json_data = JSON.parse(result);
                $(".sliderImages").html(json_data["data"]);
                $(".one-time").slick({dots:!1,slidesToShow:1,arrows:!0,autoplaySpeed:6e3,autoplay:!0});
        }
    });
}
function set_mobile_slider_images(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'slider_mobile_view_img_set'},
        success: function (result) {
            var json_data = JSON.parse(result);
                $(".sliderImages_mobile").html(json_data["data"]);
                $(".one-time2").slick({dots:!0,slidesToShow:1,arrows:!1,autoplaySpeed:2e3,autoplay:!0});
        }
    });
}


function set_bottom_slider_images(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'bottom_slider_img_set'},
        success: function (result) {
            var json_data = JSON.parse(result);
                $(".swiper-wrapper").html(json_data["data"]);
                (function ($) {
    if (window.innerWidth < 1200) {
        new Swiper(".swiper-container", {
            direction: "horizontal",
            slidesPerView: 1,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            paginationClickable: !0,
            spaceBetween: 0,
            speed: 3500,
            autoplay: 7000,
            loop: !0
        })
    } else {
        new Swiper(".swiper-container", {
            direction: "horizontal",
            slidesPerView: 1,
            parallax: !0,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            paginationClickable: !0,
            spaceBetween: 0,
            speed: 3500,
            parallax: !0,
            autoplay: 7000,
            loop: !0
        })
    };	
})(jQuery);
                // $(".bottomSlider").slick({dots:!0,slidesToShow:1,arrows:!1,autoplaySpeed:2e3,autoplay:!0});
        }
    });
}

function client_review(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'client_review'},
        success: function (result) {
            var json_data = JSON.parse(result);
                $(".title_image_testtim").html(json_data["title_image_testtim"]);
                $(".testim").html(json_data["data"]);
                // $(".testimonials .testim").slick({slidesToShow:3,slidesToScroll:1,infinite:!0,dots:!0,arrows:!1,autoplay:!1,responsive:[{breakpoint:991,settings:{slidesToShow:1}},{breakpoint:768,settings:{slidesToShow:1}}]});
                $(".testimonials .testim").slick({slidesToShow:6,slidesToScroll:1,infinite:!0,dots:!1,arrows:!0,autoplay:!1,responsive:[{breakpoint:1460,settings:{slidesToShow:4}},{breakpoint:991,settings:{slidesToShow:3}},{breakpoint:768,settings:{slidesToShow:1}}]});
        }
    });
}

//index.php page Our Services images set 
function set_our_services(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'our_services_img_set'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".titleourServices").html(json_data["titleourServices"]);
            $(".ourServices").html(json_data["data"]);
        }
    });
}

//index.php page Video gallery images set 
function set_video_gallery(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'vidoe_gallery_img_set'},
        success: function (result) {
            var json_data = JSON.parse(result);
            setTimeout(function () {
                    $(".title_videogallery").html(json_data["title_videogallery"]);
                    $(".videoGallery").html(json_data["data"]);
                    },500);
        }
    });
}



//video add page data insert function
$(document).on("click", ".video_link_add", function () {
    var video_link = $(".video_link").val();
    $(".clsicon").css("display", "inline-block");
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'video_link_insert',"video": video_link},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".baby_flex").html(json_data["data"]);
            $('.clsvideo').css("display", "block");
            $('.clsvideo').html(json_data["msg"]);
            setTimeout(function () {
                    $('#clsicon').css("display", "none");
                    $('.clsvideo').css("display", "none");
                    set_video_pages();
                }, 3000);
        }
    });
});
//one  video add  data insert function
    $(document).on("submit","#que_link_form", function (e) {
        e.preventDefault(); 
        var form_data = $("#que_link_form")[0];
        var video_link = $(".video_link_home_page").val();
        $(".clsicon").css("display", "inline-block");
        var form_data = new FormData(form_data);
        form_data.append('video',video_link);
        form_data.append('method','video_link_insert_home_page'); 
        $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        dataType: "json",
        contentType: false,
        processData: false,
        data: form_data,
        success: function (result) {
            $(".preview-zone").hide();
            // $(".preview-zone .box222 .box-body > img").attr("src","");
            // var json_data = JSON.parse(result);
            $(".baby_flex").html(result["data"]);
            $('.clsbvideo').css("display", "block");
            $('.clsbvideo').html(result["msg"]);
            setTimeout(function () {
                $('#clsicon').css("display", "none");
                $('.clsbvideo').css("display", "none");
                    // set_video_pages();
                }, 3000);
        }
    });
});

//autoplay video slider insert function
$(document).on("submit","#video_slider", function (e) {
    e.preventDefault(); 
    var form_data = $("#video_slider")[0];
    // var video_link = $(".video_link_home_page").val();
    $(".clsicon").css("display", "inline-block");
    var form_data = new FormData(form_data);
    // form_data.append('video',video_link);
    form_data.append('method','autoplay_video_slider'); 
    $.ajax({
    url: "img_ajaxcall.php",
    type: 'POST',
    dataType: "json",
    contentType: false,
    processData: false,
    data: form_data,
    success: function (result) {
        $(".baby_flex").html(result["data"]);
        $('.clsslider_vdo').css("display", "block");
        $('.clsslider_vdo').html(result["msg"]);
        setTimeout(function () {
                $('#clsicon').css("display", "none");
                $('.clsslider_vdo').css("display", "none");
                // set_video_pages();
            }, 3000);
    }
});
});

//Set video page table data function
function set_video_pages(){
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'video_get_data'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".video_tbody").html(json_data["data"]);
        }
    });
}


//contact us page send php mailer

$(document).on("click", "#mailerButton", function (event) {
    event.preventDefault();
    var name=$("#name").val();
    var email=$("#email").val();
    var subject=$("#subject").val();
    var messages=$("#messages").val();
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'contact_mailer',"name":name,"email":email,"subject":subject,"messages":messages},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".emailMassage").html(json_data["msg"]);
        }
    });    
});

function scroll_fixed_div(){
    if (window.matchMedia('(max-width: 600px)').matches) {
      var fixmeTop = $('.fix_div').offset().top;
        $(window).scroll(function() {
            var currentScroll = $(window).scrollTop();
            if (currentScroll >= fixmeTop) {
                $('.fix_div').css({
                    position: 'fixed',
                    top: '60px',
                    left: '16px',
                    width: '92%',
                    zIndex: '100',
                    backgroundColor: 'white'
                });
            } else {
                $('.fix_div').css({
                    position: 'static'
                });
            }  
        });
    }
}


//Video gallery page set function
function set_video_gallery_page(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'video_gallery_set_data'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".videoGallerySetData").html(json_data["data"]);
        }
    });
}


//autoplay video slider set function
function set_autoplay_video_slider_page(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'autoplay_video_slider_set_data'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".main_div_slider_video").html(json_data["data"]);
            setVideoSlick();

        }
    });
}

function set_one_video_section(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'set_one_video_section_fun'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".one_video_set").html(json_data["data"]);
        }
    });
}


//Photo gallery page set data
function setPhotogalleryImage(){
    $.ajax({
        url: "admin/img_ajaxcall.php",
        type: 'POST',
        data: {"method": 'photo_gallery_set_data'},
        success: function (result) {
            var json_data = JSON.parse(result);
            $(".photogallerryDiv").html(json_data["data"]);
        }
    });
}

//delete video link on video page
$(document).on("click", ".clsVideoDelete", function () {
    var clsthis = this;
    var clsid = $(this).data("id");
    var result = confirm("Want to delete?");
    if (result == true) {
        $.ajax({
            url: "img_ajaxcall.php",
            type: 'POST',
            data: {"id": clsid, "method": "video_delete_data"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
                    $(clsthis).closest("tr").remove();
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                    }, 3000);
                }
            }
        });
    } else {
        alert("something went wrong");
    }
});


// In multistep form when update function data set in the form
function multistep_update(id){
    $.ajax({
        url: "img_ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "multistep_get_data"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            $('.cls_or_num').val(json_data["order_num"]);
            $('#clsselect').val(json_data["order_by"]);
            $('.multiDate').val(json_data["date"]);
            $('.multiTime').val(json_data["time"]);
            $('#categorySelect').val(json_data["type"]);
            $('.clientName').val(json_data["client_name"]);
            $('.clientNumber').val(json_data["client_contact"]);
            $('.advanceAmount').val(json_data["advance_amount"]);
            $('.remainAmount').val(json_data["remain_amount"]);
            $('.totalAmount').val(json_data["total_amount"]);
            $('.address').val(json_data["address"]);
        }});
}
$(document).on("submit", "#addcontact", function (e) {
        e.preventDefault(); 
        var form_data = $("#addcontact")[0];
        var form_data = new FormData(form_data); 
         form_data.append('method','addcontact');      
        $.ajax({
            url: "admin/img_ajaxcall.php",
            type: "post",
            dataType: "json",
            contentType: false,
            processData: false,
            data: form_data, 
            success: function (response) {
                if(response['result'] == "fail"){
                    response["msg"].name !== undefined ? $(".name").html(response["msg"].name) : $(".name").html("");
                    response["msg"].email !== undefined ? $(".email").html(response["msg"].email) : $(".email").html(""); 
                    response["msg"].phone !== undefined ? $(".phone").html(response["msg"].phone) : $(".phone").html("");  
                    response["msg"].text !== undefined ? $(".text").html(response["msg"].text) : $(".text").html("");  
                    response["msg"].message !== undefined ? $(".message").html(response["msg"].message) : $(".message").html("");  
                }else if(response['result'] == "success"){
                    $(".name").html("");
                    $(".phone").html("");
                    $(".text").html("");
                    $(".message").html("");
                    $("#addcontact")[0].reset();
                }
            }
        })
});
   $(document).on("click",".navbar-toggler",function(){
            $ariaexpanded =  $(this).attr("aria-expanded");
          $(".navbar-brand").css("display", "block");
        $(".shopnowbtn").css("display", "block");
        if($ariaexpanded == "false"){
            $(".navbar-brand").css("display", "none");
            $(".shopnowbtn").css("display", "none");
        }
    });

$(document).ready(function() {
   $(document).on("click",".down",function(){
     $('html, body').animate({
         scrollTop: $(".up").offset().top
     }, 1500);
 });
 
      
if(window.location.pathname.split("/")[1] == "index.php"){
    $(".down").css("display","block");
}
else{
  const data = setInterval(myTimer, 200);
 
}
  
function myTimer() {
if($('.bethany-header-navbar').find(".shopnowbtn a").hasClass("down") == "true"){
    $(".down").css("display","none");
    clearInterval(data);
}
}

});
$(".our_srvs").change(function () {
    // $(".alredy_ser_img").css("display: none");
    if (typeof (FileReader) != "undefined") {
        var dvPreview = $("#dvPreview_sevices");
        var i = 1;
        var clstag = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp|.webp)$/;
        $($(this)[0].files).each(function () {
            if (i > 10) {
                $("#dvPreview").addClass("scrollbar");
            }
            var file = $(this);
            if (clstag.test(file[0].name.toLowerCase())) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var div = $("<div class='clsdiv ovr_services'>");
                    var img = $("<img class='clsimg' />");
                    img.attr("src", e.target.result);
                    var btn = $("<button class='btn  btn-raised btn-danger waves-effect remove'>X</button>");
                    div.attr("</div>");
                    div.append(img);
                    div.append(btn);
                    dvPreview.append(div);
                    $(".remove").click(function () {
                        var clsthis = this;
                        $(clsthis).closest(".clsdiv").remove();
                    });
                }
                reader.readAsDataURL(file[0]);
            } else {
                setTimeout(function () {
                    $('.clsfile').css("display", "none");
                }, 2000);
                return false;
            }
            i++;
        });
    } else {
        alert("This browser does not support HTML5 FileReader.");
    }
});