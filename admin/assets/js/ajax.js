
//multi delete and table listing
$(document).ready(function () {
    $('table[data-listing="true"]').each(function () {
        load_data(1);
    });
    $(".clsmuldel").click(function () {
        var post_atr = [];
        var clsthis = this;
        $(".clstbody input[type=checkbox]").each(function () {
            if ($(this).is(":checked")) {
                var id = this.id;
                post_atr.push(id);
            }
        });
        $.ajax({
            url: "ajaxcall.php",
            type: 'POST',
            data: {"id": post_atr, "method": "multidelete"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                console.log(json_data);
                if (json_data["result"] == "fail") {
                    $('.clsdel').html(json_data["msg"]);
                } else {
                    $.each(json_data["id"], function (i, l) {
                        $("#tr_" + l).remove();
                    });
                    $('.clsdel').html(json_data["msg"]);
                    setTimeout(function () {
                        $('.clsdel').css("display", "none");
                        load_data(1);
                    }, 3000);
                }
            }
        });
    });
});
//set data
function load_data(page, value) {
    $.ajax({
        url: "ajaxcall.php",
        type: 'POST',
        data: {"page": page, "value": value, "method": "set_data"},
        success: function (data) {
            var json_data = $.parseJSON(data);
            $('.clstbody').html(json_data["data"]);
            $('.pagination_html').html(json_data["pagination"]);
            $(".page-item").removeClass("active");
            $(".clsClass" + page).addClass("active");
        }
    });
}

function clsset_data(id) {
    $.ajax({
        url: "ajaxcall.php",
        type: 'POST',
        data: {"method": "clsset_data", "id": id},
        success: function (data) {
            var json_data = $.parseJSON(data);
            $('.clssettbody').html(json_data["data"]);
            $('.clstbody').html(json_data["clsdata"]);
        }
    });
}

//pagination
$(document).on('click', '.pagination_link', function () {
    var page = $(this).attr("id");
    load_data(page);
});
//searching on dashboard
$(document).on('keyup', '.clssearch', function () {
    var value = $(this).val().toLowerCase();
    if (this.value.length >= 3 || this.value.length == 0) {
        load_data(1, value);
    }
});
//delete data
$(document).on("click", ".clsdelete", function () {
    var clsthis = this;
    var clsid = $(this).data("id");
    $.ajax({
        url: "ajaxcall.php",
        type: 'POST',
        data: {"id": clsid, "method": "delete_data"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            console.log(json_data);
            if (json_data["result"] == "fail") {
                $('.clsdel').html(json_data["msg"]);
            } else {
                $(clsthis).closest("tr").remove();
                $('.clsdel').html(json_data["msg"]);
                setTimeout(function () {
                    $('.clsdel').css("display", "none");
                }, 3000);
            }
        }
    });
});
//get data
function clsupdate(id) {
    $.ajax({
        url: "ajaxcall.php",
        type: 'POST',
        data: {"id": id, "method": "get_data"},
        success: function (result) {
            console.log(result);
            var json_data = $.parseJSON(result);
            var items = json_data["books"].split(',');
            $.each(items, function (key, value) {
                if (value == "Maths") {
                    $('#book3').prop('checked', true);
                }
                if (value == "Biology") {
                    $('#book1').prop('checked', true);
                }
                if (value == "Eco") {
                    $('#book2').prop('checked', true);
                }
            });
            $('#clshidden').val(json_data["id"]);
            $('#clshidden1').val(json_data["profile"]);
            $('#uname').val(json_data["username"]);
            $('#email').val(json_data["email"]);
            $('.clsskill').val(json_data["skill"]);
            $('#pnumber').val(json_data["number"]);
            $('.clsabout').val(json_data["about"]);
            $('.clsupdate').val(json_data["msg"]);
        }});
}


//insert data in register_data
$(document).on("click", ".submitbtn", function (event) {
    event.preventDefault();
    var form_data = new FormData($('form')[0]);
    $.ajax({
        url: "ajaxcall.php",
        type: 'POST',
        processData: false,
        contentType: false,
        data: form_data,
        success: function (result) {
            var json_data = $.parseJSON(result);
            $('.userName1').html("");
            $('.passWord').html("");
            $('.eMail').html("");
            $('.rdo2').html("");
            $('.pNumber').html("");
            $('.chb1').html("");
            $('.clsmsg1').html("");
            $('.uploadimg').html("");
            if (json_data["result"] == "fail") {
                $('.userName1').html(json_data["data"]["username"]);
                $('.passWord').html(json_data["data"]["password"]);
                $('.eMail').html(json_data["data"]["email"]);
                $('.rdo2').html(json_data["data"]["gender"]);
                $('.pNumber').html(json_data["data"]["pnumber"]);
                $('.chb1').html(json_data["data"]["books"]);
                $('.clsmsg1').html(json_data["data"]["message"]);
                $('.uploadimg1').html(json_data["data"]["image"]);
            } else {
                $('.clsinsert').html(json_data["msg"]);
                setTimeout(function () {
                    window.location.href = "Dashboard.php";
                }, 3000);
            }
        }
    });
    return false;
});
//login process
$(document).on("click", "#signin", function (event) {
    event.preventDefault();
    var email = $('#email').val();
    var password = $('#password').val();
    if (email !== '' && password !== '') {
        $.ajax({
            url: "ajaxcall.php",
            type: 'post',
            data: {"email": email, "password": password, "method": "signin"},
            success: function (result) {
                var json_data = $.parseJSON(result);
                if (json_data["msg"] == "success") {
                    $('.userName1').html(json_data["data"]);
                    if (json_data['role'] == 0) {
                        window.location.href = "http://localhost/vasu/nexa/Dashboard.php";
                    } else {
                        window.location.href = "http://localhost/vasu/nexa/index1.php";
                    }
                } else {
                    $('.userName1').html(json_data["data"]);
                    return false;
                }
            }
        });
    } else {
        console.log(email);
        alert("Please Enter Email And password !");
    }
});
//Load data
function getExistingTime(value) {
    $.ajax({
        url: "ajaxcall.php",
        type: 'post',
        data: {"method": "getTime", "value": value},
        success: function (result) {
            var json_data = $.parseJSON(result);
            $('.clstbody').html(json_data["data"]);
            $('.navbar-header').html(json_data["clsdata"]);
            $('#time').html(json_data['time']);
            $(".clstmdiv").addClass("active");
            if (json_data["msg"] == "success") {
                $('.error-msg').html(json_data["data"]);
            } else {
                $('.error-msg').html(json_data["data"]);
                return false;
            }
        }
    });
}
//insert/upadate timer
function clstimer() {
    var time = $("#time").html();
    $.ajax({
        url: "ajaxcall.php",
        type: 'post',
        data: {"date_time": time, "method": "clstimer"},
        success: function (result) {
            var json_data = $.parseJSON(result);
            $('#time').html(json_data['data']);
            if (json_data["msg"] == "success") {
                $('.error-msg').html(json_data["data"]);
            } else {
                $('.error-msg').html(json_data["data"]);
                return false;
            }
        }
    });
}

//timer
var clssecondtime = 0;
var stopped;
var running = 0;
function timer(current_time) {
    if (current_time != undefined) {
        var items = current_time.split(':');
        clssecondtime = (+items[0]) * 60 * 60 + (+items[1]) * 60 + (+items[2]);
    }
    if (running) {
        $("h1").html(format(clssecondtime));
        clssecondtime++;
        stopped = setTimeout(timer, 1000);
    }
}
var interval = null;
function start() {
    running = 1;
    timer($("#time").html());
    clstimer();
    interval = setInterval(clstimer, 1000);
    $("#stop").removeClass("hide");
    $("#start").addClass("hide");
    $("#lap").removeClass("hide");
    $("#reset").addClass("hide");
}

function stop() {
    running = 0;
    clearTimeout(clssecondtime);
    $("#start").removeClass("hide");
    $("#stop").addClass("hide");
    $("#reset").removeClass("hide");
    $("#lap").addClass("hide");
}

function lap() {
    $('#display').html(format(clssecondtime));
}

function reset() {
    clssecondtime = 0;
    running = 0;
    clearTimeout(0);
    $('h1').html(format(0));
    $('#display').html('00:00:00');
}

function format(t) {
    var sec = place(parseInt(t) % 60);
    var old_min = Math.floor(t / 60);
    var min = place(Math.floor(t / 60 % 60));
    var hor = place(Math.floor(old_min / 60));
    return hor + ':' + min + ':' + sec;
}

function place(n) {
    return n < 10 ? '0' + n : n;
}

$(document).on('keyup', '.clstmsearch', function () {
    var value = this.value.replace(/^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/, '');
    if (this.value.length >= 10 || this.value.length == 0) {
        getExistingTime(value);
    }
});



