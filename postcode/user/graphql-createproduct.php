<?php

$client = new Graphql("https://grusshoppen-dk.myshopify.com/", "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");
$query = <<<QUERY
  mutation {
    productCreate(input: {title: "Sweet new product", productType: "Snowboard", vendor: "JadedPixel"}) {
      product {
        id
      }
    }
  }
QUERY;

$response = $client->query(["query" => $query]);
?>