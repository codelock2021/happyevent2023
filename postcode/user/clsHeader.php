<html>
    <head>
      
    </head>
    <body>
    <div p-color-scheme="light">
  <div class="Polaris-Card">
    <div>
      <div class="Polaris-Tabs__Wrapper">
        <div class="Polaris-Tabs Polaris-Tabs__TabMeasurer">
          <li class="Polaris-Tabs__TabContainer" role="presentation"><button id="all-customers-1Measurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="true"><span class="Polaris-Tabs__Title">Dashboard</span></button></li>
          <li class="Polaris-Tabs__TabContainer" role="presentation"><button id="accepts-marketing-1Measurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false"><span class="Polaris-Tabs__Title clazonerate">Zones & Rates</span></button></li>
          <li class="Polaris-Tabs__TabContainer" role="presentation"><button id="repeat-customers-1Measurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false"><span class="Polaris-Tabs__Title clssettings">Settings</span></button></li>
          <li class="Polaris-Tabs__TabContainer" role="presentation"><button id="prospects-1Measurer" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false"><span class="Polaris-Tabs__Title clssupports">Supports</span></button></li><button type="button" class="Polaris-Tabs__DisclosureActivator" aria-label="More tabs"><span class="Polaris-Tabs__Title"><span class="Polaris-Icon Polaris-Icon--colorSubdued Polaris-Icon--applyColor"><span class="Polaris-VisuallyHidden"></span><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                  <path d="M6 10a2 2 0 1 1-4.001-.001 2 2 0 0 1 4.001.001zm6 0a2 2 0 1 1-4.001-.001 2 2 0 0 1 4.001.001zm6 0a2 2 0 1 1-4.001-.001 2 2 0 0 1 4.001.001z"></path>
                </svg></span></span></button>
        </div>
        <ul role="tablist" class="Polaris-Tabs">
          <li class="Polaris-Tabs__TabContainer" role="presentation"><a class="Polaris-Link" href="#tab1" data-polaris-unstyled="true"><button id="all-customers-1" role="tab" type="button" tabindex="0" class="Polaris-Tabs__Tab " aria-selected="true" aria-controls="all-customers-content-1" aria-label="All customers"><span class="Polaris-Tabs__Title">Dashboard</span></button></a></li>
          <li class="Polaris-Tabs__TabContainer" role="presentation"><a class="Polaris-Link" href="zonetable.php?store=<?php echo $_SESSION['store']; ?>" data-polaris-unstyled="true"><button id="accepts-marketing-1" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--selected" aria-selected="false" aria-controls="accepts-marketing-content-1"><span class="Polaris-Tabs__Title clazonerate">Zones & Rates</span></button></a></li>
          <li class="Polaris-Tabs__TabContainer" role="presentation" ><a class="Polaris-Link" href="zonesettings.php?store=<?php echo $_SESSION['store']; ?>" data-polaris-unstyled="true"><button id="repeat-customers-1" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="repeat-customers-content-1"><span class="Polaris-Tabs__Title clssettings">Settings</span></button></a></li>
          <li class="Polaris-Tabs__TabContainer" role="presentation"><a class="Polaris-Link" href="zonesupport.php?store=<?php echo $_SESSION['store']; ?>" data-polaris-unstyled="true"><button id="prospects-1" role="tab" type="button" tabindex="-1" class="Polaris-Tabs__Tab" aria-selected="false" aria-controls="prospects-content-1"><span class="Polaris-Tabs__Title clssupports">Supports</span></button></a></li>
          <li class="Polaris-Tabs__DisclosureTab" role="presentation">
            <div><button type="button" class="Polaris-Tabs__DisclosureActivator" aria-label="More tabs" tabindex="0" aria-controls="Polarispopover2" aria-owns="Polarispopover2" aria-expanded="false"><span class="Polaris-Tabs__Title"><span class="Polaris-Icon Polaris-Icon--colorSubdued Polaris-Icon--applyColor"><span class="Polaris-VisuallyHidden"></span><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                      <path d="M6 10a2 2 0 1 1-4.001-.001 2 2 0 0 1 4.001.001zm6 0a2 2 0 1 1-4.001-.001 2 2 0 0 1 4.001.001zm6 0a2 2 0 1 1-4.001-.001 2 2 0 0 1 4.001.001z"></path>
                    </svg></span></span></button></div>
          </li>
        </ul>
      </div>
 
      </div>
      <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden" id="accepts-marketing-content-1" role="tabpanel" aria-labelledby="accepts-marketing-1" tabindex="-1"></div>
      <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden" id="repeat-customers-content-1" role="tabpanel" aria-labelledby="repeat-customers-1" tabindex="-1"></div>
      <div class="Polaris-Tabs__Panel Polaris-Tabs__Panel--hidden" id="prospects-content-1" role="tabpanel" aria-labelledby="prospects-1" tabindex="-1"></div>
    </div>
  </div>
  <div id="PolarisPortalsContainer">
    <div data-portal-id="popover-Polarisportal1"></div>
  </div>
</div>




   </body>
</html>

