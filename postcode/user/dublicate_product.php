<?php
function shopify_call($token, $shop, $api_endpoint, $query = array(), $method = 'GET', $request_headers = array()) {
	// Build URL
	$url = "https://" . $shop . $api_endpoint;
        if (!empty($query) && !is_null($query) && in_array($method, array('GET', 'DELETE'))) {
            $url = $url . "?" . http_build_query($query);
        } else {
            $url = $url;
        }
	// Configure cURL
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HEADER, TRUE);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($curl, CURLOPT_MAXREDIRS, 3);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	// curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 3);
	// curl_setopt($curl, CURLOPT_SSLVERSION, 3);
	curl_setopt($curl, CURLOPT_USERAGENT, 'My New Shopify App v.1');
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
	// Setup headers
	//$request_headers[] = "";
	if (!is_null($token)){$request_headers[] = "X-Shopify-Access-Token:" . $token;}
	curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
	if ($method != 'GET' && in_array($method, array('POST', 'PUT'))) {
            if (is_array($query)){
                $query = json_encode($query);
            }
            curl_setopt ($curl, CURLOPT_POSTFIELDS, $query);
	}    
	// Send request to Shopify and capture any errors
	$response = curl_exec($curl);

    /*added by developer */
    /* Then, after your curl_exec call: https://stackoverflow.com/questions/9183178/can-php-curl-retrieve-response-headers-and-body-in-a-single-request */
    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);
    $body = substr($response, $header_size);
    /*added by developer end */
        
	$error_number = curl_errno($curl);
	$error_message = curl_error($curl);
	// Close cURL to be nice
	curl_close($curl);
	// Return an error is cURL has a problem
	if ($error_number) {
            return $error_message;
	} else {
            /* added by developer */
            return array('headers' => $header, 'response' => $body);
            /* added by developer end */
	}
    
}

 function get_api_list($api_main_url_arr, $url_param_arr = array(), $method = 'GET', $is_object = 1, $extra_header=array()) {
        /* Get shop info object */
        $shop = "dashboardmanage.myshopify.com";
      $api_key = "dfc528f13f488d90ab3ee5dd3d03915b";
$password = "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";
$store ="dashboardmanage.myshopify.com";
        /* Make api url */
        
        $api_main_url_arr = array_merge(array('/admin/api/2021-07'), $api_main_url_arr);
     
        $api_main_url = implode('/', $api_main_url_arr) . '.json';
        /* Make api url end */
       
        /* call api for fetch api data */
        $api_data_list = shopify_call($password, $shop, $api_main_url, $url_param_arr, $method, $extra_header);

        /* return api response */
        if ($is_object) {
            return json_decode($api_data_list['response']);
        } else {
            return json_decode($api_data_list['response'], TRUE);
        }
    }
   
    $_POST['title'] = "2CV by the fjord - app";
    // $_POST['seller_name'] = "urvashi";
    // $_POST['variant_str_sep'] =
    $_POST['buyer_moq'] = 3;
    $_POST['buyer_price'] = 100;
    $_POST['price'] = 1000;
    $_POST['images']="PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxOCIgaGVpZ2h0PSIxNyIgdmlld0JveD0iMCAwIDE4IDE3IiBmaWxsPSJub25lIiBzdHlsZT0iYmFja2dyb3VuZDogIzAwMDsiPg0KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzI5N183NDMpIj4NCjxwYXRoIGQ9Ik01LjYxNjIyIDE2LjUyOThDNS4yMDk3NyAxNi41MjQzIDQuODE0MiAxNi4zOTg3IDQuNDc4OTMgMTYuMTY4OUM0LjE0MzY1IDE1LjkzOSAzLjg4MzY0IDE1LjYxNTEgMy43MzE5MSAxNS4yMzhDMy41ODAxOCAxNC44NjA5IDMuNTQzNDUgMTQuNDQ3MyAzLjYyNjEyIDE0LjA0OTNDMy43MDg4IDEzLjY1MTQgMy45MDc0NiAxMy4yODY3IDQuMTk2ODMgMTMuMDAxMkM0LjQ4NjIgMTIuNzE1NyA0Ljg1MzIyIDEyLjUyMjIgNS4yNTIzIDEyLjQ0NDlDNS42NTEzOCAxMi4zNjc2IDYuMDY0NTUgMTIuNDEgNi40Mzk1OCAxMi41NjY4QzYuODE0NjEgMTIuNzIzNiA3LjEzNDg4IDEyLjk4NzkgNy4zNjAxNyAxMy4zMjYyQzcuNTg1NDYgMTMuNjY0NiA3LjcwNTU3IDE0LjA2MTkgNy43MDU2MSAxNC40Njg0QzcuNzA3NTQgMTQuNzQyIDcuNjU0ODUgMTUuMDEzMyA3LjU1MDE4IDE1LjI2NjFDNy40NDU1MSAxNS41MTg5IDcuMjkxIDE1Ljc0ODIgNy4wOTYyMiAxNS45NDAzQzYuOTAxNDQgMTYuMTMyNSA2LjY3MDE2IDE2LjI4MzYgNi40MTU5NiAxNi4zODQ5QzYuMTYxNzYgMTYuNDg2MSA1Ljg4OTc5IDE2LjUzNTQgNS42MTYyMiAxNi41Mjk4VjE2LjUyOThaIiBmaWxsPSIjZmZmZmYiLz4NCjxwYXRoIGQ9Ik0xMi4yNDg3IDE2LjUyOThDMTEuODQxIDE2LjUyOTggMTEuNDQyNSAxNi40MDg5IDExLjEwMzUgMTYuMTgyNEMxMC43NjQ1IDE1Ljk1NTkgMTAuNTAwNSAxNS42MzM5IDEwLjM0NDUgMTUuMjU3M0MxMC4xODg1IDE0Ljg4MDYgMTAuMTQ3NSAxNC40NjYyIDEwLjIyNzEgMTQuMDY2M0MxMC4zMDY2IDEzLjY2NjUgMTAuNTAzIDEzLjI5OTEgMTAuNzkxMyAxMy4wMTA4QzExLjA3OTYgMTIuNzIyNiAxMS40NDY5IDEyLjUyNjMgMTEuODQ2OCAxMi40NDY3QzEyLjI0NjYgMTIuMzY3MiAxMi42NjExIDEyLjQwOCAxMy4wMzc4IDEyLjU2NEMxMy40MTQ1IDEyLjcyIDEzLjczNjMgMTIuOTg0MiAxMy45NjI4IDEzLjMyMzJDMTQuMTg5MyAxMy42NjIyIDE0LjMxMDMgMTQuMDYwNyAxNC4zMTAzIDE0LjQ2ODRDMTQuMzAzMSAxNS4wMTI5IDE0LjA4MzYgMTUuNTMzIDEzLjY5ODUgMTUuOTE4QzEzLjMxMzUgMTYuMzAzIDEyLjc5MzEgMTYuNTIyNSAxMi4yNDg3IDE2LjUyOThWMTYuNTI5OFoiIGZpbGw9IiNmZmZmZiIvPg0KPHBhdGggZD0iTTUuNjE2ODcgNC42ODA1QzUuMjA2NDEgNC42Nzc3MiA0LjgwNTk1IDQuNTUzMjcgNC40NjYyMiA0LjMyMjkyQzQuMTI2NDggNC4wOTI1NyAzLjg2Mjc2IDMuNzY2NjggMy43MDgyNSAzLjM4NjQxQzMuNTUzNzQgMy4wMDYxMyAzLjUxNTU1IDIuNTg4NTggMy41OTgzNSAyLjE4NjU1QzMuNjgxMTUgMS43ODQ1MiAzLjg4MTMxIDEuNDE2MSA0LjE3MzUxIDEuMTI3ODJDNC40NjU3IDAuODM5NTQ1IDQuODM2ODIgMC42NDQ0MDQgNS4yMzk5NCAwLjU2NzA0NkM1LjY0MzA1IDAuNDg5Njg3IDYuMDYwMDIgMC41MzM1NyA2LjQzODE3IDAuNjkzMjAzQzYuODE2MzIgMC44NTI4MzYgNy4xMzg1IDEuMTIxMDIgNy4zNjQyNCAxLjQ2Mzg0QzcuNTg5OTcgMS44MDY2NiA3LjcwOTAxIDIuMjA4NzQgNy43MDYyNSAyLjYxOTJDNy43MDYyNyAyLjg5MjI3IDcuNjUyMDUgMy4xNjI2MyA3LjU0NjcxIDMuNDE0NTdDNy40NDEzNyAzLjY2NjUxIDcuMjg3MTUgMy44OTQ5OSA3LjA5Mjc1IDQuMDg2NzhDNi44OTgzNiA0LjI3ODU2IDYuNjY3ODkgNC40Mjk4MiA2LjQxNDU1IDQuNTMxNzVDNi4xNjEyMSA0LjYzMzY4IDUuODg5OTIgNC42ODQyMiA1LjYxNjg3IDQuNjgwNVoiIGZpbGw9IiNmZmZmZiIvPg0KPHBhdGggZD0iTTEyLjI1MDMgNC42ODA1NEMxMS44Mzg3IDQuNjgzMzEgMTEuNDM1NiA0LjU2MzYyIDExLjA5MjEgNC4zMzY2NUMxMC43NDg3IDQuMTA5NjcgMTAuNDgwNyAzLjc4NTY3IDEwLjMyMTkgMy40MDU4N0MxMC4xNjMgMy4wMjYwOCAxMC4xMjA3IDIuNjA3NjcgMTAuMjAwMyAyLjIwMzc5QzEwLjI3OTkgMS43OTk5IDEwLjQ3NzkgMS40Mjg3OSAxMC43NjkgMS4xMzc3QzExLjA2MDEgMC44NDY2MTEgMTEuNDMxMiAwLjY0ODY4MSAxMS44MzUxIDAuNTY5MDVDMTIuMjM4OSAwLjQ4OTQxOSAxMi42NTczIDAuNTMxNjg0IDEzLjAzNzEgMC42OTA1QzEzLjQxNjggMC44NDkzMTUgMTMuNzQwOSAxLjExNzUzIDEzLjk2NzkgMS40NjA5N0MxNC4xOTQ5IDEuODA0NCAxNC4zMTQ3IDIuMjA3NTggMTQuMzEyIDIuNjE5MjNDMTQuMzAxMiAzLjE2MjU2IDE0LjA4MDQgMy42ODA2MyAxMy42OTYxIDQuMDY0OUMxMy4zMTE4IDQuNDQ5MTcgMTIuNzkzNyA0LjY2OTc4IDEyLjI1MDMgNC42ODA1NFoiIGZpbGw9IiNmZmZmZiIvPg0KPHBhdGggZD0iTTIuMzQ5NTEgMTAuNTU2MUMyLjA3OSAxMC41NjU1IDEuODA5MjcgMTAuNTIwNyAxLjU1NjI4IDEwLjQyNDVDMS4zMDMyOSAxMC4zMjgyIDEuMDcxOTQgMTAuMTgyNCAwLjg3NjAyNCA5Ljk5NTY2QzAuNjgwMTEgOS44MDg4OSAwLjUyMzQxOCA5LjU4NDkxIDAuNDE1MjE2IDkuMzM2OEMwLjMwNzAxNSA5LjA4ODY5IDAuMjQ5NjY1IDguODIxNSAwLjI0NjA5NCA4LjU1MDg1QzAuMjQ5NzUyIDguMjc4MyAwLjMwNjg0IDguMDA5MTIgMC40MTQ1MzIgNy43NTg3M0MwLjUyMjIyMyA3LjUwODMzIDAuNjc4MzAyIDcuMjgxNiAwLjg3MzYyNyA3LjA5MTQ4QzEuMDY4OTUgNi45MDEzNiAxLjI5OTk4IDYuNzUxNTkgMS41NTMyIDYuNjUwN0MxLjgwNjQxIDYuNTQ5ODEgMi4wNzY5NiA2LjQ5OTc4IDIuMzQ5NTEgNi41MDM0OUMyLjYxNTg4IDYuNTA1MzEgMi44NzkwNCA2LjU1OTgzIDMuMTI0MjYgNi42NjM4OEMzLjM2OTQ3IDYuNzY3OTMgMy41OTE3NiA2LjkxOTQ5IDMuNzc4MTUgNy4xMDk4QzMuOTY0NTQgNy4zMDAxIDQuMTExNiA3LjUyNTQ0IDQuMjEwNTQgNy43NzI3NkM0LjMwOTQ4IDguMDIwMDggNC4zNTgzOCA4LjI4NDUgNC4zNTQ2NyA4LjU1MDg1QzQuMzU4NDUgOC44MTUyMiA0LjMwOTMgOS4wNzc2OSA0LjIwOTg2IDkuMzIyNjhDNC4xMTA0MiA5LjU2NzY4IDMuOTYyNzIgOS43OTAyMSAzLjc3NTc2IDkuOTc3MTdDMy41ODg3OSAxMC4xNjQxIDMuMzY2MTcgMTAuMzExOCAzLjEyMTE4IDEwLjQxMTJDMi44NzYxOCAxMC41MTA2IDIuNjEzODkgMTAuNTU5OSAyLjM0OTUxIDEwLjU1NjFWMTAuNTU2MVoiIGZpbGw9IiNmZmZmZiIvPg0KPHBhdGggZD0iTTguODk4MjIgMTAuNTU2MUM4LjYzMDIxIDEwLjU2MzYgOC4zNjMzNSAxMC41MTcyIDguMTEzNTUgMTAuNDE5OEM3Ljg2Mzc1IDEwLjMyMjQgNy42MzYwOSAxMC4xNzU5IDcuNDQzOTEgOS45ODg5NEM3LjI1MTcyIDkuODAxOTkgNy4wOTkgOS41NzgzOSA2Ljk5NDc0IDkuMzMxMzdDNi44OTA0OCA5LjA4NDM1IDYuODM2ODEgOC44MTg5MiA2LjgzNjkxIDguNTUwOEM2Ljg3OTU0IDguMDM1MjcgNy4xMTQzNiA3LjU1NDYzIDcuNDk0OTIgNy4yMDQyNEM3Ljg3NTQ4IDYuODUzODUgOC4zNzQwOCA2LjY1OTMgOC44OTEzOCA2LjY1OTNDOS40MDg2NyA2LjY1OTMgOS45MDY5MyA2Ljg1Mzg1IDEwLjI4NzUgNy4yMDQyNEMxMC42NjggNy41NTQ2MyAxMC45MDI5IDguMDM1MjcgMTAuOTQ1NSA4LjU1MDhDMTAuOTQ1NiA4LjgxNzcxIDEwLjg5MjQgOS4wODE5NiAxMC43ODkgOS4zMjgwM0MxMC42ODU2IDkuNTc0MSAxMC41MzQgOS43OTcwNCAxMC4zNDMzIDkuOTgzODFDMTAuMTUyNiAxMC4xNzA2IDkuOTI2NjEgMTAuMzE3NCA5LjY3ODQ1IDEwLjQxNTdDOS40MzAyOCAxMC41MTQgOS4xNjUwOCAxMC41NjE3IDguODk4MjIgMTAuNTU2MVYxMC41NTYxWiIgZmlsbD0iI2ZmZmZmIi8+DQo8cGF0aCBkPSJNMTUuNTU4NyAxMC41NTZDMTUuMjkwMSAxMC41NjM1IDE1LjAyMjYgMTAuNTE3MyAxNC43NzIgMTAuNDJDMTQuNTIxNCAxMC4zMjI4IDE0LjI5MjkgMTAuMTc2NCAxNC4wOTk2IDkuOTg5NjdDMTMuOTA2MyA5LjgwMjkxIDEzLjc1MjIgOS41Nzk0NCAxMy42NDY0IDkuMzMyMzZDMTMuNTQwNiA5LjA4NTI3IDEzLjQ4NTIgOC44MTk1NCAxMy40ODM0IDguNTUwNzZDMTMuNDg1MiA4LjI4MDA2IDEzLjU0MDQgOC4wMTI0IDEzLjY0NTcgNy43NjMwMUMxMy43NTEgNy41MTM2MiAxMy45MDQyIDcuMjg3MzMgMTQuMDk2OSA3LjA5NzIyQzE0LjI4OTYgNi45MDcxIDE0LjUxODEgNi43NTY4NyAxNC43Njg5IDYuNjU0OThDMTUuMDE5NyA2LjU1MzA5IDE1LjI4OCA2LjUwMTU2IDE1LjU1ODcgNi41MDM0QzE1LjgyNjkgNi41MDM0IDE2LjA5MjQgNi41NTY0NiAxNi4zNCA2LjY1OTUyQzE2LjU4NzYgNi43NjI1OCAxNi44MTI0IDYuOTEzNTkgMTcuMDAxNCA3LjEwMzg5QzE3LjE5MDQgNy4yOTQxOSAxNy4zNCA3LjUyIDE3LjQ0MTMgNy43NjgzMUMxNy41NDI3IDguMDE2NjMgMTcuNTkzOCA4LjI4MjU3IDE3LjU5MiA4LjU1MDc2QzE3LjU5MzkgOC44MTcwMSAxNy41NDI1IDkuMDgwOTYgMTcuNDQwNyA5LjMyNjk2QzE3LjMzODggOS41NzI5NyAxNy4xODg2IDkuNzk2MDQgMTYuOTk5IDkuOTgzQzE2LjgwOTQgMTAuMTcgMTYuNTg0MyAxMC4zMTcgMTYuMzM2OSAxMC40MTU0QzE2LjA4OTUgMTAuNTEzOCAxNS44MjQ5IDEwLjU2MTYgMTUuNTU4NyAxMC41NTZWMTAuNTU2WiIgZmlsbD0iI2ZmZmZmIi8+DQo8L2c+DQo8ZGVmcz4NCjxjbGlwUGF0aCBpZD0iY2xpcDBfMjk3Xzc0MyI+DQo8cmVjdCB3aWR0aD0iMTcuMzUzMiIgaGVpZ2h0PSIxNiIgZmlsbD0id2hpdGUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMjM1MzUyIDAuNTI5Nzg1KSIvPg0KPC9jbGlwUGF0aD4NCjwvZGVmcz4NCjwvc3ZnPg==";
    $_POST['Checkbox'] = "1";
    $_POST['add_value'] = "AB_PHOTOGRAPHY,AB_Norway,Nature,nyheter";
    $_POST['var_combi_str_arr'] = "50x70 cm";
    $_POST['collections'] = array("283628011689","269541605545");
 function add_products() {
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        $vendor = '';
        $tags = '';
        $image_value = array();
        $collection_ids = array();
        if (isset($_POST['seller_name'])) {
            $vendor = prepare_db_inputs($_POST['seller_name']);
        }
        if (isset($_POST['add_value']) && $_POST['add_value'] != '') {
            $tags =trim($_POST['add_value']);
        }
        $err_msg = array();
        $seprator = '';
        if (isset($_POST['variant_str_sep'])) {
            $seprator = $_POST['variant_str_sep'];
        }
        $response = array('result' => 'fail', 'msg' => SOMETHING_WENT_WRONG_MSG);
        // echo "<pre>";
        // print_r($response);
        /* it keep the only index(key) value where moq vlaue is exist */
        $check_box = array();
        $img_src = '';
            /* alse check using var_combi_str_arr */
            if (isset($_POST['variant_moq']) && count($_POST['variant_moq']) == 1 && isset($_POST['variant_price']) && count($_POST['variant_price']) == 1) {
                
            } else {
                if (isset($_POST['add_value']) || isset($_POST['var_combi_str_arr'])) {
                    if (isset($_POST['Checkbox'])) {
                        $var_comi_arr = array_intersect($_POST['Checkbox'], $_POST['var_combi_str_arr']);
                    }
                    if (!empty($var_comi_arr)) {
                        $variant_key = array('option1', 'option2', 'option3');
                        $variants = array();
                        foreach ($var_comi_arr as $key => $value) {
                            $options[] = explode(' ', $value);
                        }
                        
//                        $options = $this->combinations($options);
                        if (!is_array($options[0])) {
                            foreach ($options as $key => $value) {
                                $variants[$key][$variant_key[0]] = $value;
                                $variants[$key]['price'] = $varint_price_arr[0];
                                
                            }
                        } else {
                            foreach ($options as $key => $value) {
                                foreach ($value as $k => $v) {
                                    $variants[$key][$variant_key[$k]] = $v;
                                }
                                $variants[$key]['price'] = $varint_price_arr[0];
                                $variants[$key]['sku'] = $_POST['sku'][$key];
                                $variants[$key]['inventory_quantity'] =  $_POST['inventory'][$key];
                                $variants[$key]['inventory_management'] = "shopify";
                            }
                        }
                    }
                }

                $options = array();
                if (isset($_POST['add_attribute'])) {
                    foreach ($_POST['add_attribute'] as $key => $attribute) {
                        $attr_value = explode(',', $_POST['add_value'][$key]);
                        $options[] = array('name' => $attribute, 'values' => $attr_value);
                    }
                }
            }
          
            if(isset($_POST['images']) && !empty($_POST['images']))  {
                $image_value = $_POST['images'];
                foreach ($img_arr as $value) {
                    $imgs[] = explode(',', $value);
                }
                foreach ($imgs as $value) {
                    $images[] = $value[1];
                }
                if (!empty($images)) {
                    foreach ($images as $value) {
                        $image_value[]['attachment'] = trim($value);
                    }
                }
                $product_array = array(
                    'product' => array(
                        'title' => trim($_POST['title']),
                        'body_html' => trim($_POST['description']),
                        'vendor' => $vendor,
                        'tags' => $tags,
                        'images' => $image_value,
                    )
                );
            } else {
                $product_array = array(
                    'product' => array(
                        'title' => trim($_POST['title']),
                        'body_html' => trim($_POST['description']),
                        'vendor' => $vendor,
                        'tags' => $tags,
                    )
                );
            }
            if (isset($_POST['buyer-help-place']) && $_POST['buyer-help-place'] != '') {
                $help_option = $_POST['buyer-help-place'];
            }

            /* check wether variant created or not */
            if (isset($variants) && isset($options)) {
                $product_array['product']['variants'] = $variants;
                $product_array['product']['options'] = $options;
            }
            if ($help_option == 0) {
                $product_array['product']['template_suffix'] = 'noprice';
            }
            $api = array('api_name' => 'products');
            $product = get_api_list($api, $product_array, 'POST', 1, array("Content-Type: application/json"));
          echo "<pre>";
          print_r($product);
           $api = array('api_name' => 'products/'.$product->product->id.'/images');
           $cdn_img = "https://cdn.shopify.com/s/files/1/0544/9754/2313/products/2CV_by_the_fjord_50x70_101693_0_1426x.jpg?v=1650959276%201426w";
            $product_image_array = array('image' => array('product_id' => $product->product->id, 'src' =>$cdn_img));
            
           $product_img = get_api_list($api, $product_image_array, 'POST', 1, array("Content-Type: application/json"));
           echo "<pre>";
           print_r($product_img);
           
            if (isset($product->product)) {
                $product = $product->product;
                if (isset($product->image)) {
                    $img_src = prepare_db_inputs($product->image->src);
                }
            }
            $colleci_api = array('api_name' => 'collects');
            if (isset($_POST['collections']) && $_POST['collections'] != '') {
                foreach ($_POST['collections'] as $value) {
                    $collect_api_fields = array('collect' => array('product_id' => $product->id, 'collection_id' => $value));
                    $collect_response[] = get_api_list($colleci_api, $collect_api_fields, 'POST', 1, array("Content-Type: application/json"));
                    
                }
            }
            echo "<pre>";
                    print_r($collect_response);
        die;
        return $response;
    }
add_products();
?>