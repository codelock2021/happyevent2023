-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 28, 2022 at 11:09 AM
-- Server version: 10.5.15-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u402017191_postcode`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_shops`
--

CREATE TABLE `user_shops` (
  `store_user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive',
  `application_status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=enable,0=disable',
  `application_language` enum('en','es','he_IL','fil','fr','ru','sp','pt') DEFAULT 'en',
  `shop_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_demand_accept` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1:aproved; 0:not approved',
  `charge` float NOT NULL DEFAULT 0,
  `price_id` varchar(255) DEFAULT NULL,
  `invoice_on` date DEFAULT NULL,
  `operated_on` date DEFAULT NULL,
  `test_finals_on` date DEFAULT NULL,
  `removed_on` date DEFAULT NULL,
  `store_name` varchar(255) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `store_idea` varchar(25) NOT NULL,
  `price_pattern` varchar(60) NOT NULL,
  `cash` varchar(25) NOT NULL,
  `store_holder` varchar(255) NOT NULL,
  `address11` varchar(255) NOT NULL,
  `address22` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `division` varchar(50) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `america_timezone` varchar(50) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `feedback_status` enum('1','0') NOT NULL DEFAULT '0',
  `block_status` enum('1','0') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `uninstall_on` timestamp NULL DEFAULT NULL,
  `install_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_shops`
--

INSERT INTO `user_shops` (`store_user_id`, `email`, `status`, `application_status`, `application_language`, `shop_name`, `password`, `is_demand_accept`, `charge`, `price_id`, `invoice_on`, `operated_on`, `test_finals_on`, `removed_on`, `store_name`, `api_key`, `store_idea`, `price_pattern`, `cash`, `store_holder`, `address11`, `address22`, `city`, `country_name`, `mobile_no`, `division`, `zip`, `timezone`, `america_timezone`, `domain`, `feedback_status`, `block_status`, `created_at`, `updated_at`, `uninstall_on`, `install_date`) VALUES
(9, '', '1', '0', 'en', 'postcodeapp.myshopify.com', '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', '0', 0, NULL, NULL, NULL, NULL, NULL, 'postcodeapp.myshopify.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '0', '2022-05-28 06:09:14', '2022-05-28 06:09:14', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_shops`
--
ALTER TABLE `user_shops`
  ADD PRIMARY KEY (`store_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_shops`
--
ALTER TABLE `user_shops`
  MODIFY `store_user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
