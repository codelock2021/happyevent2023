-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 08, 2022 at 07:54 AM
-- Server version: 5.7.36
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `postcode`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_shops`
--

DROP TABLE IF EXISTS `user_shops`;
CREATE TABLE IF NOT EXISTS `user_shops` (
  `store_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=active,0=deactive',
  `application_status` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=enable,0=disable',
  `application_language` enum('en','es','he_IL','fil','fr','ru','sp','pt') DEFAULT 'en',
  `shop_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_demand_accept` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1:aproved; 0:not approved',
  `charge` float NOT NULL DEFAULT '0',
  `price_id` varchar(255) DEFAULT NULL,
  `invoice_on` date DEFAULT NULL,
  `operated_on` date DEFAULT NULL,
  `test_finals_on` date DEFAULT NULL,
  `removed_on` date DEFAULT NULL,
  `store_name` varchar(255) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `store_idea` varchar(25) NOT NULL,
  `price_pattern` varchar(60) NOT NULL,
  `cash` varchar(25) NOT NULL,
  `store_holder` varchar(255) NOT NULL,
  `address11` varchar(255) NOT NULL,
  `address22` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `division` varchar(50) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `america_timezone` varchar(50) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `feedback_status` enum('1','0') NOT NULL DEFAULT '0',
  `block_status` enum('1','0') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uninstall_on` timestamp NULL DEFAULT NULL,
  `install_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`store_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_shops`
--

INSERT INTO `user_shops` (`store_user_id`, `email`, `status`, `application_status`, `application_language`, `shop_name`, `password`, `is_demand_accept`, `charge`, `price_id`, `invoice_on`, `operated_on`, `test_finals_on`, `removed_on`, `store_name`, `api_key`, `store_idea`, `price_pattern`, `cash`, `store_holder`, `address11`, `address22`, `city`, `country_name`, `mobile_no`, `division`, `zip`, `timezone`, `america_timezone`, `domain`, `feedback_status`, `block_status`, `created_at`, `updated_at`, `uninstall_on`, `install_date`) VALUES
(4, 'codelock2021@gmail.com', '1', '0', 'en', 'postcodeapp.myshopify.com', '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', '0', 0, NULL, NULL, NULL, NULL, NULL, 'cls_rakshita', '', 'partner_test', '', '', '', '', '', 'Surat', 'India', '', '', '394105', '(GMT+05:30) Asia/Calcutta', '', '', '0', '0', '2021-06-15 06:05:36', '2021-06-15 06:05:36', NULL, NULL),
(5, 'codelock2021@gmail.com', '1', '0', 'en', 'cls-rewriter.myshopify.com', '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', '0', 0, NULL, NULL, NULL, NULL, NULL, 'cls_rewriter', '', 'partner_test', '', '', '', '', '', 'Surat', 'India', '', '', '394105', '(GMT+05:30) Asia/Calcutta', '', '', '0', '0', '2021-06-16 05:49:43', '2021-06-16 05:49:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

DROP TABLE IF EXISTS `zone`;
CREATE TABLE IF NOT EXISTS `zone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_user_id` int(11) NOT NULL,
  `zonename` varchar(50) NOT NULL,
  `zonearea` text NOT NULL,
  `zoneprice` float NOT NULL,
  `zonestatus` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id`, `store_user_id`, `zonename`, `zonearea`, `zoneprice`, `zonestatus`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 'zone1', 'india', 1000, '0', '1', '2022-06-05 00:11:04', '2022-06-05 00:11:04'),
(3, 5, 'zone2', 'surat', 9980, '1', '1', '2022-06-05 01:59:14', '2022-06-05 01:59:14'),
(4, 5, 'zone3', '398776,456467,345678', 996, '1', '1', '2022-06-05 01:59:49', '2022-06-05 01:59:49'),
(5, 5, 'Zone4', '395001', 500, '1', '1', '2022-06-05 02:00:21', '2022-06-05 02:00:21'),
(6, 5, 'zone5', 'amreli', 600, '1', '1', '2022-06-05 02:00:51', '2022-06-05 02:00:51'),
(12, 4, 'Postcode1', '395001,395010', 1000, '1', '1', '2022-06-08 01:49:44', '2022-06-08 01:49:44'),
(10, 5, 'zone5', 'amreli', 6000, '1', '1', '2022-06-05 22:57:58', '2022-06-05 22:57:58');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
