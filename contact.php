<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from duruthemes.com/demo/html/bethany/demo2/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Oct 2021 05:51:37 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <title>Contact Us</title>
    <link href="css/bootstrap.min.css" rel=stylesheet>
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/themify-icons.css" rel="stylesheet">
    <link href="modules/slick/slick.css" rel="stylesheet">
    <link href="modules/slick/slick-theme.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.carousel.min.css" rel="stylesheet">
    <link href="modules/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
    <link href="modules/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="modules/youtubepopup/YouTubePopUp.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
      <link href="css/contact.css" rel="stylesheet">
     <script src="js/header-footer.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-144098545-1');
    </script>
    
    <style>
    .info {
    border-top: 3px solid #a2783a;
    border-bottom: 3px solid #a2783a;
    padding: 30px;
    background: #fff;
    width: 100%;
    box-shadow: 0 0 24px 0 rgb(0 0 0 / 10%);
}
form.php-email-form {
    width: 100%;
    border-top: 3px solid #a2783a;
    border-bottom: 3px solid #a2783a;
    padding: 30px;
    background: #fff;
    box-shadow: 0 0 24px 0 rgb(0 0 0 / 12%);
}
button {
    background: #a2783a;
}
.section-title h2{
    color: #a2783a;
}
    </style>
</head>

<body>
    <!-- Preloader -->
    <div id="bethany-page-loading" class="bethany-pageloading">
        <div class="bethany-pageloading-inner"> 
            <img src="images/logo-dark.png" class="logo" alt=""> 
        </div>
    </div>
    <!-- Navigation Bar -->
     <div w3-include-html="happyheader.php"></div> 
        <script>
            includeHTML();
        </script>
    <!-- videos -->
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contact</h2>
          <!--<p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>-->
          <p>We believe in high quality work through technological advancements, innovation, process enhancement and team work.</p>
          <p>Feel free to contact us for your inquiries and queries.</p>
        </div>

        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch">
            <div class="info">
              <div class="address">
                <i class="bi bi-geo-alt"></i>
                <h4>Location:</h4>
                <p>Silver Business Point, 21, Green Rd, Uttran, Surat, Gujarat 394105A</p>
              </div>

              <div class="email">
                <i class="bi bi-envelope"></i>
                <h4>Email:</h4>
                <p>happyeventsurat@gmail.com</p>
              </div>

              <div class="phone">
                <i class="bi bi-phone"></i>
                <h4>Call:</h4>
                <p>7600464414,9714779996</p>
              </div>

              <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>-->
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3718.9508368397505!2d72.85981175120838!3d21.23379803581841!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04f601a3dcf5d%3A0x6f62c9e8f497282b!2sCodeLock%20Solutions%20%7C%20Shopify%20Expert%20%7C%20Flutter%20App%20Development%20%7C%20Blockchain%20Development!5e0!3m2!1sen!2sin!4v1639733747804!5m2!1sen!2sin" width="100%" height="290px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>

          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name">Your Name</label>
                  <input type="text" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Your Email</label>
                  <input type="email" class="form-control" name="email" id="email" required>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Subject</label>
                <input type="text" class="form-control" name="subject" id="subject" required>
              </div>
              <div class="form-group">
                <label for="name">Message</label>
                <textarea class="form-control" name="message" rows="10" id="messages"  required></textarea>
              </div>
              <div class="my-3">
                <div class="loading"></div>
                <div class="error-message"></div>
                 <div class="text-center"><button type="submit" id="mailerButton">Send Message</button></div>
                 <center><span class="emailMassage"></span></center>
              </div>
             
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->
  
   
    <!-- Footer -->
      <div w3-include-html="footer.html"></div> 
        <script>
            footerHTML();
        </script>
    <!-- toTop --><a href="index.html#" class="totop">TOP</a>
    <!-- jQuery -->
    <script src="js/plugins/jquery-3.5.1.min.js"></script>
    <script src="admin/assets/js/img_ajax1.js"></script>
    <script src="js/plugins/bootstrap.min.js"></script>
    <script src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script src="js/plugins/jquery.isotope.v3.0.2.js"></script>
    <script src="js/plugins/modernizr-2.6.2.min.js"></script>
    <script src="js/plugins/jquery.waypoints.min.js"></script>
    <script src="modules/owl-carousel/owl.carousel.min.js"></script>
    <script src="modules/slick/slick.js"></script>
    <script src="modules/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="modules/masonry/masonry.pkgd.min.js"></script>
    <script src="modules/youtubepopup/YouTubePopUp.js"></script>
    <script src="js/script.js"></script>
</body>

<!-- Mirrored from duruthemes.com/demo/html/bethany/demo2/services.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Oct 2021 05:51:53 GMT -->
</html>



